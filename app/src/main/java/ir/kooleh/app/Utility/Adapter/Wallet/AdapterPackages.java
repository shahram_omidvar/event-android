package ir.kooleh.app.Utility.Adapter.Wallet;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Wallet.InterfacePackage;
import ir.kooleh.app.Structure.Model.Wallet.ModelPackage;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterPackages extends RecyclerView.Adapter<AdapterPackages.HolderPackage>
{

    private Activity activity;
    private List<ModelPackage> packageList;
    private InterfacePackage.Package.IntCallbackBtnBuyPackageItem intCallbackBtnBuyPackageItem;

    public AdapterPackages(Activity activity)
    {
        this.activity = activity;
        intCallbackBtnBuyPackageItem = (InterfacePackage.Package.IntCallbackBtnBuyPackageItem) activity;
    }

    @NonNull
    @Override
    public HolderPackage onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderPackage(LayoutInflater.from(activity).inflate(R.layout.item_package, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderPackage holder, int position)
    {
        ModelPackage modelPackage = packageList.get(position);
        holder.tvName.setText(modelPackage.getName());
        holder.tvPrice.setText("قیمت: " + NumberUtils.digitDivider(modelPackage.getPrice().toString()) + " ریال");
        holder.tvPeriod.setText("مدت: " + modelPackage.getCountDay() + " روز");
        if (modelPackage.getCount().equals(-1))
            holder.tvAmount.setVisibility(View.GONE);
        else
            holder.tvAmount.setText("تعداد: " + modelPackage.getCount());
        holder.tvExplain.setText("توضیحات: " + modelPackage.getExplain());

        holder.btnBuy.setOnClickListener(v -> {
            intCallbackBtnBuyPackageItem.onResponseCallbackBtnBuyPackageItem(modelPackage);
        });
    }

    @Override
    public int getItemCount()
    {
        return packageList == null ? 0 : packageList.size();
    }


    public void setList(List<ModelPackage> packageList)
    {
        this.packageList = packageList;
        notifyDataSetChanged();
    }

    public class HolderPackage extends RecyclerView.ViewHolder
    {
        private final CTextView tvName;
        private final CTextView tvPrice;
        private final CTextView tvPeriod;
        private final CTextView tvAmount;
        private final CTextView tvExplain;
        private final CardView btnBuy;
        public HolderPackage(@NonNull View itemView)
        {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_package_name_package);
            tvPrice = itemView.findViewById(R.id.tv_package_price_package);
            tvPeriod = itemView.findViewById(R.id.tv_package_period_package);
            tvAmount = itemView.findViewById(R.id.tv_package_amount_package);
            tvExplain = itemView.findViewById(R.id.tv_package_explain_package);
            btnBuy = itemView.findViewById(R.id.btn_package_buy_package);
        }
    }
}

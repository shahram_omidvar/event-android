package ir.kooleh.app.Utility.Adapter.Wallet;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelUserPackage;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterUserPackages extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private Activity activity;
    private List<ModelUserPackage> userPackageList = new ArrayList<>();
    private InterfaceTryAgainPaginate interfaceTryAgainPaginate;
    private boolean showbtnTryAgain = false;

    public AdapterUserPackages(Activity activity)
    {
        this.activity = activity;
        if (activity instanceof InterfaceTryAgainPaginate)
        {
            this.interfaceTryAgainPaginate = (InterfaceTryAgainPaginate) activity;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == 2)
            return new HolderItemLastPaginate(LayoutInflater.from(activity).inflate(R.layout.item_last_paginate, parent, false));
        return new HolderUserPackages(LayoutInflater.from(activity).inflate(R.layout.item_user_package, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof HolderUserPackages)
        {
            HolderUserPackages holderUserPackages = (HolderUserPackages) holder;
            ModelUserPackage modelUserPackage = userPackageList.get(position);
            holderUserPackages.tvName.setText(modelUserPackage.getName());
            holderUserPackages.tvStatus.setText(modelUserPackage.getStatus());
            holderUserPackages.tvPrice.setText("هزینه: " + NumberUtils.digitDivider(modelUserPackage.getPrice()) + " ریال");
            holderUserPackages.tvStartDate.setText("از " + modelUserPackage.getStartDate());
            holderUserPackages.tvEndDate.setText("تا " + modelUserPackage.getEndDate());
            holderUserPackages.tvExplain.setText("توضیحات: " + modelUserPackage.getExplain());
            holderUserPackages.tvDate.setText(modelUserPackage.getCreateTIme());
        }
        else
        {
            HolderItemLastPaginate holderUserPackagesBottom = (HolderItemLastPaginate) holder;
            if (showbtnTryAgain)
            {
                showbtnTryAgain = false;
                holderUserPackagesBottom.itemView.setVisibility(View.VISIBLE);
                holderUserPackagesBottom.itemView.setOnClickListener(v ->
                {
                    holderUserPackagesBottom.itemView.setVisibility(View.GONE);
                    interfaceTryAgainPaginate.onResponseTryAgainPaginate();
                });
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return userPackageList == null ? 0 : userPackageList.size() + 1;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == this.getItemCount() - 1)
        {
            return 2;
        }
        return 1;
    }

    public void setList(List<ModelUserPackage> userPackageList)
    {
        this.userPackageList = userPackageList;
        notifyDataSetChanged();
    }

    public void addList(List<ModelUserPackage> userPackageList)
    {
        this.userPackageList.addAll(userPackageList);
        notifyDataSetChanged();
    }

    public void showBtnTryAgain()
    {
        this.showbtnTryAgain = true;
        notifyDataSetChanged();
    }



    public class HolderUserPackages extends RecyclerView.ViewHolder
    {
        private final CTextView tvName;
        private final CTextView tvStatus;
        private final CTextView tvPrice;
        private final CTextView tvStartDate;
        private final CTextView tvEndDate;
        private final CTextView tvExplain;
        private final CTextView tvDate;
        public HolderUserPackages(@NonNull View itemView)
        {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_package_name_user_package);
            tvStatus = itemView.findViewById(R.id.tv_package_status_user_package);
            tvPrice = itemView.findViewById(R.id.tv_package_price_user_package);
            tvStartDate = itemView.findViewById(R.id.tv_package_start_date_user_package);
            tvEndDate = itemView.findViewById(R.id.tv_package_end_date_user_package);
            tvExplain = itemView.findViewById(R.id.tv_package_explain_user_package);
            tvDate = itemView.findViewById(R.id.tv_package_date_user_package);
            tvStartDate.setSelected(true);
            tvEndDate.setSelected(true);
        }
    }


    public class HolderItemLastPaginate extends RecyclerView.ViewHolder
    {
        public HolderItemLastPaginate(@NonNull View itemView)
        {
            super(itemView);
            itemView.setVisibility(View.GONE);
        }
    }
}
package ir.kooleh.app.Utility.Adapter.Fragment;

import android.content.Context;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;

import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Model.Ads.ModelAdsKey;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Fragment.FragmentAdsKey;


public class AdapterAdsKey extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private final List<ModelAdsKey.ListAd> listAdsKey = new ArrayList<>();

    private final Context context;
    private final InterfaceAds.Fragment.AdsKey.AdapterAdsKey iAdapterAdsKey;
    private static final String TAG = "adapteradskey";
    private String listTypeAd;

    private boolean showbtnTryAgain;
    private final InterfaceTryAgainPaginate interfaceTryAgainPaginate;


    public AdapterAdsKey(Context context, FragmentAdsKey fragmentAdsKey)
    {
        this.context = context;
        this.iAdapterAdsKey =  fragmentAdsKey;
        this.interfaceTryAgainPaginate = fragmentAdsKey;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == 2)
            return new HolderItemLastPaginate(LayoutInflater.from(context).inflate(R.layout.item_last_paginate, parent, false));
        return new AdsKeyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_row_ads_key, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof AdsKeyViewHolder)
        {
            AdsKeyViewHolder adsKeyViewHolder = (AdsKeyViewHolder) holder;
            ModelAdsKey.ListAd modelAdsKey = listAdsKey.get(position);

            adsKeyViewHolder.tvStory.setText(String.format("%s", modelAdsKey.getStory()));
            adsKeyViewHolder.tvExplain.setText(String.format("%s", modelAdsKey.getExplain()));
            adsKeyViewHolder.tvExplainTwo.setText(String.format("%s", modelAdsKey.getExplainTwo()));
            adsKeyViewHolder.ivAd.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            adsKeyViewHolder.ivAd.setImageResource(R.drawable.img_name_logo_kooleh);

            if (modelAdsKey.getTypeAd().equals("auction_ad") && NumberUtils.isNumeric(modelAdsKey.getMessageImage()))
            {
                if (adsKeyViewHolder.tvMessageImage.getVisibility() == View.GONE)
                {
                    adsKeyViewHolder.tvMessageImage.setVisibility(View.VISIBLE);
                }
                if (modelAdsKey.getCountDownTimer() != null)
                {
                    modelAdsKey.getCountDownTimer().cancel();
                }
                modelAdsKey.setCountDownTimer(new CountDownTimer(Integer.parseInt(modelAdsKey.getMessageImage()) * 1000L
                        , 1000) {
                    public void onTick(long millisUntilFinished) {
                        long seconds = millisUntilFinished / 1000;
                        long minutes = seconds / 60;
                        long hours = minutes / 60;
                        String strCountdownTime = String.format(Locale.ENGLISH, "%02d", hours % 24)
                                + ":" + String.format(Locale.ENGLISH, "%02d", minutes % 60)
                                + ":" + String.format(Locale.ENGLISH, "%02d", seconds % 60);
                        Log.i(TAG, "onTick: " + strCountdownTime);
                        adsKeyViewHolder.tvMessageImage.setText(strCountdownTime);
                    }

                    public void onFinish()
                    {
                        adsKeyViewHolder.tvMessageImage.setText("00:00:00");
                    }
                }.start());
            }
            else
            {
                adsKeyViewHolder.tvMessageImage.setVisibility(View.GONE);
            }

            if (modelAdsKey.getIconTypeAd() != null && modelAdsKey.getIconTypeAd().length() > 0)
                Glide.with(context).load(modelAdsKey.getIconTypeAd()).placeholder(R.drawable.img_name_logo_kooleh).into(adsKeyViewHolder.ivTypeAd);

            if (modelAdsKey.getRate().equals("0"))
                adsKeyViewHolder.relRate.setVisibility(View.GONE);
            else
            {
                adsKeyViewHolder.relRate.setVisibility(View.VISIBLE);
                adsKeyViewHolder.tvRate.setText(String.format("%s", modelAdsKey.getRate()));

                if (modelAdsKey.getIconRate() != null && modelAdsKey.getIconRate().length() > 0)
                    Glide.with(context).load(modelAdsKey.getIconRate()).placeholder(R.drawable.img_name_logo_kooleh).into(adsKeyViewHolder.ivRate);
            }


            if (modelAdsKey.getImage() != null && modelAdsKey.getImage().length() > 0)
            {
                adsKeyViewHolder.ivAd.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(context).load(modelAdsKey.getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(adsKeyViewHolder.ivAd);
            }

            holder.itemView.setOnClickListener(v -> iAdapterAdsKey.onClickItemAdKey(modelAdsKey));
        }
        else
        {
            HolderItemLastPaginate holderUserPackagesBottom = (HolderItemLastPaginate) holder;
            if (showbtnTryAgain)
            {
                showbtnTryAgain = false;
                holderUserPackagesBottom.itemView.setVisibility(View.VISIBLE);
                holderUserPackagesBottom.itemView.setOnClickListener(v ->
                {
                    holderUserPackagesBottom.itemView.setVisibility(View.GONE);
                    interfaceTryAgainPaginate.onResponseTryAgainPaginate();
                });
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return listAdsKey == null ? 0 : listAdsKey.size() + 1;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == this.getItemCount() - 1)
        {
            return 2;
        }
        return 1;
    }

    public void setList(String listTypeAd, List<ModelAdsKey.ListAd> listAdsKey)
    {
        this.listAdsKey.addAll(listAdsKey);
        this.listTypeAd = listTypeAd;
        notifyDataSetChanged();
    }

    public void showBtnTryAgain()
    {
        this.showbtnTryAgain = true;
        notifyDataSetChanged();
    }

    public void killTimers()
    {
        if (listAdsKey != null)
        {
            for (ModelAdsKey.ListAd modelAdsKey : listAdsKey)
            {
                if (modelAdsKey.getCountDownTimer() != null)
                {
                    modelAdsKey.getCountDownTimer().cancel();
                }
            }
        }
    }

    static class AdsKeyViewHolder extends RecyclerView.ViewHolder
    {

        private final TextView tvStory;
        private final TextView tvExplain;
        private final TextView tvExplainTwo;
        private final ImageView ivTypeAd;

        //image

        private final RelativeLayout relRate;
        private final ImageView ivRate;
        private final TextView tvRate;
        private final ImageView ivAd;
        private final TextView tvMessageImage;

        public AdsKeyViewHolder(@NonNull View view)
        {
            super(view);

            tvStory = view.findViewById(R.id.tv_story_row_rads_key);
            tvExplain = view.findViewById(R.id.tv_explain_row_rads_key);
            tvExplainTwo = view.findViewById(R.id.tv_explain_two_row_ads_key);
            ivTypeAd = view.findViewById(R.id.iv_type_ad_row_ads_key);

            //------*-*-*-*--------------*-*-*---------------
            relRate = view.findViewById(R.id.rel_rate_row_ads_key);
            ivRate = view.findViewById(R.id.iv_rate_row_ads_key);
            tvRate = view.findViewById(R.id.tv_rate_row_rads_key);
            ivAd = view.findViewById(R.id.iv_image_row_rads_key);
            tvMessageImage = view.findViewById(R.id.tv_message_image_row_rads_key);

        }
    }

    public class HolderItemLastPaginate extends RecyclerView.ViewHolder
    {
        public HolderItemLastPaginate(@NonNull View itemView)
        {
            super(itemView);
            itemView.setVisibility(View.GONE);
        }
    }
}

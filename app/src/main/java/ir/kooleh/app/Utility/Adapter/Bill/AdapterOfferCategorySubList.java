package ir.kooleh.app.Utility.Adapter.Bill;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.Structure.Model.Bills.ModelListSuggestion;
import ir.kooleh.app.Structure.Model.Dashboard.ModelMostViewedDashboard;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterMostViewedDashboard;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterOfferCategorySubList extends RecyclerView.Adapter<AdapterOfferCategorySubList.HolderBillType>
{

    private Activity activity;
    private List<ModelListSuggestion> modelListSuggestions;

    public AdapterOfferCategorySubList(Activity activity)
    {
        this.activity = activity;
    }

    @NonNull
    @Override
    public HolderBillType onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderBillType(LayoutInflater.from(activity).inflate(R.layout.item_offer_category_sub_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderBillType holder, int position)
    {
        ModelListSuggestion modelListSuggestion = modelListSuggestions.get(position);
        holder.tvCategoryName.setText(modelListSuggestion.getTitle());

        AdapterMostViewedDashboard adapterBillType = new AdapterMostViewedDashboard(activity, modelListSuggestion.getSuggestions());
        holder.rvCategorySubList.setAdapter(adapterBillType);
    }

    @Override
    public int getItemCount()
    {
        return modelListSuggestions == null ? 0 : modelListSuggestions.size();
    }

    public void setList(List<ModelListSuggestion> modelListSuggestions)
    {
        this.modelListSuggestions = modelListSuggestions;
        notifyDataSetChanged();
    }


    public class HolderBillType extends RecyclerView.ViewHolder
    {
        private CTextView tvCategoryName;
        private RecyclerView rvCategorySubList;
        public HolderBillType(@NonNull View itemView)
        {
            super(itemView);
            tvCategoryName = itemView.findViewById(R.id.tv_bill_category_name);
            rvCategorySubList = itemView.findViewById(R.id.rv_bill_category_sub_list);
            rvCategorySubList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
//            FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(activity);
//            flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
//            flexboxLayoutManager.setJustifyContent(JustifyContent.CENTER);
//            rvCategorySubList.setLayoutManager(flexboxLayoutManager);
        }
    }

}

package ir.kooleh.app.Utility.Adapter.Wallet;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceBuys;
import ir.kooleh.app.Structure.Model.Wallet.ModelBuy;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterBuys extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private final Activity activity;
    private final List<ModelBuy> buyList = new ArrayList<>();

    private boolean showbtnTryAgain;
    private final InterfaceTryAgainPaginate interfaceTryAgainPaginate;
    private final InterfaceBuys.Buys.IntOpenMoreDetailsTransaction intOpenMoreDetailsTransaction;

    public AdapterBuys(Activity activity)
    {
        this.activity = activity;
        this.interfaceTryAgainPaginate = (InterfaceTryAgainPaginate) activity;
        this.intOpenMoreDetailsTransaction = (InterfaceBuys.Buys.IntOpenMoreDetailsTransaction) activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == 2)
            return new HolderItemLastPaginate(LayoutInflater.from(activity).inflate(R.layout.item_last_paginate, parent, false));
        return new HolderBuys(LayoutInflater.from(activity).inflate(R.layout.item_buy, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof HolderBuys)
        {
            HolderBuys holderBuys = (HolderBuys) holder;
            ModelBuy modelBuy = buyList.get(position);
            holderBuys.tvNoTransaction.setVisibility(View.VISIBLE);
            holderBuys.containerTransaction.setVisibility(View.GONE);

            holderBuys.tvNav.setText(modelBuy.getTitleNav());
            holderBuys.tvExplainNav.setText(modelBuy.getExplainNav());
            holderBuys.tvRightOne.setText(modelBuy.getRightOne());
            holderBuys.tvLeftOne.setText(modelBuy.getLeftOne());
            holderBuys.tvRightTwo.setText(modelBuy.getRightTwo());
            holderBuys.tvLeftTwo.setText(modelBuy.getLeftTwo());
            holderBuys.tvExplain.setText(modelBuy.getExplain());

            if (modelBuy.getTransaction() != null)
            {
                holderBuys.tvNoTransaction.setVisibility(View.GONE);
                holderBuys.containerTransaction.setVisibility(View.VISIBLE);
                holderBuys.tvStatusTransaction.setText(modelBuy.getTransaction().getStatusExplain());
                holderBuys.tvIDTransaction.setText(modelBuy.getTransaction().getTransactionId());
                holderBuys.tvCodeTransaction.setText(modelBuy.getTransaction().getCode());
                holderBuys.tvExplainTransaction.setText(modelBuy.getTransaction().getStatusExplain());
            }

            holderBuys.btnMoreDetailsTransaction.setOnClickListener(v ->
            {
                intOpenMoreDetailsTransaction.onResponseOpenMoreDetailsTransaction(modelBuy.getTransaction().getId());
            });
        }
        else
        {
            HolderItemLastPaginate holderItemLastPaginate = (HolderItemLastPaginate) holder;
            if (showbtnTryAgain)
            {
                showbtnTryAgain = false;
                holderItemLastPaginate.itemView.setVisibility(View.VISIBLE);
                holderItemLastPaginate.itemView.setOnClickListener(v ->
                {
                    holderItemLastPaginate.itemView.setVisibility(View.GONE);
                    interfaceTryAgainPaginate.onResponseTryAgainPaginate();
                });
            }
        }

    }

    @Override
    public int getItemCount()
    {
        return buyList == null ? 0 : buyList.size() + 1;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == this.getItemCount() - 1)
        {
            return 2;
        }
        return 1;
    }

    public void showBtnTryAgain()
    {
        this.showbtnTryAgain = true;
        notifyDataSetChanged();
    }


    public void addList(List<ModelBuy> buyList)
    {
        this.buyList.addAll(buyList);
        notifyDataSetChanged();
    }

    public static class HolderBuys extends RecyclerView.ViewHolder
    {
        private final CTextView tvNav;
        private final CTextView tvExplainNav;
        private final CTextView tvRightOne;
        private final CTextView tvLeftOne;
        private final CTextView tvRightTwo;
        private final CTextView tvLeftTwo;
        private final CTextView tvExplain;
        private final RelativeLayout containerTransaction;
        private final CTextView tvNoTransaction;
        private final CTextView tvStatusTransaction;
        private final CTextView tvIDTransaction;
        private final CTextView tvCodeTransaction;
        private final CTextView tvExplainTransaction;
        private final CardView btnMoreDetailsTransaction;

        public HolderBuys(@NonNull View itemView)
        {
            super(itemView);
            tvNav = itemView.findViewById(R.id.tv_nav_buy);
            tvExplainNav = itemView.findViewById(R.id.tv_explain_nav_buy);
            tvRightOne = itemView.findViewById(R.id.tv_right_one_buy);
            tvLeftOne = itemView.findViewById(R.id.tv_left_one_buy);
            tvRightTwo = itemView.findViewById(R.id.tv_right_two_buy);
            tvLeftTwo = itemView.findViewById(R.id.tv_left_two_buy);
            tvExplain = itemView.findViewById(R.id.tv_explain_buy);
            containerTransaction = itemView.findViewById(R.id.container_transaction_buy);
            tvNoTransaction = itemView.findViewById(R.id.tv_no_transaction_buy);
            tvStatusTransaction = itemView.findViewById(R.id.tv_status_transaction_buy);
            tvIDTransaction = itemView.findViewById(R.id.tv_id_transaction_buy);
            tvCodeTransaction = itemView.findViewById(R.id.tv_code_transaction_buy);
            tvExplainTransaction = itemView.findViewById(R.id.tv_explain_transaction_buy);
            btnMoreDetailsTransaction = itemView.findViewById(R.id.btn_more_details_transaction_buy);

            tvStatusTransaction.setSelected(true);
            tvIDTransaction.setSelected(true);
            tvCodeTransaction.setSelected(true);
            tvExplainTransaction.setSelected(true);
        }
    }

    public static class HolderItemLastPaginate extends RecyclerView.ViewHolder
    {
        public HolderItemLastPaginate(@NonNull View itemView)
        {
            super(itemView);
            itemView.setVisibility(View.GONE);
        }
    }
}

package ir.kooleh.app.Utility.Class;


import ir.kooleh.app.R;

public class DashboardUtils
{

    public static int getButtonIconByAction(String action)
    {
        switch (action)
        {
            case "profile":
                return R.drawable.man;
            case "my_ads":
                return R.drawable.broadcast;
            case "my_auctions":
                return R.drawable.auction;
            case "my_offers":
                return R.drawable.ic_offer;
            default:
                return 0;

        }
    }


    public static int getAdStepByViewpagerPositionAndType(int adType, int position)
    {
        switch (adType)
        {
            case 9:
                switch (position)
                {
                    case 0:
                        return 12;
                    case 1:
                        return 11;
                    default:
                        return 14;
                }
            case 10:
                switch (position)
                {
                    case 0:
                        return 20;
                    case 1:
                        return 21;
                    case 2:
                        return 22;
                    case 3:
                        return 11;
                    default:
                        return 14;
                }
            case 11:
                switch (position)
                {
                    case 1:
                        return 20;
                    case 2:
                        return 21;
                    default:
                        return 22;
                }
            default:
                return 14;
        }
    }


    public static int getArchiveByViewpagerPositionAndType(int adType, int position)
    {
        switch (adType)
        {
            case 9:
                if (position == 2)
                {
                    return 1;
                }
                return 0;
            case 10:
                if (position == 4)
                {
                    return 1;
                }
                return 0;
            default:
                return 0;
            case 11:
                switch (position)
                {
                    case 0:
                        return 2;
                    case 3:
                        return 1;
                    default:
                        return 0;
                }

        }
    }
}

package ir.kooleh.app.Utility.Volley;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class Controller extends MultiDexApplication
{
    public static final String TAG = Controller.class.getSimpleName();
    private static Controller controller;
    private RequestQueue queue;
    private ImageLoader mImageLoader;
    public static Typeface MyTypeface;
    public static Context context;

    public static synchronized Controller getPermission()
    {
        return controller;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        controller = this;
        context = getApplicationContext();
        Log.d(TAG, "onCreate: controller");
        MyTypeface = Typeface.createFromAsset(getAssets(), "fonts/iran_sans.ttf");
    }

    public static Context getContext() {
        return context;
    }

    public RequestQueue getRequestQueue()
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(getBaseContext());
        }
        return queue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag)
    {
        // set the def tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req)
    {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag)
    {
        if (queue != null)
        {
            queue.cancelAll(tag);
        }
    }

}



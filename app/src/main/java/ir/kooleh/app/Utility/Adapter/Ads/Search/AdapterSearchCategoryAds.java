package ir.kooleh.app.Utility.Adapter.Ads.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Model.Search.ModelSearchAds;
import ir.kooleh.app.View.ViewCustom.CButton;

public class AdapterSearchCategoryAds extends RecyclerView.Adapter<AdapterSearchCategoryAds.SearchCategoryAdsViewHolder>
{
    private final Context context;
    private final InterfaceAds.IAdapterCategorySearchAds interfaceSearchAds;
    private List<ModelSearchAds.Category> listCategory;


    public AdapterSearchCategoryAds(Context context, InterfaceAds.IAdapterCategorySearchAds iAdapterCategorySearchAds)
    {
        this.context = context;
        this.interfaceSearchAds = iAdapterCategorySearchAds;
    }

    @NonNull
    @Override
    public SearchCategoryAdsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new SearchCategoryAdsViewHolder(LayoutInflater.from(context).inflate(R.layout.item_row_search_category, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearchCategoryAdsViewHolder holder, int position)
    {
        ModelSearchAds.Category category = listCategory.get(position);
        holder.btnSearchAd.setText(String.format("%s", category.getName()));
        holder.btnSearchAd.setOnClickListener(view -> interfaceSearchAds.onClickCategorySearchAds(category));
    }

    @Override
    public int getItemCount()
    {
        return listCategory == null ? 0 : listCategory.size();
    }


    static class SearchCategoryAdsViewHolder extends RecyclerView.ViewHolder
    {
        CButton btnSearchAd;

        public SearchCategoryAdsViewHolder(@NonNull View itemView)
        {
            super(itemView);
            btnSearchAd = itemView.findViewById(R.id.btn_name_category_row_search);
        }
    }

    public void addListSearchCategoryAds(List<ModelSearchAds.Category> listCategory)
    {
        this.listCategory = listCategory;
        notifyDataSetChanged();
    }
}

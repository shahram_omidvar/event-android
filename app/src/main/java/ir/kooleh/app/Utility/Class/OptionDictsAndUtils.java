package ir.kooleh.app.Utility.Class;

import androidx.fragment.app.Fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.App.ModelApp;

public class OptionDictsAndUtils
{
    public Map<String, String> values = new HashMap<>();
    public Map<String, Integer> icons = new HashMap<>();
    public Map<String, Integer> ids = new HashMap<>();


    public void setOption(List<ModelApp.Menu> menuList)
    {
        int idCounter = -1;
        for (ModelApp.Menu option : menuList)
        {
            idCounter++;
            values.put(option.getName(), option.getValue());
            icons.put(option.getName(), getIconId(option.getName()));
            ids.put(option.getName(), idCounter);
        }
        values.put("fragment_dashboard", null);
        icons.put("fragment_dashboard", getIconId("fragment_dashboard"));
        ids.put("fragment_dashboard", idCounter + 1);
    }


    private int getIconId(String optionName)
    {
        switch (optionName)
        {
            case "fragment_load":
                return R.drawable.ic_suitecase;
            case "fragment_service":
                return R.drawable.ic_service;
            case "fragment_crate_ad":
                return R.drawable.ic_add;
            case "fragment_key":
                return R.drawable.ic_donate;
            case "fragment_web":
                return R.drawable.ic_driver_news;
            case "fragment_dashboard":
                return R.drawable.ic_backpack;
            default:
                return 0;
        }
    }


    public Fragment getFragmentByName(String optionName)
    {
        try {
            switch (optionName)
            {
                case "fragment_load":
                    return (Fragment) Class.forName("ir.kooleh.app.View.Ui.Fragment.FragmentTripsTours").newInstance();
                case "fragment_service":
                    return (Fragment) Class.forName("ir.kooleh.app.View.Ui.Fragment.FragmentServices").newInstance();
                case "fragment_key":
                    return (Fragment) Class.forName("ir.kooleh.app.View.Ui.Fragment.FragmentOffer").newInstance();
                case "fragment_web":
                    return (Fragment) Class.forName("ir.kooleh.app.View.Ui.Fragment.FragmentWebView").newInstance();
                case "fragment_dashboard":
                    return (Fragment) Class.forName("ir.kooleh.app.View.Ui.Fragment.FragmentDashboard").newInstance();
            }
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public String getFragmentValueByName(String optionName)
    {
        return values.get(optionName);
    }

}

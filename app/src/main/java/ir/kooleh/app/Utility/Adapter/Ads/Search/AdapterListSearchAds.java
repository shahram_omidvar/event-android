package ir.kooleh.app.Utility.Adapter.Ads.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Model.Search.ModelSearchAds;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterListSearchAds extends RecyclerView.Adapter<AdapterListSearchAds.SearchItemAdsViewHolder>
{
    private final Context context;
    private final InterfaceAds.IAdapterListSearchAds interfaceAdapterListSearchAds;
    private List<ModelSearchAds.Ads> lisAds;


    public AdapterListSearchAds(Context context, InterfaceAds.IAdapterListSearchAds interfaceAdapterListSearchAds)
    {
        this.context = context;
        this.interfaceAdapterListSearchAds = interfaceAdapterListSearchAds;
    }

    @NonNull
    @Override
    public SearchItemAdsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new SearchItemAdsViewHolder(LayoutInflater.from(context).inflate(R.layout.item_row_search_ads, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearchItemAdsViewHolder holder, int position)
    {
        ModelSearchAds.Ads ads = lisAds.get(position);


        if (ads.getImage() != null && ads.getImage().length() > 0)
            Glide.with(context).load(ads.getImage()).into(holder.ivLoad);

        if (ads.getIconRate() != null && ads.getIconRate().length() > 0)
            Glide.with(context).load(ads.getIconRate()).into(holder.ivRating);


        holder.tvNameCategory.setText(String.format("%s", ads.getNameCategory()));
        holder.tvRate.setText(String.format("%s", ads.getRate()));
        holder.tvTitle.setText(String.format("%s", ads.getTitle()));
        holder.tvExplain.setText(String.format("%s", ads.getExplain()));
        holder.tvPrice.setText(String.format("%s", ads.getPrice()));


        holder.itemView.setOnClickListener(view -> interfaceAdapterListSearchAds.onClickItemSearchAds(ads));
        holder.ivAddComment.setOnClickListener(view -> interfaceAdapterListSearchAds.onClickSaveCommentSearchAds(ads));

    }

    @Override
    public int getItemCount()
    {
        return lisAds == null ? 0 : lisAds.size();
    }


    static class SearchItemAdsViewHolder extends RecyclerView.ViewHolder
    {
        ImageView ivLoad, ivRating, ivAddComment;
        CTextView tvNameCategory, tvTitle, tvRate, tvExplain, tvPrice;

        public SearchItemAdsViewHolder(@NonNull View itemView)
        {
            super(itemView);

            ivLoad = itemView.findViewById(R.id.iv_image_row_search_ads);
            ivRating = itemView.findViewById(R.id.iv_image_rate_row_search_ads);
            ivAddComment = itemView.findViewById(R.id.iv_save_comment_row_search_ads);

            tvNameCategory = itemView.findViewById(R.id.tv_name_category_row_search_ads);
            tvRate = itemView.findViewById(R.id.tv_rate_row_search_ads);
            tvTitle = itemView.findViewById(R.id.tv_title_row_search_ads);
            tvExplain = itemView.findViewById(R.id.tv_explain_row_search_ads);
            tvPrice = itemView.findViewById(R.id.tv_price_row_search_ads);

        }
    }

    public void addListSearchAds(List<ModelSearchAds.Ads> listAds)
    {
        this.lisAds = listAds;
        notifyDataSetChanged();
    }
}

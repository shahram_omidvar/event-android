package ir.kooleh.app.Utility.Adapter.Load;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.Load.InterfaceLoad;
import ir.kooleh.app.Structure.Model.Load.ModelListLoad;
import ir.kooleh.app.View.Ui.Fragment.FragmentLoad;
import ir.kooleh.app.View.Ui.Fragment.FragmentTripsTours;

public class AdapterFragmentLoad extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private List<ModelListLoad> listLoads = new ArrayList<>();

    private final Context context;
    private final InterfaceLoad.Fragment interfaceLoadFragment;

    private boolean showbtnTryAgain;
    private final InterfaceTryAgainPaginate interfaceTryAgainPaginate;

    public AdapterFragmentLoad(Context context, FragmentTripsTours FragmentTripsTours)
    {
        this.context = context;
        this.interfaceLoadFragment = FragmentTripsTours;
        this.interfaceTryAgainPaginate = FragmentTripsTours;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == 2)
            return new HolderItemLastPaginate(LayoutInflater.from(context).inflate(R.layout.item_last_paginate, parent, false));
        return new LoadViewHolder(LayoutInflater.from(context).inflate(R.layout.item_row_list_load, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof LoadViewHolder)
        {
            LoadViewHolder loadViewHolder = (LoadViewHolder) holder;
            ModelListLoad load = listLoads.get(position);

            loadViewHolder.tvNameCarFragmentLoad.setText(load.getFleetInfo().getTitle());
            loadViewHolder.imFragmentLoad.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            loadViewHolder.imFragmentLoad.setImageResource(R.drawable.img_name_logo_kooleh);

            if (load.getImageMasterLoad() != null && load.getImageMasterLoad().length() > 0)
            {
                Glide.with(context).load(load.getImageMasterLoad()).placeholder(R.drawable.ic_avatar).into(loadViewHolder.ivFreightLoad);
            }


            if (load.getTagInfo() != null)
            {
                loadViewHolder.btnTagFragmentLoad.setVisibility(View.VISIBLE);
                loadViewHolder.btnTagFragmentLoad.setText(load.getTagInfo().getTitle());
                try
                {
                    loadViewHolder.btnTagFragmentLoad.setTextColor(Color.parseColor(load.getTagInfo().getColorTitle()));
                    loadViewHolder.btnTagFragmentLoad.getBackground().setColorFilter(Color.parseColor(load.getTagInfo().getColorBack()), PorterDuff.Mode.SRC_ATOP);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
            else loadViewHolder.btnTagFragmentLoad.setVisibility(View.GONE);


            if (load.getFleetInfo().getImage() != null && load.getFleetInfo().getImage().length() > 0)
            {
                loadViewHolder.imFragmentLoad.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(context).load(load.getFleetInfo().getImage()).placeholder(R.drawable.img_name_logo_kooleh).dontAnimate().into(loadViewHolder.imFragmentLoad);
            }

            loadViewHolder.tvTitleLoad.setText(load.getTitleMasterLoad());
            loadViewHolder.tvStartLoadFragmentLoad.setText(load.getSourceCityName());
            loadViewHolder.tvNameEndLoadFragmentLoad.setText(load.getGoalCityName());
//            loadViewHolder.tvWeightFragmentLoad.setText(String.format(" %s", load.getWeight()));
            loadViewHolder.tvCountSeenFragmentLoad.setText(String.format("  %s", load.getCountView()));
            loadViewHolder.tvStatusFragmentLoad.setText(load.getStep());
            loadViewHolder.tvTimeFragmentLoad.setText(load.getDateCreate());
            loadViewHolder.tvLoadNameFragmentLoad.setText(load.getTitle());

            loadViewHolder.tvCallFragmentLoad.setOnClickListener(v -> context.startActivities(
                    new Intent[]
                            {new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + load.getPhoneNumber()))}));

            holder.itemView.setOnClickListener(v -> interfaceLoadFragment.onOnclickItemLoadFragment(load));
        }
        else
        {
            HolderItemLastPaginate holderItemLastPaginate = (HolderItemLastPaginate) holder;
            if (showbtnTryAgain)
            {
                showbtnTryAgain = false;
                holderItemLastPaginate.itemView.setVisibility(View.VISIBLE);
                holderItemLastPaginate.itemView.setOnClickListener(v ->
                {
                    holderItemLastPaginate.itemView.setVisibility(View.GONE);
                    interfaceTryAgainPaginate.onResponseTryAgainPaginate();
                });
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return listLoads == null ? 0 : listLoads.size();
    }


    @Override
    public int getItemViewType(int position)
    {
        if (position == this.getItemCount() - 1)
        {
            return 2;
        }
        return 1;
    }

    public void addList(List<ModelListLoad> listLoads)
    {
        this.listLoads.addAll(listLoads);
        notifyDataSetChanged();
    }

    public void showBtnTryAgain()
    {
        this.showbtnTryAgain = true;
        notifyDataSetChanged();
    }

    static class LoadViewHolder extends RecyclerView.ViewHolder
    {

        private final ImageView ivFreightLoad;
        private final TextView tvTitleLoad, tvNameCarFragmentLoad, tvLoadNameFragmentLoad, tvStartLoadFragmentLoad;
        private final TextView tvNameEndLoadFragmentLoad, tvCallFragmentLoad, tvCountSeenFragmentLoad;
        private final Button btnTagFragmentLoad;

        private final ImageView imFragmentLoad;
        private final TextView tvStatusFragmentLoad;
        private final TextView tvTimeFragmentLoad;

        public LoadViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ivFreightLoad = itemView.findViewById(R.id.iv_freight_row_load);
            tvTitleLoad = itemView.findViewById(R.id.tv_title_row_frg_load);
            btnTagFragmentLoad = itemView.findViewById(R.id.btn_tag_row_load);
            imFragmentLoad = itemView.findViewById(R.id.im_item_row_rv_fragment_load);
            tvNameCarFragmentLoad = itemView.findViewById(R.id.tv_name_car_list_load);
            tvLoadNameFragmentLoad = itemView.findViewById(R.id.tv_item_row_name_load_rv_fragment_load);
            tvStartLoadFragmentLoad = itemView.findViewById(R.id.tv_name_start_load_row_load);
            tvNameEndLoadFragmentLoad = itemView.findViewById(R.id.tv_name_end_load_row_load);
//            tvWeightFragmentLoad = itemView.findViewById(R.id.tv_item_row_weight_rv_fragment_load);
            tvCallFragmentLoad = itemView.findViewById(R.id.tv_call_row_load);
            tvCountSeenFragmentLoad = itemView.findViewById(R.id.tv_item_row_count_seen_rv_fragment_load);
            tvStatusFragmentLoad = itemView.findViewById(R.id.tv_item_row_status_rv_fragment_load);
            tvTimeFragmentLoad = itemView.findViewById(R.id.tv_item_row_time_rv_fragment_load);
            tvTitleLoad.setSelected(true);
            tvNameCarFragmentLoad.setSelected(true);
        }
    }

    public class HolderItemLastPaginate extends RecyclerView.ViewHolder
    {
        public HolderItemLastPaginate(@NonNull View itemView)
        {
            super(itemView);
            itemView.setVisibility(View.GONE);
        }
    }
}

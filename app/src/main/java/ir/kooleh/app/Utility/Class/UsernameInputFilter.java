package ir.kooleh.app.Utility.Class;

import android.text.InputFilter;
import android.text.Spanned;

public class UsernameInputFilter implements InputFilter
{
    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//        StringBuilder builder = new StringBuilder(dest);
//        builder.replace(dstart, dend, source.subSequence(start, end).toString());
//
//        if (!builder.toString().matches("(?=.{0,20}$)(?![0-9.])(?!.*[.]{2})[a-zA-Z0-9.]*")) {
//            return "";
//        }
//
//        return source;
        StringBuilder filteredStringBuilder = new StringBuilder();
        filteredStringBuilder.replace(dstart, dend, source.subSequence(start, end).toString());
        if (filteredStringBuilder.toString().matches("(?=.{0,20}$)(?![0-9.])(?!.*[.]{2})[a-zA-Z0-9.]*"))
        {
            return filteredStringBuilder.toString();
        }
        return "";
    }
}

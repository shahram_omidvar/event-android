package ir.kooleh.app.Utility.Adapter.Layout.Alert;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Alert.InterfaceAlertShowList;

import ir.kooleh.app.Structure.Model.Alert.ModelAlertShowList;


public class AdapterAlertShowList extends RecyclerView.Adapter<AdapterAlertShowList.HolderAlertShowList>
{

    private final Context context;
    private List<ModelAlertShowList> listAlert;
    private final InterfaceAlertShowList.Adapter interfaceAlertShowList;

    public AdapterAlertShowList(Context context, InterfaceAlertShowList.Adapter interfaceAlertShowList)
    {

        this.context = context;
        this.interfaceAlertShowList = interfaceAlertShowList;
    }


    @NonNull
    @Override
    public HolderAlertShowList onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new AdapterAlertShowList.HolderAlertShowList(
                LayoutInflater.from(context).inflate(R.layout.item_row_select_item,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderAlertShowList holder, int position)
    {
        final ModelAlertShowList item = listAlert.get(position);
        holder.tvTiltSearchItem.setText(String.format("%s", item.getTitle()));

        if (item.getImage() != null && !item.getImage().equals(""))
            Glide.with(context).load(item.getImage()).
                    into(holder.ivSearchItem);


        holder.itemView.setOnClickListener(view -> interfaceAlertShowList.OnClickItemAlertShowDialog(item));
    }

    @Override
    public int getItemCount()
    {
        return listAlert == null ? 0 : listAlert.size();
    }


    public void setListAlert(List<ModelAlertShowList> listAlert)
    {
        this.listAlert = listAlert;
        notifyDataSetChanged();
    }

    static class HolderAlertShowList extends RecyclerView.ViewHolder
    {
        private final TextView tvTiltSearchItem;
        private final ImageView ivSearchItem;

        public HolderAlertShowList(@NonNull View itemView)
        {
            super(itemView);
            tvTiltSearchItem = itemView.findViewById(R.id.tv_item_row_select_item);
            ivSearchItem = itemView.findViewById(R.id.iv_item_row_select_item);

        }
    }
}

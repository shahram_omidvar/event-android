package ir.kooleh.app.Utility.Adapter.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;


import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.ListView.InterfaceView;

import ir.kooleh.app.Structure.Model.View.ModelOption;
import ir.kooleh.app.Structure.Model.View.ModelView;
import ir.kooleh.app.Structure.StaticValue.SvInputTypeValue;
import ir.kooleh.app.Structure.StaticValue.SvOptionView;
import ir.kooleh.app.View.ViewCustom.CEditText;


public class AdapterViewEditText extends RecyclerView.Adapter<AdapterViewEditText.ListViewEditTextViewHolder>
{
    private static List<ModelView.EditText> listViewEditText;

    public AdapterViewEditText(Context context, InterfaceView.EditText.AdapterEditText iAdapterEditText)
    {
    }


    @NonNull
    @Override
    public ListViewEditTextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ListViewEditTextViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_row_view_edit_text, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewEditTextViewHolder holder, @SuppressLint("RecyclerView") int position)
    {

        ModelView.EditText modelEditText = listViewEditText.get(position);

        // set Title
        holder.textInputLayout.setHint(modelEditText.getFaName());


        // set max length
        holder.textInputLayout.setCounterMaxLength(Integer.parseInt(modelEditText.getMaxLength()));


        // set required
        if (modelEditText.getRequired())
            holder.etEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_required, 0, 0, 0);
        else
            holder.etEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


        // set input type
        if (modelEditText.getInputType().equals(SvInputTypeValue.NUMBER_DECIMAL))
            holder.etEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);//for decimal numbers

        else if (modelEditText.getInputType().equals(SvInputTypeValue.Number_SIGEND))
        {
            holder.etEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED); //for positive or negative values
            holder.etEditText.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        }


        else if (modelEditText.getInputType().equals(SvInputTypeValue.STRING))
            holder.etEditText.setInputType(InputType.TYPE_CLASS_TEXT); //for String


        holder.etEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer.parseInt(modelEditText.getMaxLength()))});

        // set other option

        List<ModelOption> listOption = modelEditText.getListOption();
        for (ModelOption option : listOption)
        {

            // set Helper text
            if (option.getNameOption().equals(SvOptionView.HELPER_TEXT))
                holder.textInputLayout.setHelperText(option.getValue());


                // set padding

            else if (option.getNameOption().equals(SvOptionView.PADDING))
            {
                holder.etEditText.setPadding
                        (
                                Integer.parseInt(option.getValue()),
                                Integer.parseInt(option.getValue()),
                                Integer.parseInt(option.getValue()),
                                Integer.parseInt(option.getValue())
                        );
            }


            else if (option.getNameOption().equals(SvOptionView.PADDING_HORIZONTAL))
            {
                holder.etEditText.setPadding
                        (
                                Integer.parseInt(option.getValue()),
                                0,
                                Integer.parseInt(option.getValue()),
                                0
                        );
            }


            else if (option.getNameOption().equals(SvOptionView.PADDING_VERTICAL))
            {
                holder.etEditText.setPadding
                        (
                                0,
                                Integer.parseInt(option.getValue()),
                                0,
                                Integer.parseInt(option.getValue())
                        );
            }


        }


        holder.etEditText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                try
                {
                    listViewEditText.get(position).setUserInput(holder.etEditText.getText().toString());
                }
                catch (Exception e)
                {
                }

            }
        });

    }

    @Override
    public int getItemCount()
    {
        return listViewEditText == null ? 0 : listViewEditText.size();
    }


    public void setListEditTextView(List<ModelView.EditText> listViewEditText)
    {
        AdapterViewEditText.listViewEditText = listViewEditText;
        notifyDataSetChanged();
    }


    public String checkRequiredData()
    {
        for (ModelView.EditText editText : listViewEditText)
        {
            if (editText.getRequired())
            {
                if (editText.getUserInput() == null
                        || editText.getUserInput().trim().equals(""))
                    return "خطا لطفا " + editText.getFaName() + " را وارد کنید ";

            }

        }
        return "";
    }


    // view holder

    static class ListViewEditTextViewHolder extends RecyclerView.ViewHolder
    {


        TextInputLayout textInputLayout;
        CEditText etEditText;


        public ListViewEditTextViewHolder(@NonNull View itemView)
        {
            super(itemView);
            textInputLayout = itemView.findViewById(R.id.txt_input_view_edit_text);
            etEditText = itemView.findViewById(R.id.et_view_edit_text);

        }
    }
}



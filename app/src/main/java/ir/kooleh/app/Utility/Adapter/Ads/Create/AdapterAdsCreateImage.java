package ir.kooleh.app.Utility.Adapter.Ads.Create;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Model.Image.ModelListImage;


public class AdapterAdsCreateImage extends RecyclerView.Adapter<AdapterAdsCreateImage.AdsCreateImageHolder>
{
    private List<ModelListImage> listImage;
    private Context context;
    private InterfaceAds.IAdapterCreateAds iAdapterCreateAds;

    public AdapterAdsCreateImage(Context context, InterfaceAds.IAdapterCreateAds iAdapterCreateAds)
    {
        this.context = context;
        this.iAdapterCreateAds = iAdapterCreateAds;
    }


    @NonNull
    @Override
    public AdsCreateImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new AdsCreateImageHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_select_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdsCreateImageHolder holder, int position)
    {

        ModelListImage modelListImage = listImage.get(position);
        holder.ivImage.setImageResource(R.drawable.ic_image_holder);

        if (modelListImage.getModel().equals("camara") || modelListImage.getModel().equals("gallery"))
        {
            holder.ivImage.setImageBitmap(modelListImage.getBitmap());
        }
//        else
//        {
//            String urlImage = modelListImage.getUrl();
//            if (urlImage != null && urlImage.length() > 0)
//            {
//                holder.ivImage.setImageBitmap(modelListImage.getBitmap());
//            }
//        }

        holder.itemView.setOnClickListener(v -> iAdapterCreateAds.onClickAdapterListImage(modelListImage));
    }

    @Override
    public int getItemCount()
    {
        return listImage == null ? 0 : listImage.size();
    }


    public void setListCreateImage(List<ModelListImage> listImage)
    {
        this.listImage = listImage;
        notifyDataSetChanged();
    }

    static class AdsCreateImageHolder extends RecyclerView.ViewHolder
    {
        ImageView ivImage;

        public AdsCreateImageHolder(@NonNull View itemView)
        {
            super(itemView);
            ivImage = itemView.findViewById(R.id.iv_image_item_row_select_image);
        }
    }
}



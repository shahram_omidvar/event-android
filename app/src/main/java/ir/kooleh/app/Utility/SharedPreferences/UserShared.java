package ir.kooleh.app.Utility.SharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import ir.kooleh.app.Structure.Model.User.ModelUser;

public class UserShared
{
    private final String TAG_FIRST_NAME = "TAG_FIRST_NAME";
    private final String TAG_Last_Name = "TAG_Last_Name";
    private final String TAG_ANDROID_ID = "TAG_ANDROID_ID";
    private final String TAG_API_TOKEN = "TAG_API_TOKEN";
    private final String TAG_MODEL = "TAG_MODEL";
    private final String TAG_ID = "TAG_ID";
    private final String TAG_Full_Name = "TAG_Full_Name";
    private final String TAG_Phone_Number = "TAG_Phone_Number";
    private final String TAG_Username = "TAG_Username";
    private final String TAG_Image = "TAG_Image";
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;
    private final String SHARED_PREFERENCES_NAME = "UsernameInfo";


    private static final String TAG = "UserShared";

    public UserShared(Context context)
    {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public void deletePreferences(String file)
    {
        editor.clear();
        editor.commit();
        Log.i(TAG, "delete: " + file);
    }

    public boolean checkExist()
    {
        return (!getApiToken().equals("-1"));
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public void setAndroidID()
    {

        editor.putString(TAG_ANDROID_ID, Settings.Secure.ANDROID_ID);
        editor.commit();
    }


    public String getAndroidID()
    {

        String android_id = sharedPreferences.getString(TAG_ANDROID_ID, "-1");

        if (android_id == null || android_id.equals("-1"))
        {
            setAndroidID();
            android_id = sharedPreferences.getString(TAG_ANDROID_ID, "-1");
        }
        return android_id;

    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public String getApiToken()
    {
        return sharedPreferences.getString(TAG_API_TOKEN, "-1");
    }

    public void setApiToken(String apiToken)
    {
        editor.putString(TAG_API_TOKEN, apiToken);
        editor.commit();
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public String getModel()
    {
        return sharedPreferences.getString(TAG_MODEL, "-1");
    }

    public void setModel(String model)
    {
        editor.putString(TAG_MODEL, model);
        editor.commit();
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public int getID()
    {
        return Integer.parseInt(sharedPreferences.getString(TAG_ID, "-1"));
    }

    public void setID(String id)
    {
        editor.putString(TAG_ID, id);
        editor.commit();
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public String getFullName()
    {
        return sharedPreferences.getString(TAG_Full_Name, "-1");
    }

    public void setFullName(String fullName)
    {
        editor.putString(TAG_Full_Name, fullName);
        editor.commit();
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public void setFirstName(String firstName)
    {
        editor.putString(TAG_FIRST_NAME, firstName);
        editor.commit();
    }

    public String getFirstName()
    {
        return sharedPreferences.getString(TAG_FIRST_NAME, "-1");
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public void setLastName(String lastName)
    {
        editor.putString(TAG_Last_Name, lastName);
        editor.commit();
    }

    public String getLastName()
    {
        return sharedPreferences.getString(TAG_Last_Name, "-1");
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public void setPhoneNumber(String phoneNumber)
    {
        editor.putString(TAG_Phone_Number, phoneNumber);
        editor.commit();
    }

    public String getPhoneNumber()
    {
        return sharedPreferences.getString(TAG_Phone_Number, "-1");
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public void setUsername(String username)
    {
        editor.putString(TAG_Username, username);
        editor.commit();
    }

    public String getUsername()
    {
        return sharedPreferences.getString(TAG_Username, "-1");
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public void setImage(String image)
    {
        editor.putString(TAG_Image, image);
        editor.commit();
    }

    public String getImage()
    {
        return sharedPreferences.getString(TAG_Image, "");
    }


    //+-+-+-+-+--+-+-+-+-+-+-+-+-+-------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    public void setData(ModelUser user)
    {
        if (user.getApiToken() != null)
            setApiToken(user.getApiToken());
        setID(user.getId());
        setFullName(user.getFullName());
        setFirstName(user.getFirstName());
        setLastName(user.getLastName());
        setPhoneNumber(user.getPhoneNumber());
        setUsername(user.getUsername());
        if (user.getImage() != null)
            setImage(user.getImage() + "");

    }


}

package ir.kooleh.app.Utility.Adapter.Ads.Details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Ads.ModelAd;

public class AdapterAdDetailsSpecial extends RecyclerView.Adapter<AdapterAdDetailsSpecial.AdsDetailsSpecialHolder>
{
    private List<ModelAd.ListDataAd.SpecialsAd> listSpecialAds;
    private Context context;

    public AdapterAdDetailsSpecial(Context context)
    {
        this.context = context;
    }


    @NonNull
    @Override
    public AdsDetailsSpecialHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {

        return new AdsDetailsSpecialHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_special_ad_details, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull AdsDetailsSpecialHolder holder, int position)
    {
        if (listSpecialAds != null)
        {
            holder.itemNameTV.setText(listSpecialAds.get(position).getTopText());
            holder.itemValueTV.setText(listSpecialAds.get(position).getBottomText());
        }
    }

    @Override
    public int getItemCount()
    {

        return listSpecialAds == null ? 5 : listSpecialAds.size();

    }

    public void setListSpecialAds(List<ModelAd.ListDataAd.SpecialsAd> listSpecialAds)
    {
        this.listSpecialAds = listSpecialAds;
        notifyDataSetChanged();
    }

    static class AdsDetailsSpecialHolder extends RecyclerView.ViewHolder
    {

        TextView itemNameTV, itemValueTV;

        public AdsDetailsSpecialHolder(@NonNull View itemView)
        {
            super(itemView);
            itemNameTV = itemView.findViewById(R.id.tv_item_name_special_ads);
            itemValueTV = itemView.findViewById(R.id.tv_item_value_special_ads);
            itemNameTV.setSelected(true);
            itemValueTV.setSelected(true);
        }
    }
}



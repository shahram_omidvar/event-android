package ir.kooleh.app.Utility.Adapter.Comment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Comment.ApiComment;
import ir.kooleh.app.Structure.Interface.Comment.InterfaceComment;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCloseBottomSheetMoreComments;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Comment.ModelComment;
import ir.kooleh.app.Structure.Model.Comment.ModelUpdateVoteComment;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.View.Ui.Dialog.BottomSheetDialogMoreComments;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class AdapterCommentLoad extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements InterfaceComment
{
    private final Context context;
    private List<ModelComment> listComment = new ArrayList<>();
    ApiComment apiComment;
    ViewLoading viewLoading;

    private final String like = "1";
    private final String dsLike = "0";

    private boolean showbtnTryAgain;
    private boolean showIvCloseComment;
    private InterfaceTryAgainPaginate interfaceTryAgainPaginate;
    private InterfaceCloseBottomSheetMoreComments interfaceCloseBottomSheetMoreComments;


    public AdapterCommentLoad(Context context)
    {
        this.context = context;
        apiComment = new ApiComment(context);
        viewLoading = new ViewLoading(context);
        if (context instanceof InterfaceTryAgainPaginate)
        {
            this.interfaceTryAgainPaginate = (InterfaceTryAgainPaginate) context;
        }
    }

    public AdapterCommentLoad(BottomSheetDialogMoreComments bottomSheetDialogMoreComments, Context context)
    {
        this.context = context;
        apiComment = new ApiComment(context);
        viewLoading = new ViewLoading(context);
        this.interfaceTryAgainPaginate = bottomSheetDialogMoreComments;
        this.interfaceCloseBottomSheetMoreComments = bottomSheetDialogMoreComments;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == 1)
            return new HolderFirstItemMoreComments(LayoutInflater.from(context).inflate(R.layout.layout_first_item_rv_more_comments, parent, false));
        if (viewType == 2)
            return new HolderItemLastPaginate(LayoutInflater.from(context).inflate(R.layout.item_last_paginate, parent, false));
        return new CommentViewHolder(LayoutInflater.from(context).inflate(R.layout.item_row_comment_load, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof CommentViewHolder)
        {
            CommentViewHolder commentViewHolder = (CommentViewHolder) holder;
            ModelComment comment = listComment.get(position - 1);

            if (comment.getImageProfile() != null && comment.getImageProfile().length() > 0)
            {
                Glide.with(context).load(comment.getImageProfile()).
                        placeholder(R.drawable.ic_avatar).
                        into(commentViewHolder.cvUserProfile);
            }
            commentViewHolder.tvDate.setText(comment.getDate());
            commentViewHolder.tvDescription.setText(comment.getText());
            commentViewHolder.tvFullNameUser.setText(comment.getFullName());
            commentViewHolder.tvCountDisLike.setText(String.format("%s", comment.getAgainstCount()));
            commentViewHolder.tvCountLike.setText(String.format("%s", comment.getAgreeCount()));
            commentViewHolder.rateComment.setRating(comment.getPoint());

            commentViewHolder.ivLike.setOnClickListener(v ->
            {
                viewLoading.show();
                apiComment.setVoteComment(AdapterCommentLoad.this, comment.getId(), like, position);
            });
            commentViewHolder.ivDisLike.setOnClickListener(v ->
            {
                viewLoading.show();
                apiComment.setVoteComment(AdapterCommentLoad.this, comment.getId(), dsLike, position);
            });
        }
        else if (holder instanceof HolderItemLastPaginate)
        {
            HolderItemLastPaginate holderItemLastPaginate = (HolderItemLastPaginate) holder;
            if (showbtnTryAgain)
            {
                showbtnTryAgain = false;
                holderItemLastPaginate.itemView.setVisibility(View.VISIBLE);

                holderItemLastPaginate.itemView.setOnClickListener(v ->
                {
                    holderItemLastPaginate.itemView.setVisibility(View.GONE);
                    interfaceTryAgainPaginate.onResponseTryAgainPaginate();
                });
            }
        }
        else
        {
            HolderFirstItemMoreComments holderFirstItemMoreComments = (HolderFirstItemMoreComments) holder;
            if (showIvCloseComment)
            {
                holderFirstItemMoreComments.itemView.setVisibility(View.VISIBLE);
                holderFirstItemMoreComments.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                holderFirstItemMoreComments.ivCloseBottomSheet.setOnClickListener(v -> {
                    interfaceCloseBottomSheetMoreComments.onResponseCloseBottomSheetMoreComments();
                });

            }
        }
    }

    @Override
    public int getItemCount()
    {
        return listComment == null ? 0 : listComment.size() + 2;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == 0)
        {
            return 1;
        }
        if (position == this.getItemCount() - 1)
        {
            return 2;
        }
        return 3;
    }

    public void addList(List<ModelComment> listComment)
    {
        this.listComment.addAll(listComment);
        notifyDataSetChanged();
    }

    public void showBtnTryAgain()
    {
        this.showbtnTryAgain = true;
        notifyDataSetChanged();
    }

    public void showIvCloseComment()
    {
        this.showIvCloseComment = true;
        notifyDataSetChanged();
    }


    @Override
    public void onResponseListComment(boolean response, List<ModelComment> listComment, ModelPaginate modelPaginate, String message)
    {

    }

    @Override
    public void onResponseVoteComment(boolean response, ModelUpdateVoteComment.Result updateVoteComment, String message, int position)
    {
        viewLoading.close();
        if (response)
        {
            CustomToast customToast = new CustomToast(context);
            customToast.showSuccess(message);
            listComment.get(position - 1).setAgainstCount(updateVoteComment.getComment().getAgainstCount());
            listComment.get(position - 1).setAgreeCount(updateVoteComment.getComment().getAgreeCount());
            notifyDataSetChanged();
        }
    }

    @Override
    public void onResponseSavePointAndComment(boolean response, String message)
    {

    }

    static class CommentViewHolder extends RecyclerView.ViewHolder
    {
        ImageView cvUserProfile;
        CTextView tvFullNameUser, tvDate, tvDescription, tvCountLike, tvCountDisLike;
        ImageView ivLike, ivDisLike;
        RatingBar rateComment;

        public CommentViewHolder(@NonNull View itemView)
        {
            super(itemView);

            cvUserProfile = itemView.findViewById(R.id.iv_profile_user_rv_comment_load);
            tvFullNameUser = itemView.findViewById(R.id.tv_full_name_profile_item_rv_comment);
            tvDate = itemView.findViewById(R.id.tv_date_item_rv_comment);
            tvDescription = itemView.findViewById(R.id.tv_comment_item_rv_load);
            ivDisLike = itemView.findViewById(R.id.iv_dislike_item_rv_comment);
            ivLike = itemView.findViewById(R.id.iv_like_item_rv_comment);
            tvCountDisLike = itemView.findViewById(R.id.tv_count_dislike_rv_load);
            tvCountLike = itemView.findViewById(R.id.tv_count_like_rv_load);
            rateComment = itemView.findViewById(R.id.rate_item_rv_comment);
        }
    }

    public static class HolderItemLastPaginate extends RecyclerView.ViewHolder
    {

        public HolderItemLastPaginate(@NonNull View itemView)
        {
            super(itemView);
            itemView.setVisibility(View.GONE);
        }
    }

    public static class HolderFirstItemMoreComments extends RecyclerView.ViewHolder
    {
        private final ImageView ivCloseBottomSheet;
        public HolderFirstItemMoreComments(@NonNull View itemView)
        {
            super(itemView);
            ivCloseBottomSheet = itemView.findViewById(R.id.iv_close_bottom_sheet_first_item_rv_more_comments);
            itemView.setVisibility(View.GONE);
            itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
        }
    }
}

package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Model.Dashboard.ModelMyAd;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Activity.Ads.AdDetailsActivity;
import ir.kooleh.app.View.Ui.Activity.dashboard.AdDetailsChooseWinnerActivity;
import ir.kooleh.app.View.Ui.Fragment.FragmentMyAdAdStep;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterMyAdStep extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private final FragmentMyAdAdStep fragmentMyAdAdStep;
    private final Context context;
    private List<ModelMyAd> myAdList = new ArrayList<>();
    private final int adType, adStep;

    private boolean showbtnTryAgain;
    private final InterfaceTryAgainPaginate interfaceTryAgainPaginate;

    public AdapterMyAdStep(Context context, FragmentMyAdAdStep fragmentMyAdAdStep, int adType, int adStep)
    {
        this.fragmentMyAdAdStep = fragmentMyAdAdStep;
        this.context = context;
        this.adType = adType;
        this.adStep = adStep;
        this.interfaceTryAgainPaginate = fragmentMyAdAdStep;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == 2)
            return new HolderItemLastPaginate(LayoutInflater.from(context).inflate(R.layout.item_last_paginate, parent, false));
        return new HolderMyAdStep(LayoutInflater.from(context).inflate(R.layout.item_my_ad_step, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof HolderMyAdStep)
        {
            HolderMyAdStep holderMyAdStep = (HolderMyAdStep) holder;
            ModelMyAd modelMyAd = myAdList.get(position);
            holderMyAdStep.tvAdNumber.setText(modelMyAd.getLineOne());
            holderMyAdStep.tvAdTitle.setText(modelMyAd.getLineTwo());
            holderMyAdStep.tvAdExplain.setText(modelMyAd.getLineThere());
            holderMyAdStep.ivAdImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            holderMyAdStep.ivAdImage.setImageResource(R.drawable.img_name_logo_kooleh);

            if (modelMyAd.getImage() != null && modelMyAd.getImage().length() > 0)
            {
                holderMyAdStep.ivAdImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(context).load(modelMyAd.getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(holderMyAdStep.ivAdImage);
            }
            messageImageCountdownHandler(holderMyAdStep, modelMyAd.getMessageImage());
            Glide.with(context).load(modelMyAd.getTagIcon()).into(holderMyAdStep.ivTagMessageIcon);
            holderMyAdStep.tvAdDate.setText(modelMyAd.getTagMessageEnd());
            holderMyAdStep.tvTagMessage.setText(modelMyAd.getTagMessageCenter());

            try
            {
                holderMyAdStep.containerBelowAd.getBackground().setColorFilter(Color.parseColor(modelMyAd.getTagColor()), PorterDuff.Mode.SRC_ATOP);
            }
            catch (IllegalArgumentException iae)
            {
                holderMyAdStep.containerBelowAd.getBackground().setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
            }

            holder.itemView.setOnClickListener(v ->
            {
                Intent intent;
                if ((adType == 10 && adStep == 1) || (adType == 10 && adStep == 2))
                {
                    intent = new Intent(context, AdDetailsChooseWinnerActivity.class);
                    intent.putExtra(AdDetailsChooseWinnerActivity.Ad_ID, modelMyAd.getId());
                    intent.putExtra(AdDetailsChooseWinnerActivity.ADS_TITLE, modelMyAd.getLineTwo());
                    intent.putExtra(AdDetailsActivity.API_NAME, UrlAPI.GET_MY_AD);
                }
                else if (adType == 11)
                {
                    intent = new Intent(context, AdDetailsActivity.class);
                    intent.putExtra(AdDetailsChooseWinnerActivity.Ad_ID, modelMyAd.getId());
                    intent.putExtra(AdDetailsChooseWinnerActivity.ADS_TITLE, modelMyAd.getLineTwo());
                    intent.putExtra(AdDetailsActivity.API_NAME, UrlAPI.GET_MY_OFFER_AD);
                }
                else
                {
                    intent = new Intent(context, AdDetailsActivity.class);
                    intent.putExtra(AdDetailsActivity.Ad_ID, modelMyAd.getId());
                    intent.putExtra(AdDetailsActivity.ADS_TITLE, modelMyAd.getLineTwo());
                    intent.putExtra(AdDetailsActivity.API_NAME, UrlAPI.GET_MY_AD);
                }
                fragmentMyAdAdStep.startActivityForResult(intent, 1);
            });
        }
        else
        {
            HolderItemLastPaginate holderUserPackagesBottom = (HolderItemLastPaginate) holder;
            if (showbtnTryAgain)
            {
                showbtnTryAgain = false;
                holderUserPackagesBottom.itemView.setVisibility(View.VISIBLE);
                holderUserPackagesBottom.itemView.setOnClickListener(v ->
                {
                    holderUserPackagesBottom.itemView.setVisibility(View.GONE);
                    interfaceTryAgainPaginate.onResponseTryAgainPaginate();
                });
            }
        }

    }


    @Override
    public int getItemCount()
    {
        return myAdList == null ? 0 : myAdList.size() + 1;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == this.getItemCount() - 1)
        {
            return 2;
        }
        return 1;
    }

    public void showBtnTryAgain()
    {
        this.showbtnTryAgain = true;
        notifyDataSetChanged();
    }



    private void messageImageCountdownHandler(HolderMyAdStep holder, String messageImage)
    {
        if (!messageImage.equals("0") && NumberUtils.isNumeric(messageImage) && ((adType == 10 && adStep == 0) || (adType == 11 && adStep == 1)))
        {
            if (holder.countDownTimer != null)
            {
                holder.countDownTimer.cancel();
            }
            holder.countDownTimer = new CountDownTimer(Integer.parseInt(messageImage) * 1000L, 1000)
            {
                public void onTick(long millisUntilFinished)
                {
                    long seconds = millisUntilFinished / 1000;
                    long minutes = seconds / 60;
                    long hours = minutes / 60;
                    String strCountdownTime = String.format(Locale.ENGLISH, "%02d", hours % 24)
                            + ":" + String.format(Locale.ENGLISH, "%02d", minutes % 60)
                            + ":" + String.format(Locale.ENGLISH, "%02d", seconds % 60);
                    holder.tvMessageImage.setText(strCountdownTime);
                }

                public void onFinish()
                {
                    holder.tvMessageImage.setText("00:00:00");
                }
            }.start();
        }
        else if (messageImage.equals("") || messageImage.equals("0"))
        {
            holder.tvMessageImage.setVisibility(View.GONE);
        }
        else
        {
            holder.tvMessageImage.setText(messageImage);
        }
    }


    public void setList(List<ModelMyAd> myAdList)
    {
        this.myAdList.addAll(myAdList);
        notifyDataSetChanged();
    }



    public class HolderMyAdStep extends RecyclerView.ViewHolder
    {

        private final CTextView tvAdNumber, tvAdTitle, tvAdExplain;
        private final ImageView ivAdImage, ivTagMessageIcon;
        private final CTextView tvAdDate, tvMessageImage, tvTagMessage;
        private final LinearLayout containerBelowAd;
        private CountDownTimer countDownTimer;


        public HolderMyAdStep(@NonNull View itemView)
        {
            super(itemView);
            tvAdNumber = itemView.findViewById(R.id.tv_ad_number_my_ad_step);
            tvAdTitle = itemView.findViewById(R.id.tv_ad_title_my_ad_step);
            tvAdExplain = itemView.findViewById(R.id.tv_ad_explain_my_ad_step);
            ivAdImage = itemView.findViewById(R.id.iv_ad_image_my_ad_step);
            ivTagMessageIcon = itemView.findViewById(R.id.iv_tag_message_icon_my_ad_step);
            tvAdDate = itemView.findViewById(R.id.tv_ad_date_my_ad_step);
            tvMessageImage = itemView.findViewById(R.id.tv_ad_message_image_my_ad_step);
            tvTagMessage = itemView.findViewById(R.id.tv_tag_message_my_ad_step);
            containerBelowAd = itemView.findViewById(R.id.container_below_my_ad_step);
        }
    }

    public class HolderItemLastPaginate extends RecyclerView.ViewHolder
    {
        public HolderItemLastPaginate(@NonNull View itemView)
        {
            super(itemView);
            itemView.setVisibility(View.GONE);
        }
    }
}


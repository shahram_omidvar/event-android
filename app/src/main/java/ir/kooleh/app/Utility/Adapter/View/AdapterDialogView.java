package ir.kooleh.app.Utility.Adapter.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.ListView.InterfaceView;

import ir.kooleh.app.Structure.Model.View.ModelViewValue;

public class AdapterDialogView extends RecyclerView.Adapter<AdapterDialogView.HolderDialogView>
{
    private final Context context;
    private List<ModelViewValue> listValue;
    private final InterfaceView.Adapter.AlertDialogView interfaceAdapterAlertDialogView;

    public AdapterDialogView(Context context, InterfaceView.Adapter.AlertDialogView interfaceAdapterAlertDialogView)
    {
        this.context = context;
        this.interfaceAdapterAlertDialogView = interfaceAdapterAlertDialogView;
    }


    @NonNull
    @Override
    public AdapterDialogView.HolderDialogView onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new AdapterDialogView.HolderDialogView(
                LayoutInflater.from(context).inflate(R.layout.item_row_select_item,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterDialogView.HolderDialogView holder, int position)
    {
        final ModelViewValue value = listValue.get(position);
        holder.tvTiltSearchItem.setText(String.format("%s", value.getFaName()));

        if (value.getImage() != null && !value.getImage().equals(""))
        {
            Glide.with(context).load(value.getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(holder.ivSearchItem);
        }
        else
        {
            holder.ivSearchItem.setVisibility(View.GONE);
        }

        if (value.getChild() == null || value.getChild().size() == 0)
            holder.tvTiltSearchItem.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        else
            holder.tvTiltSearchItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_next_black, 0, 0, 0);


        holder.itemView.setOnClickListener(v -> interfaceAdapterAlertDialogView.onClickItemAdapterDialogView(value));

    }

    @Override
    public int getItemCount()
    {
        return listValue == null ? 0 : listValue.size();
    }


    public void setListDialogView(List<ModelViewValue> listValue)
    {
        this.listValue = listValue;
        notifyDataSetChanged();
    }

    static class HolderDialogView extends RecyclerView.ViewHolder
    {
        private final TextView tvTiltSearchItem;
        private final ImageView ivSearchItem;

        public HolderDialogView(@NonNull View itemView)
        {
            super(itemView);
            tvTiltSearchItem = itemView.findViewById(R.id.tv_item_row_select_item);
            ivSearchItem = itemView.findViewById(R.id.iv_item_row_select_item);

        }
    }

}

package ir.kooleh.app.Utility.Class;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EmDurationToast;

public class CustomToast
{
    private Toast toast;
    private TextView tvTitle;
    private ImageView imCustomToast;

    public CustomToast(Context context)
    {
        inflateView(context);
    }

    private void inflateView(Context context)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View layout = layoutInflater.inflate(R.layout.layout_custom_toast, null);
        tvTitle = layout.findViewById(R.id.tv_title_custom_toast);
        imCustomToast = layout.findViewById(R.id.im_status_custom_toast);
        toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 250);
    }

    private void show(String message)
    {
        tvTitle.setText(message);
        toast.show();
        garbageCollect();
    }

    public void showSuccess(String message)
    {
        imCustomToast.setImageResource(R.drawable.ic_success);
        show(message);
    }

    public void showError(String message)
    {
        imCustomToast.setImageResource(R.drawable.ic_error);
        show(message);
    }


    public void showWarnig(String message)
    {
        imCustomToast.setImageResource(R.drawable.ic_warning);
        show(message);
    }


    public void showInfo(String message)
    {
        imCustomToast.setImageResource(R.drawable.ic_info);
        show(message);
    }


    public void showHelp(String message)
    {
        imCustomToast.setImageResource(R.drawable.ic_help);
        show(message);
    }

    public void garbageCollect()
    {
        toast = null;
        tvTitle = null;
        imCustomToast = null;
    }


    public void setDuration(EmDurationToast duration)
    {
        int du = 6;
        if (duration == EmDurationToast.SHORT)
            du = 3;

        else if (duration == EmDurationToast.MEDIUM)
            du = 6;

        else if (duration == EmDurationToast.LONG)
            du = 9;

        else if (duration == EmDurationToast.VeryLONG)
            du = 12;

        toast.setDuration(du);
    }

}

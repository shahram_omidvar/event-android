package ir.kooleh.app.Utility.Adapter.Ads.Details;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceSlider;
import ir.kooleh.app.Structure.Model.Slider.ModelSlider;

public class AdapterSliderAdDetails extends RecyclerView.Adapter<AdapterSliderAdDetails.HolderSliderAdDetails>
{

    private List<ModelSlider> listSlider;
    private final Activity activity;
    private InterfaceSlider interfaceSlider;

    public AdapterSliderAdDetails(Activity activity, List<ModelSlider> listSlider)
    {
        this.activity = activity;
        this.listSlider = listSlider;
        interfaceSlider = (InterfaceSlider) activity;
    }

    @NonNull
    @Override
    public HolderSliderAdDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderSliderAdDetails(LayoutInflater.from(activity).inflate(R.layout.item_slider, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull AdapterSliderAdDetails.HolderSliderAdDetails holder, int position)
    {

        Glide.with(activity).load(listSlider.get(position).getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(holder.sliderIV);

        holder.itemView.setOnClickListener(v -> interfaceSlider.onClickedSlider(position));
    }

    @Override
    public int getItemCount()
    {
        return listSlider == null ? 0 : listSlider.size();
    }

    public void setListSlider(List<ModelSlider> listSlider)
    {
        this.listSlider = listSlider;
        notifyDataSetChanged();
    }

    static class HolderSliderAdDetails extends RecyclerView.ViewHolder
    {
        ImageView sliderIV;

        public HolderSliderAdDetails(View itemView)
        {
            super(itemView);
            sliderIV = itemView.findViewById(R.id.iv_item_slider);
        }
    }
}

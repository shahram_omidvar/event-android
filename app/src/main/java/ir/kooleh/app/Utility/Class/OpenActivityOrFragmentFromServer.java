package ir.kooleh.app.Utility.Class;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import ir.kooleh.app.View.Ui.Fragment.FragmentLoad;

public class OpenActivityOrFragmentFromServer
{


    public static void openActivity(Context context, String name)
    {
        try
        {
            context.startActivity(new Intent(context, Class.forName(name)));
        }
        catch (Exception e)
        {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public static void openFragment(Context context, String name, int idParent)
    {
        loadFragment(context, instantiate(context, name), idParent);
    }

    public static void openWeb(Context context, String URL)
    {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL)));
    }

    private static Fragment instantiate(Context context, String name)
    {
        try
        {

            return (Fragment) Class.forName(name).newInstance();
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException ignored)
        {
        }

        return null;
    }

    private static void loadFragment(Context context, Fragment fragment, int idParent)
    {
        if (fragment == null)
            fragment = new FragmentLoad("", "", "", "");
        AppCompatActivity activity = (AppCompatActivity) context;
        activity.getSupportFragmentManager().beginTransaction()
                .replace(idParent, fragment)
                .commit();
    }
}

package ir.kooleh.app.Utility.Class;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.paginate.recycler.LoadingListItemCreator;

import ir.kooleh.app.R;

public class CustomLayoutPaginate implements LoadingListItemCreator
{
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.custom_layout_paginate_loading, parent, false);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    }


    public class VH extends RecyclerView.ViewHolder
    {
        public VH(@NonNull View itemView)
        {
            super(itemView);
        }
    }

}

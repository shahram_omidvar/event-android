package ir.kooleh.app.Utility.Adapter.Wallet;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelLogWallet;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterLogWallet extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private final Activity activity;
    private List<ModelLogWallet> logWalletList = new ArrayList<>();

    private InterfaceTryAgainPaginate interfaceTryAgainPaginate;
    private boolean showbtnTryAgain = false;

    public AdapterLogWallet(Activity activity)
    {
        this.activity = activity;
        if (activity instanceof InterfaceTryAgainPaginate)
        {
            this.interfaceTryAgainPaginate = (InterfaceTryAgainPaginate) activity;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == 2)
            return new HolderItemLastPaginate(LayoutInflater.from(activity).inflate(R.layout.item_last_paginate, parent, false));
        return new HolderLogWallet(LayoutInflater.from(activity).inflate(R.layout.item_log_wallet, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof HolderLogWallet)
        {
            HolderLogWallet holderLogWallet = (HolderLogWallet) holder;
            ModelLogWallet modelLogWallet = logWalletList.get(position);
            holderLogWallet.tvAction.setText(modelLogWallet.getAction());
            holderLogWallet.tvPrice.setText("هزینه: " + NumberUtils.digitDivider(modelLogWallet.getPrice()) + " ریال");

            if (modelLogWallet.getExplain() == null || modelLogWallet.getExplain().equals(""))
                holderLogWallet.tvExplain.setVisibility(View.GONE);
            else
                holderLogWallet.tvExplain.setText(modelLogWallet.getExplain());

            holderLogWallet.tvDate.setText(modelLogWallet.getCreatedTime());
        }
        else
        {
            HolderItemLastPaginate holderUserPackagesBottom = (HolderItemLastPaginate) holder;
            if (showbtnTryAgain)
            {
                showbtnTryAgain = false;
                holderUserPackagesBottom.itemView.setVisibility(View.VISIBLE);
                holderUserPackagesBottom.itemView.setOnClickListener(v ->
                {
                    holderUserPackagesBottom.itemView.setVisibility(View.GONE);
                    interfaceTryAgainPaginate.onResponseTryAgainPaginate();
                });
            }
        }

    }

    @Override
    public int getItemCount()
    {
        return logWalletList == null ? 0 : logWalletList.size() + 1;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == this.getItemCount() - 1)
        {
            return 2;
        }
        return 1;
    }


    public void setList(List<ModelLogWallet> logWalletList)
    {
        this.logWalletList = logWalletList;
        notifyDataSetChanged();
    }

    public void addList(List<ModelLogWallet> logWalletList)
    {
        this.logWalletList.addAll(logWalletList);
        notifyDataSetChanged();
    }

    public void showBtnTryAgain()
    {
        this.showbtnTryAgain = true;
        notifyDataSetChanged();
    }

    public class HolderLogWallet extends RecyclerView.ViewHolder
    {
        private final CTextView tvAction;
        private final CTextView tvPrice;
        private final CTextView tvExplain;
        private final CTextView tvDate;
        public HolderLogWallet(@NonNull View itemView)
        {
            super(itemView);
            tvAction = itemView.findViewById(R.id.tv_action_log_wallet);
            tvPrice = itemView.findViewById(R.id.tv_price_log_wallet);
            tvExplain = itemView.findViewById(R.id.tv_explain_log_wallet);
            tvDate = itemView.findViewById(R.id.tv_date_log_wallet);
        }
    }

    public class HolderItemLastPaginate extends RecyclerView.ViewHolder
    {
        public HolderItemLastPaginate(@NonNull View itemView)
        {
            super(itemView);
            itemView.setVisibility(View.GONE);
        }
    }
}

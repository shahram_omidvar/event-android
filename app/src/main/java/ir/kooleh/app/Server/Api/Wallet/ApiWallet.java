package ir.kooleh.app.Server.Api.Wallet;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceWallet;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelInfoWallet;
import ir.kooleh.app.Structure.Model.Wallet.ModelLogWallet;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiWallet
{
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;
    private static final String TAG = "Api Wallet";

    public ApiWallet(Context context)
    {
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void getInfoWalletuser(InterfaceWallet.Wallet.IntGetInfoWalletUser intGetInfoWalletUser)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_INFO_WALLET_USER,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetInfoWalletUser.onResponseGetInfoWalletUser(
                                    true,
                                    jsonResponse.getString("message"),
                                    gson.fromJson(
                                            jsonResponse.getJSONObject("result").toString(),
                                            ModelInfoWallet.class)
                            );
                        }
                        else
                        {
                            intGetInfoWalletUser.onResponseGetInfoWalletUser(
                                    false,
                                    jsonResponse.getString("message"),
                                    null
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetInfoWalletUser.onResponseGetInfoWalletUser(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null
                        );
                    }

                },
                error ->
                        intGetInfoWalletUser.onResponseGetInfoWalletUser(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                return null;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void getLogsWalletUser(InterfaceWallet.Wallet.IntGetLogsWalletUser intGetLogsWalletUser, int nextPage, int itemsPerPage)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_LOGS_WALLET_USER,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetLogsWalletUser.onResponseGetLogsWalletUser(
                                    true,
                                    jsonResponse.getString("message"),
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONArray("data").toString(),
                                            ModelLogWallet[].class)),
                                    gson.fromJson(jsonResponse.getJSONObject("result").getJSONObject("paginate").toString(),
                                            ModelPaginate.class)
                            );
                        }
                        else
                        {
                            intGetLogsWalletUser.onResponseGetLogsWalletUser(
                                    false,
                                    jsonResponse.getString("message"),
                                    null,
                                    null
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetLogsWalletUser.onResponseGetLogsWalletUser(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null,
                                null
                        );
                    }

                },
                error ->
                        intGetLogsWalletUser.onResponseGetLogsWalletUser(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null,
                                null
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }
}

package ir.kooleh.app.Server.Api.Driver;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Driver.InterfaceDriver;
import ir.kooleh.app.Structure.Model.Driver.ModelDriver;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiDriver
{

    private static final String TAG = "ApiDriver";
    private final Context context;
    private final ControllerError controllerError;
    private final Gson gson;
    private UserShared userShared;

    public ApiDriver(Context context)
    {
        this.context = context;
        controllerError = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    // get info driver


    // دریافت اطلاعات پروفایل
    public void getDriver(InterfaceDriver.Info interfaceDriver)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_DRIVER,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            interfaceDriver.onResponseGetDriver(
                                    true,
                                    gson.fromJson(jsonResponse.getString("result"), ModelDriver.class),
                                    jsonResponse.getString("message")

                            );
                        }
                        else
                        {
                            interfaceDriver.onResponseGetDriver(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceDriver.onResponseGetDriver(

                                false,
                                null,
                                controllerError.checkException(TAG, ex)
                        );
                    }

                }
                ,
                error -> interfaceDriver.onResponseGetDriver(

                        false,
                        null,
                        controllerError.checkVolleyError(error)
                )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


}

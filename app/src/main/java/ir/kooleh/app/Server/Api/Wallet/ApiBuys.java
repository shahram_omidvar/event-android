package ir.kooleh.app.Server.Api.Wallet;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Enum.EnumWalletModule;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceBuys;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelBuy;
import ir.kooleh.app.Structure.Model.Wallet.ModelGateway;
import ir.kooleh.app.Structure.Model.Wallet.ModelTransactionDetails;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiBuys
{
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;
    private static final String TAG = "Api Wallet";

    public ApiBuys(Context context)
    {
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }



    public void BuyApp(InterfaceBuys.Buys.IntBuyApp intBuyApp, String gatewayName, String model, String price, String packageOrAdID)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.BUY_APP,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intBuyApp.onResponseBuyApp(
                                    true,
                                    jsonResponse.getJSONObject("result").getString("token_one"),
                                    jsonResponse.getJSONObject("result").getString("token_two"),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intBuyApp.onResponseBuyApp(
                                    false,
                                    "",
                                    "",
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intBuyApp.onResponseBuyApp(
                                false,
                                "",
                                "",
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intBuyApp.onResponseBuyApp(
                                false,
                                "",
                                "",
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("model", model);
                params.put("gateway_name", gatewayName);

                switch (model)
                {
                    case EnumWalletModule.BuyModels.MODEL_WALLET:
                        params.put("price", price);
                        break;
                    case EnumWalletModule.BuyModels.MODEL_AD:
                    case EnumWalletModule.BuyModels.MODEL_PACKAGE:
                        params.put("id", packageOrAdID + "");
                        break;
                    case EnumWalletModule.BuyModels.MODEL_BILL:
                        params.put("user_receipt_id", packageOrAdID + "");
                        break;
                }
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void BuyWalletApp(InterfaceBuys.Buys.IntBuyWalletApp intBuyWalletApp, String model, String packageOrAdID)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.BUY_WALLET_APP,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intBuyWalletApp.onResponseBuyWalletApp(
                                    true,
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intBuyWalletApp.onResponseBuyWalletApp(
                                    false,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intBuyWalletApp.onResponseBuyWalletApp(
                                false,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intBuyWalletApp.onResponseBuyWalletApp(
                                false,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("model", model);
                params.put("id", packageOrAdID + "");
                // TODO: 8/29/2021 it gives error "خطایی رخ داد"
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void getBuysUser(InterfaceBuys.Buys.IntGetBuysUser intGetBuysUser, int nextPage, int itemsPerPage, String model)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_BUYS_USER,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetBuysUser.onResponseGetBuysUser(
                                    true,
                                    jsonResponse.getString("message"),
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONArray("data").toString(),
                                            ModelBuy[].class)),
                                            gson.fromJson
                                                    (jsonResponse.getJSONObject("result").getJSONObject("paginate").toString(),
                                                    ModelPaginate.class)
                            );
                        }
                        else
                        {
                            intGetBuysUser.onResponseGetBuysUser(
                                    false,
                                    jsonResponse.getString("message"),
                                    null,
                                    null
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetBuysUser.onResponseGetBuysUser(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null,
                                null
                        );
                    }

                },
                error ->
                        intGetBuysUser.onResponseGetBuysUser(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null,
                                null
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("model", model);
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void getListGateway(InterfaceBuys.Buys.IntGetListGateWay intGetListGateWay)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_LIST_GATEWAY,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetListGateWay.onResponseGetListGateWay(
                                    true,
                                    jsonResponse.getString("message"),
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONArray("result").toString(),
                                            ModelGateway[].class))
                            );
                        }
                        else
                        {
                            intGetListGateWay.onResponseGetListGateWay(
                                    false,
                                    jsonResponse.getString("message"),
                                    null
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetListGateWay.onResponseGetListGateWay(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null
                        );
                    }

                },
                error ->
                        intGetListGateWay.onResponseGetListGateWay(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                return null;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public void getTransctionDetails(InterfaceBuys.Buys.IntGetTransactionDetails intGetTransactionDetails, String transactionID)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_TRANSACTION,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetTransactionDetails.onResponseGetTransactionDetails(
                                    true,
                                    jsonResponse.getString("message"),
                                    gson.fromJson(
                                            jsonResponse.getJSONObject("result").toString(),
                                            ModelTransactionDetails.class)
                            );
                        }
                        else
                        {
                            intGetTransactionDetails.onResponseGetTransactionDetails(
                                    false,
                                    jsonResponse.getString("message"),
                                    null
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetTransactionDetails.onResponseGetTransactionDetails(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null
                        );
                    }

                },
                error ->
                        intGetTransactionDetails.onResponseGetTransactionDetails(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String, String> params = new HashMap<>();
                params.put("transaction_id", transactionID);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }
}

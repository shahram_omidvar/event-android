package ir.kooleh.app.Server.Api.User;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.User.InterfaceUser;
import ir.kooleh.app.Structure.Model.Base.ModelResponse;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiUser
{

    private static final String TAG = "ApiLogin";

    private final Gson gson;
    private final UserShared userShared;
    private final ControllerError controllerError;

    public ApiUser(Context context)
    {
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
        controllerError = new ControllerError(context);

    }


    // ثبت نام
    public void checkExistsUser(InterfaceUser.checkExistsUser inCheckExistsUser, String phoneNumber)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.CHECK_EXISTS_USER,
                response ->
                {
                    try
                    {
                        ModelResponse modelResponse = gson.fromJson(response, ModelResponse.class);
                        if (modelResponse.getOk())
                        {
                            inCheckExistsUser.onResponseCheckExistsUser
                                    (
                                            modelResponse.getOk(),
                                            modelResponse.getMessage(),
                                            modelResponse.getResult().getCommend(),
                                            modelResponse.getResult().getLengthCode()
                                    );
                        }
                        else
                        {
                            inCheckExistsUser.onResponseCheckExistsUser
                                    (
                                            modelResponse.getOk(),
                                            modelResponse.getMessage(),
                                            null,
                                            -1
                                    );

                        }


                    }
                    catch (Exception ex)
                    {
                        inCheckExistsUser.onResponseCheckExistsUser
                                (
                                        false,
                                        controllerError.checkException(TAG, ex),
                                        null,
                                        -1
                                );
                    }

                }
                ,
                error -> inCheckExistsUser.onResponseCheckExistsUser
                        (
                                false,
                                controllerError.checkVolleyError(error),
                                null, -1
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("phone_number", phoneNumber);
                return params;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);

    }

}

package ir.kooleh.app.Server.Api.Ads;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Model.Ads.ModelAd;
import ir.kooleh.app.Structure.Model.Ads.ModelAdsKey;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Image.ModelListImage;
import ir.kooleh.app.Structure.Model.Search.ModelSearchAds;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiAds
{
    private static final String TAG = "ApiSearchAds";
    private final Context context;
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;

    public ApiAds(Context context)
    {
        this.context = context;
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void searchAds(final InterfaceAds.Search interfaceSearchAds, String text, final String categoryId)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.SEARCH_ADS,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            ModelSearchAds searchAds = gson.fromJson(jsonResponse.getJSONObject("result").toString(), ModelSearchAds.class);

                            interfaceSearchAds.onResponseSearchAds(
                                    true,
                                    searchAds,
                                    jsonResponse.getString("message")

                            );
                        }
                        else
                        {
                            interfaceSearchAds.onResponseSearchAds(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceSearchAds.onResponseSearchAds
                                (
                                        false,
                                        null,
                                        controlEroAndExp.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error -> interfaceSearchAds.onResponseSearchAds
                        (
                                false,
                                null,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();

                if (text != null && text.length() >= 2)
                    params.put("text", text);

                if (categoryId != null)
                    params.put("category_id", categoryId);

                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void getAd(final InterfaceAds.IInfo iInfo, String adID, String apiName)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                apiName,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            iInfo.onResponseGetAds(
                                    true,
                                    gson.fromJson(jsonResponse.getString("result"), ModelAd.class),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {

                            iInfo.onResponseGetAds
                                    (
                                            false,
                                            null,
                                            jsonResponse.getString("message")
                                    );

                        }

//com.google.gson.JsonSyntaxException: java.lang.IllegalStateException: Expected BEGIN_OBJECT but was BEGIN_ARRAY at line 1 column 13 path $.slider[0]
                    }
                    catch (Exception ex)
                    {
                        iInfo.onResponseGetAds
                                (
                                        false,
                                        null,
                                        controlEroAndExp.checkException(TAG, ex)
                                );

                    }
                },
                error ->
                {
                    iInfo.onResponseGetAds
                            (
                                    false,
                                    null,
                                    controlEroAndExp.checkVolleyError(error)
                            );
                }
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("ad_id", adID);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void createAds
            (
                    final InterfaceAds.Server iAdsServer,
                    String idCategoryAd,
                    String title,
                    String explain,
                    String typeAdID,
                    String auctionCountdown,
                    String jsonItem,
                    List<ModelListImage> listImage
            )
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.CREATE_AD,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        iAdsServer.onResponseCreateAd(
                                jsonResponse.getBoolean("ok"),
                                jsonResponse.getString("message")
                        );

                    }
                    catch (Exception ex)
                    {
                        iAdsServer.onResponseCreateAd
                                (
                                        false,
                                        controlEroAndExp.checkException(TAG, ex)
                                );

                    }
                },
                error ->
                {
                    iAdsServer.onResponseCreateAd
                            (
                                    false,

                                    controlEroAndExp.checkVolleyError(error)
                            );
                }
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("category_ad_id", idCategoryAd);
                params.put("story", title);
                params.put("explain", explain);
                params.put("type_ad_id", typeAdID);
                params.put("count_down", auctionCountdown);
                params.put("json_item", jsonItem);

                if (listImage != null && listImage.size() > 0)
                {
                    int i = 1;
                    for (ModelListImage image : listImage)
                    {
                        if (!image.getModel().equals("no_set") && image.getBase64() != null)
                        {
                            params.put("image_" + i, image.getBase64());
                            i++;
                        }
                    }

                }

                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void getAdsKey(final InterfaceAds.Fragment.AdsKey.ServerAdsKey serverAdsKey, String key, int nextPage, int itemsPerPage)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_ADS_KEY,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            JSONObject jsonResult = new JSONObject(jsonResponse.getJSONObject("result").toString());

                            ModelAdsKey modelAdsKey = gson.fromJson(
                                            jsonResult.getJSONObject("data").toString(), ModelAdsKey.class);

                            serverAdsKey.onResponseAdsKey
                                    (
                                            true,
                                            modelAdsKey,
                                            gson.fromJson(jsonResult.getJSONObject("paginate").toString(), ModelPaginate.class),
                                            jsonResponse.getString("message")
                                    );
                        }
                        else
                        {

                            serverAdsKey.onResponseAdsKey
                                    (
                                            false,
                                            null,
                                            null,
                                            jsonResponse.getString("message")
                                    );

                        }

                    }
                    catch (Exception ex)
                    {
                        serverAdsKey.onResponseAdsKey
                                (
                                        false,
                                        null,
                                        null,
                                        controlEroAndExp.checkException(TAG, ex)
                                );

                    }
                },
                error ->
                {
                    serverAdsKey.onResponseAdsKey
                            (
                                    false,
                                    null,
                                    null,
                                    controlEroAndExp.checkVolleyError(error)
                            );
                }
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("key", key + "");
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

}

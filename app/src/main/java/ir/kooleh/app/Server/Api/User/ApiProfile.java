package ir.kooleh.app.Server.Api.User;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.User.InterfaceProfile;
import ir.kooleh.app.Structure.Model.Driver.ModelDriver;
import ir.kooleh.app.Structure.Model.Freight.ModelFreight;
import ir.kooleh.app.Structure.Model.User.Profile.ModelProfile;
import ir.kooleh.app.Structure.Model.User.Profile.ModelProfileAllTypeUser;
import ir.kooleh.app.Structure.Model.User.Profile.ModelTargetProfile;
import ir.kooleh.app.Structure.StaticValue.SVTypeUser;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiProfile
{

    private static final String TAG = "ApiProfile";
    private final Context context;
    private final ControllerError controllerError;
    private final Gson gson;
    private UserShared userShared;

    public ApiProfile(Context context)
    {
        this.context = context;
        controllerError = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    // دریافت اطلاعات پروفایل
    public void getProfileFragment(InterfaceProfile.FragmentProfile interfaceProfile)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GETP_ROFILE_User,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            interfaceProfile.onResponseGetProfileFragment(
                                    true,
                                    gson.fromJson(jsonResponse.getString("result"), ModelTargetProfile.class),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            interfaceProfile.onResponseGetProfileFragment(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceProfile.onResponseGetProfileFragment
                                (
                                        false,
                                        null,
                                        controllerError.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error -> interfaceProfile.onResponseGetProfileFragment
                        (
                                false,
                                null,
                                controllerError.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                userShared = new UserShared(context);
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userShared.getID() + "");
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    // دریافت اطلاعات  پروفایل
    public void getProfile(InterfaceProfile.ActivityProfile interfaceProfile)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_ADV_PROFILE,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            ModelProfile data = gson.fromJson(jsonResponse.getString("result"), ModelProfile.class);
                            interfaceProfile.onResponseGetProfile
                                    (
                                            true,
                                            data,
                                            jsonResponse.getString("message")
                                    );
                        }
                        else
                        {
                            interfaceProfile.onResponseGetProfile
                                    (
                                            false,
                                            null,
                                            jsonResponse.getString("message")
                                    );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceProfile.onResponseGetProfile
                                (
                                        false,
                                        null,
                                        controllerError.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error -> interfaceProfile.onResponseGetProfile
                        (
                                false,
                                null,
                                controllerError.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                userShared = new UserShared(context);
                Map<String, String> params = new HashMap<>();
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    // آپدیت پروفایل
    public void updateProfile(InterfaceProfile.ActivityProfile interfaceProfile, ModelProfileAllTypeUser profile)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.UPDATE_PROFILE,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        interfaceProfile.onResponseUpdateProfile
                                (
                                        jsonResponse.getBoolean("ok"),
                                        jsonResponse.getString("message")
                                );

                    }
                    catch (Exception ex)
                    {
                        interfaceProfile.onResponseUpdateProfile
                                (
                                        false,
                                        controllerError.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error -> interfaceProfile.onResponseUpdateProfile
                        (
                                false,
                                controllerError.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                ModelProfile baseProfile = profile.getBaseModelProfile();
                Map<String, String> params = new HashMap<>();
                params.put("type_user", baseProfile.getTypeUser());
                params.put("first_name", baseProfile.getFirstName());
                params.put("last_name", baseProfile.getLastName());
                params.put("username", baseProfile.getUsername());
                params.put("smart_phone", baseProfile.getSmartPhone() + "");
                params.put("notification", baseProfile.getNotification() + "");
//                if (baseProfile.getProfileImage() != null && !baseProfile.getProfileImage().equals(""))
//                {
                    params.put("image", baseProfile.getProfileImage());
//                }

                // driver

                if (baseProfile.getTypeUser().equals(SVTypeUser.DRIVER))
                {
                    ModelDriver driver = profile.getModelDriver();
                    params.put("smart_code", driver.getSmartCode() + "");
                    params.put("father_name", driver.getFatherName());
                    params.put("city_id", driver.getCityId() + "");
                    params.put("fleet_id", driver.getFleetId() + "");
                }
                else if (baseProfile.getTypeUser().equals(SVTypeUser.FREIGHT))
                {
                    ModelFreight freight = profile.getModelFreight();
                    params.put("name", freight.getName());
                    params.put("register_number", freight.getRegisterNumber());
                    params.put("name_operator", freight.getManagerName());
                    params.put("address", freight.getAddress());
                    params.put("phone", freight.getPhone());
                    params.put("city_id", freight.getCityID() + "");
                }
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }
}

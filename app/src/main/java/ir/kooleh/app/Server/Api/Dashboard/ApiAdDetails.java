package ir.kooleh.app.Server.Api.Dashboard;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Structure.Model.Ads.ModelAd;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiAdDetails
{
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;
    private static final String TAG = "ApiDashboard";

    public ApiAdDetails(Context context)
    {
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void updateStepAd(InterfaceDashboard.Apis.IntUpdateStepAd intUpdateStepAd, String adID, String stepID, String actionID, String explain)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.UPDATE_STEP_AD,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intUpdateStepAd.onResponseUpdateStepAd(
                                    true,
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intUpdateStepAd.onResponseUpdateStepAd(
                                    false,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intUpdateStepAd.onResponseUpdateStepAd(
                                false,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intUpdateStepAd.onResponseUpdateStepAd(
                                false,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("ad_id", adID);
                params.put("step_id", stepID);
                params.put("action_id", actionID);
                params.put("explain", explain);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void setActionAd(InterfaceDashboard.Apis.IntSetActionAd intSetActionAd, String adID, String explain, String command)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.SET_ACTION_AD,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intSetActionAd.onResponseSetActionAd(
                                    true,
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intSetActionAd.onResponseSetActionAd(
                                    false,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intSetActionAd.onResponseSetActionAd(
                                false,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intSetActionAd.onResponseSetActionAd(
                                false,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("ad_id", adID);
                params.put("explain", explain);
                params.put("command", command);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void showInfoAuctionOwner(InterfaceDashboard.Apis.IntShowInfoAuctionOwner intShowInfoAuctionOwner, String adID, String url)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            String layoutType = jsonResponse.getJSONObject("result").getJSONObject("layout").getString("layout");
                            if (layoutType.equals("payment"))
                            {
                                intShowInfoAuctionOwner.onResponseShowInfoAuctionOwnerPayment(
                                        true,
                                        jsonResponse.getJSONObject("result").getJSONObject("layout").getString("message"),
                                        jsonResponse.getJSONObject("result").getJSONObject("layout").getString("price"),
                                        jsonResponse.getString("message")
                                );
                            }
                            else
                            {
                                intShowInfoAuctionOwner.onResponseShowInfoAuctionOwnerShow(
                                        true,
                                        gson.fromJson(jsonResponse.getJSONObject("result").getString("data"), ModelAd.class),
                                        jsonResponse.getString("message")
                                );
                            }

                        }
                        else
                        {
                            intShowInfoAuctionOwner.onResponseShowInfoAuctionOwnerPayment(
                                    false,
                                    "",
                                    "0",
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intShowInfoAuctionOwner.onResponseShowInfoAuctionOwnerPayment(
                                false,
                                "",
                                "0",
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intShowInfoAuctionOwner.onResponseShowInfoAuctionOwnerPayment(
                                false,
                                "",
                                "0",
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("ad_id", adID);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }
}

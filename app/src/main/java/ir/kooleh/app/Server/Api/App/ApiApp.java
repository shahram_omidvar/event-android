package ir.kooleh.app.Server.Api.App;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.App.InterfaceApp;

import ir.kooleh.app.Structure.Interface.App.InterfaceVersion;
import ir.kooleh.app.Structure.Model.App.ModelApp;

import ir.kooleh.app.Structure.Model.App.ModelInfoAppDevice;
import ir.kooleh.app.Structure.Model.App.ModelInfoVersion;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiApp
{

    private static final String TAG = "ApiApp";
    private final Context context;
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;

    public ApiApp(Context context)
    {
        this.context = context;
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    // دریافت اطلاعات سرویس
    public void getInfoApp(final InterfaceApp.ServerApp iServerApp)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_INFO_APP,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {


                            iServerApp.onResponseInfoApp
                                    (
                                            true,
                                            gson.fromJson(jsonResponse.getJSONObject("result").toString(), ModelApp.class),
                                            jsonResponse.getString("message")
                                    );
                        }
                        else
                        {
                            iServerApp.onResponseInfoApp
                                    (
                                            false,
                                            null,
                                            jsonResponse.getString("message")
                                    );
                        }


                    }
                    catch (Exception ex)
                    {
                        iServerApp.onResponseInfoApp
                                (
                                        false,
                                        null,
                                        controlEroAndExp.checkException(TAG, ex)
                                );
                    }

                }
                ,

                error -> iServerApp.onResponseInfoApp
                        (
                                false,
                                null,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )


        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                return new HashMap<>();
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void getInfoVersion(final InterfaceVersion.IntInfoVersion intInfoVersion, final ModelInfoAppDevice modelInfoAppDevice)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_INFO_VERSION,
                response ->
                {

                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intInfoVersion.onResponseInfoVersion(
                                    true,
                                    jsonResponse.getString("message"),
                                    gson.fromJson(jsonResponse.getJSONObject("result").toString(),
                                            ModelInfoVersion.class)
                            );

                        }
                        else
                        {
                            intInfoVersion.onResponseInfoVersion(
                                    false,
                                    jsonResponse.getString("message"),
                            null
                            );
                        }


                    }
                    catch (Exception ex)
                    {
                        intInfoVersion.onResponseInfoVersion(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null
                        );
                    }

                }
                , error ->
                intInfoVersion.onResponseInfoVersion(
                        false,
                        controlEroAndExp.checkVolleyError(error),
                        null
                )
        )
        {
            @Override
            public Map<String, String> getHeaders()
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("device_model", modelInfoAppDevice.getModel());
                params.put("version_install", modelInfoAppDevice.getVersionCode() + "");
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

}

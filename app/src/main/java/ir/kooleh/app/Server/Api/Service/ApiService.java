package ir.kooleh.app.Server.Api.Service;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Service.InterfaceService;
import ir.kooleh.app.Structure.Model.Service.ModelIndexService;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiService
{

    private static final String TAG = "ApiService";
    private Context context;
    private final ControllerError controlEroAndExp;
    private Gson gson;
    private UserShared userShared;

    public ApiService(Context context)
    {
        this.context = context;
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    // دریافت اطلاعات سرویس
    public void getIndexService(final InterfaceService.FragmentService interfaceService)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.SERVICE,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            ModelIndexService indexService =
                                    gson.fromJson(jsonResponse.getJSONObject("result").toString(), ModelIndexService.class);

                            interfaceService.onResponseIndexData(
                                    true,
                                    indexService,
                                    jsonResponse.getString("message")

                            );
                        }
                        else
                        {
                            interfaceService.onResponseIndexData(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceService.onResponseIndexData
                                (
                                        false,
                                        null,
                                        controlEroAndExp.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error -> interfaceService.onResponseIndexData
                        (
                                false,
                                null,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

}

package ir.kooleh.app.Server.Api.Fleet;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Fleet.InterfaceFleet;
import ir.kooleh.app.Structure.Model.Fleet.ModelFleet;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiFleet
{
    private static final String TAG = "ApiFleet";
    private final Context context;
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;

    public ApiFleet(Context context)
    {
        this.context = context;
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void getListFleet(final InterfaceFleet.ServerFleet interfaceFleet, final int parentId)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_LIST_FLEET,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            List<ModelFleet> listFleet =
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONArray("result").toString(),
                                            ModelFleet[].class));
                            interfaceFleet.onResponseListFleet(
                                    true,
                                    listFleet,
                                    jsonResponse.getString("message")

                            );
                        }
                        else
                        {
                            interfaceFleet.onResponseListFleet(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceFleet.onResponseListFleet
                                (
                                        false,
                                        null,
                                        controlEroAndExp.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error -> interfaceFleet.onResponseListFleet
                        (
                                false,
                                null,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();


                if (parentId >= 1)
                    params.put("parent_id", parentId + "");

                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


}

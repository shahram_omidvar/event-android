package ir.kooleh.app.Server.Api.auth;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;

import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Enum.EnumListCommendApi;
import ir.kooleh.app.Structure.Interface.Auth.InterfaceCode;

import ir.kooleh.app.Structure.Model.Base.ModelResponse;
import ir.kooleh.app.Structure.Model.User.ModelUser;
import ir.kooleh.app.Utility.Class.ControllerError;

import ir.kooleh.app.Utility.Volley.Controller;

public class ApiCode
{
    private static final String TAG = "ApiCheckCodeAndUser";
    private final ControllerError controllerError;
    private final Gson gson;


    public ApiCode(Activity activity)
    {
        controllerError = new ControllerError(activity);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
    }


    //  تایید کد ارسالی به سرور
    public void VerifyCode(InterfaceCode.VerifyCode interfaceVerifyCode,
                           String phoneNumber,
                           String code,
                           String type
    )
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.VERIFY_CODE,
                response ->
                {

                    try
                    {

                        ModelResponse modelResponse = gson.fromJson(response, ModelResponse.class);

                        if (modelResponse.getOk())
                        {
                            ModelUser user = null;
                            if (modelResponse.getResult().getCommend().equals(EnumListCommendApi.Home))
                            {
                                JSONObject jsonResponse = new JSONObject(response);
                                user = gson.fromJson(jsonResponse.getJSONObject("result").getString("user"), ModelUser.class);
                            }

                            interfaceVerifyCode.onResponseVerifyCode
                                    (
                                            true,
                                            modelResponse.getMessage(),
                                            modelResponse.getResult().getCommend(),
                                            user
                                    );
                        }

                        else
                        {
                            interfaceVerifyCode.onResponseVerifyCode(
                                    false,
                                    modelResponse.getMessage(),
                                    "error",
                                    null
                            );
                        }


                    } catch (Exception ex)
                    {
                        interfaceVerifyCode.onResponseVerifyCode(
                                false,
                                controllerError.checkException(TAG, ex),
                                "error",
                                null
                        );
                    }

                }
                ,
                error -> interfaceVerifyCode.onResponseVerifyCode
                        (
                                false,
                                controllerError.checkVolleyError(error),
                                "error",
                                null
                        )


        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("phone_number", phoneNumber);
                params.put("code", code);
                params.put("type", type);
                return params;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);

    }


    public void SendCode(InterfaceCode.SendCode interfaceSendCode, String phoneNumber, String type)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.SEND_CODE,
                response ->
                {
                    try
                    {
                        ModelResponse modelResponse = gson.fromJson(response, ModelResponse.class);

                        if (modelResponse.getOk())
                        {
                            interfaceSendCode.onResponseSendCode
                                    (
                                            true,
                                            modelResponse.getMessage(),
                                            modelResponse.getResult().getCommend(),
                                            modelResponse.getResult().getLengthCode()
                                    );
                        }

                        else
                        {
                            interfaceSendCode.onResponseSendCode(
                                    false,
                                    modelResponse.getMessage(),
                                    "error",
                                    -1
                            );
                        }


                    } catch (Exception ex)
                    {
                        interfaceSendCode.onResponseSendCode
                                (
                                        false,
                                        controllerError.checkException(TAG, ex),
                                        null,
                                        0
                                );
                    }

                }
                ,
                error -> interfaceSendCode.onResponseSendCode
                        (
                                false,
                                controllerError.checkVolleyError(error),
                                null,
                                0
                        )


        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("phone_number", phoneNumber);
                params.put("type", type);
                return params;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);

    }

}

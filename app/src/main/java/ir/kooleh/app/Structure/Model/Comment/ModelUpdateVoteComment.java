package ir.kooleh.app.Structure.Model.Comment;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ModelUpdateVoteComment
{

    @SerializedName("ok")
    @Expose
    private Boolean ok;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Boolean getOk()
    {
        return ok;
    }

    public void setOk(Boolean ok)
    {
        this.ok = ok;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Result getResult()
    {
        return result;
    }

    public void setResult(Result result)
    {
        this.result = result;
    }

    public Integer getStatus()
    {
        return status;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public class Result
    {

        @SerializedName("action")
        @Expose
        private Boolean action;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("comment")
        @Expose
        private ModelComment comment;

        public Boolean getAction()
        {
            return action;
        }

        public void setAction(Boolean action)
        {
            this.action = action;
        }

        public String getMessage()
        {
            return message;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }

        public ModelComment getComment()
        {
            return comment;
        }

        public void setComment(ModelComment comment)
        {
            this.comment = comment;
        }


    }
}





package ir.kooleh.app.Structure.Model.View;

import java.util.List;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ModelView
{

    @SerializedName("edit_text")
    @Expose
    private List<EditText> listEditText = null;

    @SerializedName("button")
    @Expose
    private List<Button> listButton = null;

    @SerializedName("other")
    @Expose
    private List<Other> listOther = null;


    @SerializedName("list_type_ad")
    @Expose
    private List<ModelViewValue> listTypeAds = null;



    public List<EditText> getListEditText()
    {
        return listEditText;
    }

    public void setListEditText(List<EditText> listEditText)
    {
        this.listEditText = listEditText;
    }


    public List<Button> getListButton()
    {
        return listButton;
    }

    public void setListButton(List<Button> listButton)
    {
        this.listButton = listButton;
    }


    public List<Other> getListOther()
    {
        return listOther;
    }

    public void setListOther(List<Other> listOther)
    {
        this.listOther = listOther;
    }

    public List<ModelViewValue> getListTypeAds() { return listTypeAds; }

    public void setListTypeAds(List<ModelViewValue> listTypeAds) { this.listTypeAds = listTypeAds; }


    public class Button
    {


        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fa_name")
        @Expose
        private String faName;
        @SerializedName("view")
        @Expose
        private String view;
        @SerializedName("input_type")
        @Expose
        private String inputType;
        @SerializedName("required")
        @Expose
        private Boolean required;
        @SerializedName("max_length")
        @Expose
        private String maxLength;
        @SerializedName("min_length")
        @Expose
        private String minLength;

        @SerializedName("list_option")
        @Expose
        private List<ModelOption> listOption = null;
        @SerializedName("list_value")
        @Expose
        private List<ModelViewValue> modelViewValue = null;

        private String userInput;

        private String value;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public String getFaName()
        {
            return faName;
        }

        public void setFaName (String faName) { this.faName = faName; }

        public String getView()
        {
            return view;
        }

        public void setView(String view)
        {
            this.view = view;
        }

        public String getInputType()
        {
            return inputType;
        }

        public void setInputType(String inputType)
        {
            this.inputType = inputType;
        }

        public Boolean getRequired()
        {
            return required;
        }

        public void setRequired(Boolean required)
        {
            this.required = required;
        }

        public List<ModelOption> getListOption()
        {
            return listOption;
        }

        public void setListOption(List<ModelOption> listOption)
        {
            this.listOption = listOption;
        }

        public List<ModelViewValue> getListValue()
        {
            return modelViewValue;
        }

        public void setListValue(List<ModelViewValue> modelViewValue)
        {
            this.modelViewValue = modelViewValue;
        }


        public String getUserInput()
        {
            return userInput;
        }

        public void setUserInput(String userInput)
        {
            this.userInput = userInput;
        }

        public String getMaxLength()
        {
            return maxLength;
        }

        public void setMaxLength(String maxLength)
        {
            this.maxLength = maxLength;
        }

        public String getMinLength()
        {
            return minLength;
        }

        public void setMinLength(String minLength)
        {
            this.minLength = minLength;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }
    }

    public class EditText
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fa_name")
        @Expose
        private String faName;
        @SerializedName("view")
        @Expose
        private String view;
        @SerializedName("input_type")
        @Expose
        private String inputType;
        @SerializedName("required")
        @Expose
        private Boolean required;
        @SerializedName("max_length")
        @Expose
        private String maxLength;
        @SerializedName("min_length")
        @Expose
        private String minLength;
        @SerializedName("list_option")
        @Expose
        private List<ModelOption> listOption = null;
        @SerializedName("list_value")
        @Expose
        private List<ModelViewValue> modelViewValue = null;


        private String userInput;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public String getFaName()
        {
            return faName;
        }

        public void setFaName(String faName)
        {
            this.faName = faName;
        }

        public String getView()
        {
            return view;
        }

        public void setView(String view)
        {
            this.view = view;
        }

        public String getInputType()
        {
            return inputType;
        }

        public void setInputType(String inputType)
        {
            this.inputType = inputType;
        }

        public Boolean getRequired()
        {
            return required;
        }

        public void setRequired(Boolean required)
        {
            this.required = required;
        }

        public List<ModelOption> getListOption()
        {
            return listOption;
        }

        public void setListOption(List<ModelOption> listOption)
        {
            this.listOption = listOption;
        }

        public List<ModelViewValue> getListValue()
        {
            return modelViewValue;
        }

        public void setListValue(List<ModelViewValue> modelViewValue)
        {
            this.modelViewValue = modelViewValue;
        }

        public String getUserInput()
        {
            return userInput;
        }

        public void setUserInput(String userInput)
        {
            this.userInput = userInput;
        }

        public String getMaxLength()
        {
            return maxLength;
        }

        public void setMaxLength(String maxLength)
        {
            this.maxLength = maxLength;
        }

        public String getMinLength()
        {
            return minLength;
        }

        public void setMinLength(String minLength)
        {
            this.minLength = minLength;
        }
    }


    public class Other
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("view")
        @Expose
        private String view;
        @SerializedName("input")
        @Expose
        private String input;
        @SerializedName("required")
        @Expose
        private Boolean required;
        @SerializedName("list_option")
        @Expose
        private List<ModelOption> listOption = null;
        @SerializedName("list_value")
        @Expose
        private List<ModelViewValue> modelViewValue = null;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getTitle()
        {
            return title;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }

        public String getView()
        {
            return view;
        }

        public void setView(String view)
        {
            this.view = view;
        }

        public String getInput()
        {
            return input;
        }

        public void setInput(String input)
        {
            this.input = input;
        }

        public Boolean getRequired()
        {
            return required;
        }

        public void setRequired(Boolean required)
        {
            this.required = required;
        }

        public List<ModelOption> getListOption()
        {
            return listOption;
        }

        public void setListOption(List<ModelOption> listOption)
        {
            this.listOption = listOption;
        }

        public List<ModelViewValue> getListValue()
        {
            return modelViewValue;
        }

        public void setListValue(List<ModelViewValue> modelViewValue)
        {
            this.modelViewValue = modelViewValue;
        }


    }

}




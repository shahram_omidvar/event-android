
package ir.kooleh.app.Structure.Model.MultiSelection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ModelMultiSelection
{

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("key")
    @Expose
    private String key;

    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("parent_id")
    @Expose
    private String parentId;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Object getImage()
    {
        return image;
    }

    public void setImage(Object image)
    {
        this.image = image;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }


    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }
}




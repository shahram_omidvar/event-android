
package ir.kooleh.app.Structure.Model.Load;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelListLoad
{
    
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title_master_load")
    @Expose
    private String titleMasterLoad;
    @SerializedName("image_master_load")
    @Expose
    private String imageMasterLoad;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("source_city_name")
    @Expose
    private String sourceCityName;
    @SerializedName("goal_city_name")
    @Expose
    private String goalCityName;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("count_view")
    @Expose
    private String countView;
    @SerializedName("step")
    @Expose
    private String step;
    @SerializedName("date_create")
    @Expose
    private String dateCreate;
    @SerializedName("tag_info")
    @Expose
    private TagInfo tagInfo;
    @SerializedName("fleet_info")
    @Expose
    private FleetInfo fleetInfo;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getTitleMasterLoad() { return titleMasterLoad; }

    public void setTitleMasterLoad(String titleMasterLoad) { this.titleMasterLoad = titleMasterLoad; }

    public String getImageMasterLoad() { return imageMasterLoad; }

    public void setImageMasterLoad(String imageMasterLoad) { this.imageMasterLoad = imageMasterLoad; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getSourceCityName() { return sourceCityName; }

    public void setSourceCityName(String sourceCityName) { this.sourceCityName = sourceCityName; }

    public String getGoalCityName() { return goalCityName; }

    public void setGoalCityName(String goalCityName) { this.goalCityName = goalCityName; }

    public String getWeight() { return weight; }

    public void setWeight(String weight) { this.weight = weight; }

    public String getPhoneNumber() { return phoneNumber; }

    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

    public String getCountView() { return countView; }

    public void setCountView(String countView) { this.countView = countView; }

    public String getStep() { return step; }

    public void setStep(String step) { this.step = step; }

    public String getDateCreate() { return dateCreate; }

    public void setDateCreate(String dateCreate) { this.dateCreate = dateCreate; }

    public TagInfo getTagInfo() { return tagInfo; }

    public void setTagInfo(TagInfo tagInfo) { this.tagInfo = tagInfo; }

    public FleetInfo getFleetInfo() { return fleetInfo; }

    public void setFleetInfo(FleetInfo fleetInfo) { this.fleetInfo = fleetInfo; }


    public class FleetInfo
    {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;

        public String getTitle() { return title; }

        public void setTitle(String title) { this.title = title; }

        public String getImage() { return image; }

        public void setImage(String image) { this.image = image; }
    }


    public class TagInfo
    {
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("color_title")
        @Expose
        private String colorTitle;
        @SerializedName("color_back")
        @Expose
        private String colorBack;

        public String getTitle() { return title; }

        public void setTitle(String title) { this.title = title; }

        public String getColorTitle() { return colorTitle; }

        public void setColorTitle(String colorTitle) { this.colorTitle = colorTitle; }

        public String getColorBack() { return colorBack; }

        public void setColorBack(String colorBack) { this.colorBack = colorBack; }
    }
}








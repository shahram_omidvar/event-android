package ir.kooleh.app.Structure.Interface.Bill;

import java.util.List;

import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.Structure.Model.Bills.ModelListSuggestion;

public class InterfaceOfferType
{
    public static class OfferType
    {
        public interface IntGetOfferType
        {
            void onResponseGetOfferType(boolean response, String message, List<ModelListSuggestion> modelListSuggestions);
        }
    }
}

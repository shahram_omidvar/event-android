package ir.kooleh.app.Structure.ListView;

import java.util.List;

import ir.kooleh.app.Structure.Model.View.ModelView;
import ir.kooleh.app.Structure.Model.View.ModelViewValue;

public class InterfaceView
{

    public static class EditText
    {
        public interface AdapterEditText
        {
//            void onClickItemViewEditText();
        }

    }


    public static class Button
    {
        public interface AdapterButton
        {
//            void onClickItemViewButton(String btnTitle, String btnValue);
        }

    }

    public static class Adapter
    {

        public interface AlertDialogView
        {
            void onClickItemAdapterDialogView(ModelViewValue modelViewValue);
        }


    }


    public static class AlertDialog
    {

        public interface AlertDialogView
        {
            void onResponseAlertDialogView(ModelViewValue value,int requestCodeDialogView, String choosenSequence);
        }


    }


    public interface Server
    {
        void onResponseListView(boolean response, ModelView modelListView, String message);
        void onResponseListViewValue(boolean response, List<ModelViewValue> modelViewValueList, String message, String model, String title);
    }


}

package ir.kooleh.app.Structure.Model.Image;

import android.graphics.Bitmap;

public class ModelListImage
{
    private int id;
    private String url;
    private String model;
    private String base64;
    private Bitmap bitmap;
    private String localPath = "";


    public ModelListImage(int id, String url, String model, String base64)
    {
        this.id = id;
        this.url = url;
        this.model = model;
        this.base64 = base64;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getBase64()
    {
        return base64;
    }

    public void setBase64(String base64)
    {
        this.base64 = base64;
    }

    public Bitmap getBitmap()
    {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap)
    {
        this.bitmap = bitmap;
    }

    public String getLocalPath() { return localPath; }

    public void setLocalPath(String localPath) { this.localPath = localPath; }
}

package ir.kooleh.app.Structure.Model.Freight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelFreight
{
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("register_number")
    @Expose
    private String registerNumber;
    @SerializedName("name_operator")
    @Expose
    private String managerName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("city_id")
    @Expose
    private String cityID;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("status")
    @Expose
    private String status;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getRegisterNumber() { return registerNumber; }

    public void setRegisterNumber(String registerNumber) { this.registerNumber = registerNumber; }

    public String getManagerName() { return managerName; }

    public void setManagerName(String managerName) { this.managerName = managerName; }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getCityID()
    {
        return cityID;
    }

    public void setCityID(String cityID)
    {
        this.cityID = cityID;
    }

    public String getCityName()
    {
        return cityName;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}

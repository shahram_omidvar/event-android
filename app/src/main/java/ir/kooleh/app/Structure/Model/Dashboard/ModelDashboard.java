package ir.kooleh.app.Structure.Model.Dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.kooleh.app.Structure.Model.Service.ModelIndexService;

public class ModelDashboard
{
    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("dashboard_data")
    @Expose
    private Data dashboardData;
    @SerializedName("list_ad")
    @Expose
    private List<ModelMostViewedDashboard> mostViewedDashboardList;
    @SerializedName("money")
    @Expose
    private Money money;

    public Profile getProfile() { return profile; }

    public void setProfile(Profile profile) { this.profile = profile; }

    public Data getDashboardData() { return dashboardData; }

    public void setDashboardData(Data dashboardData) { this.dashboardData = dashboardData; }

    public List<ModelMostViewedDashboard> getMostViewedDashboardList() { return mostViewedDashboardList; }

    public void setMostViewedDashboardList(List<ModelMostViewedDashboard> mostViewedDashboardList) { this.mostViewedDashboardList = mostViewedDashboardList; }

    public Money getMoney()
    {
        return money;
    }

    public void setMoney(Money money)
    {
        this.money = money;
    }

    public static class Profile
    {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("full_name")
        @Expose
        private String fullName;
        @SerializedName("explain")
        @Expose
        private String explain;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("wallet")
        @Expose
        private String wallet;
        @SerializedName("rate")
        @Expose
        private String rate;

        public String getId() { return id; }

        public void setId(String id) { this.id = id; }

        public String getFullName() { return fullName; }

        public void setFullName(String fullName) { this.fullName = fullName; }

        public String getExplain() { return explain; }

        public void setExplain(String explain) { this.explain = explain; }

        public String getImage() { return image; }

        public void setImage(String image) { this.image = image; }

        public String getWallet() { return wallet; }

        public void setWallet(String wallet) { this.wallet = wallet; }

        public String getRate() { return rate; }

        public void setRate(String rate) { this.rate = rate; }
    }

    public static class Data
    {
        @SerializedName("list_btn")
        @Expose
        private List<DashboardBtns> dashboardBtns;
        @SerializedName("list_slider")
        @Expose
        private List<ModelIndexService.Slider> DashboardSliderList;


        public List<DashboardBtns> getDashboardBtns() { return dashboardBtns; }

        public void setDashboardBtns(List<DashboardBtns> dashboardBtns) { }

        public List<ModelIndexService.Slider> getDashboardSliderList() { return DashboardSliderList; }

        public void setDashboardSliderList(List<ModelIndexService.Slider> dashboardSliderList) { DashboardSliderList = dashboardSliderList; }

        public static class DashboardBtns
        {
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("icon")
            @Expose
            private String icon;
            @SerializedName("action")
            @Expose
            private String action;
            @SerializedName("value")
            @Expose
            private String value;

            public String getTitle() { return title; }

            public void setTitle(String title) { this.title = title; }

            public String getIcon() { return icon; }

            public void setIcon(String icon) { this.icon = icon;}

            public String getAction() { return action; }

            public void setAction(String action) { this.action = action; }

            public String getValue() { return value; }

            public void setValue(String value) { this.value = value; }
        }

    }

    public static class Money
    {
        @SerializedName("wallet")
        @Expose
        private String wallet;
        @SerializedName("package")
        @Expose
        private String packages;
        @SerializedName("transaction")
        @Expose
        private String transaction;
        @SerializedName("buy_other")
        @Expose
        private String buyOther;
        @SerializedName("buy_bill")
        @Expose
        private String buyBill;

        public String getWallet()
        {
            return wallet;
        }

        public void setWallet(String wallet)
        {
            this.wallet = wallet;
        }

        public String getPackages()
        {
            return packages;
        }

        public void setPackages(String packages)
        {
            this.packages = packages;
        }

        public String getTransaction()
        {
            return transaction;
        }

        public void setTransaction(String transaction)
        {
            this.transaction = transaction;
        }

        public String getBuyOther()
        {
            return buyOther;
        }

        public void setBuyOther(String buy)
        {
            this.buyOther = buyOther;
        }

        public String getBuyBill() { return buyBill; }

        public void setBuyBill(String buyBill) { this.buyBill = buyBill; }
    }


}

package ir.kooleh.app.Structure.Interface.View;

public class InterfacePickers
{
    public interface IntCountdownPickerDialogResult
    {
        void onReponseCountdownPickerDialog(String strTimeAmount);
    }

    public interface IntTimePickerDialogResult
    {
        void onReponseTimePickerDialog(String strTime);
    }
}

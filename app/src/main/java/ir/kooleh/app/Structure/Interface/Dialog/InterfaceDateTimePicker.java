package ir.kooleh.app.Structure.Interface.Dialog;

import ir.kooleh.app.Structure.Enum.EnTypeDateTime;


public interface InterfaceDateTimePicker
{
    void onResponseDateTime(EnTypeDateTime enTypeDateTime, String value);
}

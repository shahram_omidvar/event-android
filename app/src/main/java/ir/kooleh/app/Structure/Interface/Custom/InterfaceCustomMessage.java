package ir.kooleh.app.Structure.Interface.Custom;


import ir.kooleh.app.Structure.Enum.EnCustomMessage;

public interface InterfaceCustomMessage
{
    void onClickCustomDialog(EnCustomMessage enCustomMessage);
}

package ir.kooleh.app.Structure.Enum;

//برای دریافت پریمژن ها
public enum ListPermission
{
    CAMERA,
    INTERNET,
    RECORD_AUDIO,
    RECEIVE_SMS,
    WRITE_EXTERNAL_STORAGE,
    READ_EXTERNAL_STORAGE,
    ACCESS_MEDIA_LOCATION
}

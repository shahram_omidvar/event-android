
package ir.kooleh.app.Structure.Model.Alert;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelAlertShowList
{


    public ModelAlertShowList()
    {

    }

    public ModelAlertShowList(int id, String title, int parentId, String image)
    {
        setId(id);
        setTitle(title);
        setParentId(parentId);
        setImage(image);
    }


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public Integer getParentId()
    {
        return parentId;
    }

    public void setParentId(Integer parentId)
    {
        this.parentId = parentId;
    }

}


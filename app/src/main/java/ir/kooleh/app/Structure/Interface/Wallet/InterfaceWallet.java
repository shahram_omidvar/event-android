package ir.kooleh.app.Structure.Interface.Wallet;

import java.util.List;

import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelInfoWallet;
import ir.kooleh.app.Structure.Model.Wallet.ModelLogWallet;

public class InterfaceWallet
{

    public static class Wallet
    {

        public interface IntGetInfoWalletUser
        {
            void onResponseGetInfoWalletUser(boolean response, String message, ModelInfoWallet infoWallet);
        }
        public interface IntGetLogsWalletUser
        {
            void onResponseGetLogsWalletUser(boolean response, String message, List<ModelLogWallet> logWalletList, ModelPaginate modelPaginate);
        }
    }

}

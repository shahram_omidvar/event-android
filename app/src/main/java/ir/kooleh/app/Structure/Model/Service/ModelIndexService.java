package ir.kooleh.app.Structure.Model.Service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelIndexService
{

    @SerializedName("slider")
    @Expose
    private List<Slider> slider = null;
    @SerializedName("service_square")
    @Expose
    private List<ServiceSquare> serviceSquareList = null;
    @SerializedName("service_horizontal")
    @Expose
    private List<ServiceHorizontal> serviceHorizontalList = null;


    public List<Slider> getSlider()
    {
        return slider;
    }

    public void setSlider(List<Slider> slider)
    {
        this.slider = slider;
    }

    public List<ServiceSquare> getServiceSquareList()
    {
        return serviceSquareList;
    }

    public void setServiceSquareList(List<ServiceSquare> serviceSquareList)
    {
        this.serviceSquareList = serviceSquareList;
    }

    public List<ServiceHorizontal> getServiceHorizontalList()
    {
        return serviceHorizontalList;
    }

    public void setServiceHorizontalList(List<ServiceHorizontal> serviceHorizontalList)
    {
        this.serviceHorizontalList = serviceHorizontalList;
    }

    public class ServiceSquare
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("action")
        @Expose
        private String action;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("story")
        @Expose
        private String story;
        @SerializedName("image")
        @Expose
        private String image;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getAction()
        {
            return action;
        }

        public void setAction(String action)
        {
            this.action = action;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }

        public String getStory()
        {
            return story;
        }

        public void setStory(String story)
        {
            this.story = story;
        }

        public String getImage()
        {
            return image;
        }

        public void setImage(String image)
        {
            this.image = image;
        }

    }


    public class Slider
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("action")
        @Expose
        private String action;
        @SerializedName("value")
        @Expose
        private String value;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getImage()
        {
            return image;
        }

        public void setImage(String image)
        {
            this.image = image;
        }

        public String getText()
        {
            return text;
        }

        public void setText(String text)
        {
            this.text = text;
        }

        public String getAction()
        {
            return action;
        }

        public void setAction(String action)
        {
            this.action = action;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }

    }

    public class ServiceHorizontal
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("action")
        @Expose
        private String action;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("story")
        @Expose
        private String story;
        @SerializedName("image")
        @Expose
        private String image;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getAction()
        {
            return action;
        }

        public void setAction(String action)
        {
            this.action = action;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }

        public String getStory()
        {
            return story;
        }

        public void setStory(String story)
        {
            this.story = story;
        }

        public String getImage()
        {
            return image;
        }

        public void setImage(String image)
        {
            this.image = image;
        }

    }

}



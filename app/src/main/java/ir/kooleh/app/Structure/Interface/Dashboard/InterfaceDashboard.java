package ir.kooleh.app.Structure.Interface.Dashboard;

import java.util.List;

import ir.kooleh.app.Structure.Model.Ads.ModelAd;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Dashboard.ModelDashboard;
import ir.kooleh.app.Structure.Model.Dashboard.ModelMyAd;
import ir.kooleh.app.Structure.Model.Dashboard.ModelOffersMyAd;

public class InterfaceDashboard
{

    public static class Apis
    {
        public interface IntGetProfile
        {
            void onResponseGetProfile(boolean response, ModelDashboard modelDashboard, String message);
        }

        public interface IntGetMyAds
        {
            void onResponseGetMyAds(boolean response, List<ModelMyAd> myAdList, ModelPaginate modelPaginate, String message);
        }

        public interface IntGetMyAdAuctions
        {
            void onResponseGetMyAdAuctions(boolean response, List<ModelMyAd> myAdList, String message);
        }

        public interface IntGetOffersMyAd
        {
            void onResponseGetOffersMyAd(boolean response, ModelOffersMyAd modelOffersMyAd, String message);
        }

        public interface IntChooseWinner
        {
            void onResponseChooseWinner(boolean response, String message);
        }

        public interface IntGetMyOffers
        {
            void onResponseGetMyOffers(boolean response, List<ModelMyAd> myAdList, ModelPaginate modelPaginate, String message);
        }

        public interface IntUpdateStepAd
        {
            void onResponseUpdateStepAd(boolean response, String message);
        }

        public interface IntSetActionAd
        {
            void onResponseSetActionAd(boolean response, String message);
        }

        public interface IntShowInfoAuctionOwner
        {
            void onResponseShowInfoAuctionOwnerPayment(boolean response, String extraMessage, String price, String message);
            void onResponseShowInfoAuctionOwnerShow(boolean response, ModelAd modelAd, String message);
        }
    }


    public static class InterClassData
    {
        public interface IntSendWinnerDataToActivity
        {
            void onResponseSendWinnerData(String userID, String auctionID, String username, String price);
        }

        public interface IntChangeTabPositionInMyAds
        {
            void onResponseIntChangeTabPositionInMyAds(int tabPosition);
        }
    }

}

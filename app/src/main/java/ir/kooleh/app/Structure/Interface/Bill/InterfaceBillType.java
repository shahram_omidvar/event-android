package ir.kooleh.app.Structure.Interface.Bill;

import java.util.List;

import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;

public class InterfaceBillType
{
    public static class BillType
    {
        public interface IntGetBillTypes
        {
            void onResponseGetBillTypes(boolean response, String message, List<ModelBillCategories> billTypeList, String backgroundImage);
        }
    }

}

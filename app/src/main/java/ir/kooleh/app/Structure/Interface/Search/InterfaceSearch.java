package ir.kooleh.app.Structure.Interface.Search;

import java.util.List;

import ir.kooleh.app.Structure.Model.Search.ModelSearchItem;

public class InterfaceSearch
{
    public interface SelectItem
    {
        // دریافت ریسپان سرور برای سرچ
        void onResponseSearchModelItem(boolean response, List<ModelSearchItem.Result> listSearch, String message);

        // کلیک روی آیتم ای سرچ
        void onClickListSearchSelectItem(ModelSearchItem.Result searchItem);
    }
}

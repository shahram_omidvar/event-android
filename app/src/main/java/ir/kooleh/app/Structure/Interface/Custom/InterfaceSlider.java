package ir.kooleh.app.Structure.Interface.Custom;

public interface InterfaceSlider
{
    void onClickedSlider(int currentItemPos);

    void onClosedSlider();
}

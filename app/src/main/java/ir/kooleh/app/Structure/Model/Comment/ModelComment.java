
package ir.kooleh.app.Structure.Model.Comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelComment
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image_profile")
    @Expose
    private String imageProfile;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("point")
    @Expose
    private Float point;
    @SerializedName("agree_count")
    @Expose
    private String agreeCount;
    @SerializedName("against_count")
    @Expose
    private String againstCount;
    @SerializedName("date")
    @Expose
    private String date;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getImageProfile()
    {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile)
    {
        this.imageProfile = imageProfile;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Float getPoint()
    {
        return point;
    }

    public void setPoint(Float point)
    {
        this.point = point;
    }

    public String getAgreeCount()
    {
        return agreeCount;
    }

    public void setAgreeCount(String agreeCount)
    {
        this.agreeCount = agreeCount;
    }

    public String getAgainstCount()
    {
        return againstCount;
    }

    public void setAgainstCount(String againstCount)
    {
        this.againstCount = againstCount;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

}
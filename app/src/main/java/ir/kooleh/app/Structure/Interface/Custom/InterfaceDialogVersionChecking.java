package ir.kooleh.app.Structure.Interface.Custom;

import ir.kooleh.app.Structure.Enum.EnCustomMessage;

public interface InterfaceDialogVersionChecking
{
    void onClickDialogVersionChecking(String message, EnCustomMessage enCustomMessage, int requestCode);
}

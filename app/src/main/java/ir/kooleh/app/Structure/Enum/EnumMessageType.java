package ir.kooleh.app.Structure.Enum;

public enum EnumMessageType
{
    NO_NETWORK,
    NOTFOUND,
    EMPTY,
    SERVER,
    PROFILE_INCOMPLETE,
    ACCOUNT_LOCKED
}

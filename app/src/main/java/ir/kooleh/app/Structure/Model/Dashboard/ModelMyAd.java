package ir.kooleh.app.Structure.Model.Dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelMyAd {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("line_one")
    @Expose
    private String lineOne;
    @SerializedName("line_two")
    @Expose
    private String lineTwo;
    @SerializedName("line_there")
    @Expose
    private String lineThere;
    @SerializedName("type_ad_id")
    @Expose
    private String typeAdID;
    @SerializedName("step_ad_id")
    @Expose
    private String stepAdID;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("message_image")
    @Expose
    private String messageImage;
    @SerializedName("tag_icon")
    @Expose
    private String tagIcon;
    @SerializedName("tag_message_center")
    @Expose
    private String tagMessageCenter;
    @SerializedName("tag_message_end")
    @Expose
    private String tagMessageEnd;
    @SerializedName("tag_color")
    @Expose
    private String tagColor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLineOne() {
        return lineOne;
    }

    public void setLineOne(String lineOne) {
        this.lineOne = lineOne;
    }

    public String getLineTwo() {
        return lineTwo;
    }

    public void setLineTwo(String lineTwo) {
        this.lineTwo = lineTwo;
    }

    public String getLineThere() {
        return lineThere;
    }

    public void setLineThere(String lineThere) {
        this.lineThere = lineThere;
    }

    public String getTypeAdID() {
        return typeAdID;
    }

    public void setTypeAdID(String typeAdID) {
        this.typeAdID = typeAdID;
    }

    public String getStepAdID() {
        return stepAdID;
    }

    public void setStepAdID(String stepAdID) {
        this.stepAdID = stepAdID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessageImage() {
        return messageImage;
    }

    public void setMessageImage(String messageImage) {
        this.messageImage = messageImage;
    }

    public String getTagIcon() {
        return tagIcon;
    }

    public void setTagIcon(String tagIcon) {
        this.tagIcon = tagIcon;
    }

    public String getTagMessageCenter() {
        return tagMessageCenter;
    }

    public void setTagMessageCenter(String tagMessageCenter) {
        this.tagMessageCenter = tagMessageCenter;
    }

    public String getTagMessageEnd() {
        return tagMessageEnd;
    }

    public void setTagMessageEnd(String tagMessageEnd) {
        this.tagMessageEnd = tagMessageEnd;
    }

    public String getTagColor() {
        return tagColor;
    }

    public void setTagColor(String tagColor) {
        this.tagColor = tagColor;
    }
}

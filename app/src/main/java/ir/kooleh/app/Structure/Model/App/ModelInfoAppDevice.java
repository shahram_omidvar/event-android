package ir.kooleh.app.Structure.Model.App;

public class ModelInfoAppDevice
{
    public final int interfaceApp = 1;
    private String version;
    private int versionCode;

    private String manufacturer;
    private String marketName;
    private String model;
    private String codename;


    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public int getVersionCode()
    {
        return versionCode;
    }

    public void setVersionCode(int versionCode)
    {
        this.versionCode = versionCode;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }


    public String getMarketName()
    {
        return marketName;
    }

    public void setMarketName(String marketName)
    {
        this.marketName = marketName;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getCodename()
    {
        return codename;
    }

    public void setCodename(String codename)
    {
        this.codename = codename;
    }
}

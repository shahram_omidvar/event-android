
package ir.kooleh.app.Structure.Model.Base;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelResponse
{

    @SerializedName("ok")
    @Expose
    private Boolean ok;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;

    public Boolean getOk()
    {
        return ok;
    }

    public void setOk(Boolean ok)
    {
        this.ok = ok;
    }

    public Integer getStatus()
    {
        return status;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Result getResult()
    {
        return result;
    }

    public void setResult(Result result)
    {
        this.result = result;
    }

    public class Result
    {

        @SerializedName("commend")
        @Expose
        private String commend;
        @SerializedName("length_code")
        @Expose
        private Integer lengthCode;

        public String getCommend()
        {
            return commend;
        }

        public void setCommend(String commend)
        {
            this.commend = commend;
        }

        public Integer getLengthCode()
        {
            return lengthCode;
        }

        public void setLengthCode(Integer lengthCode)
        {
            this.lengthCode = lengthCode;
        }

    }
}



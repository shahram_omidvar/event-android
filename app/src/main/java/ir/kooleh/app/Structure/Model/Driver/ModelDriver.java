package ir.kooleh.app.Structure.Model.Driver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelDriver
{
    @SerializedName("father_name")
    @Expose
    private String fatherName;
    @SerializedName("birth_date")
    @Expose
    private Object birthDate;
    @SerializedName("fleet_id")
    @Expose
    private String fleetId;
    @SerializedName("fleet_name")
    @Expose
    private String fleetName;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("smart_code")
    @Expose
    private String smartCode;
    @SerializedName("status")
    @Expose
    private String status;

    public String getFatherName()
    {
        return fatherName;
    }

    public void setFatherName(String fatherName)
    {
        this.fatherName = fatherName;
    }

    public Object getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(Object birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getFleetId()
    {
        return fleetId;
    }

    public void setFleetId(String fleetId)
    {
        this.fleetId = fleetId;
    }

    public String getFleetName()
    {
        return fleetName;
    }

    public void setFleetName(String fleetName)
    {
        this.fleetName = fleetName;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public String getCityName()
    {
        return cityName;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public String getSmartCode()
    {
        return smartCode;
    }

    public void setSmartCode(String smartCode)
    {
        this.smartCode = smartCode;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
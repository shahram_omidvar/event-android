package ir.kooleh.app.Structure.Interface.Ads;

import java.util.List;

import ir.kooleh.app.Structure.Model.Ads.ModelAd;
import ir.kooleh.app.Structure.Model.Ads.ModelAdsKey;
import ir.kooleh.app.Structure.Model.Ads.ModelAuctionOffer;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Image.ModelListImage;
import ir.kooleh.app.Structure.Model.Search.ModelSearchAds;

public class InterfaceAds
{


    public static class Fragment
    {

        public static class AdsKey
        {
            public interface ServerAdsKey
            {
                void onResponseAdsKey(boolean response, ModelAdsKey modelAdsKey, ModelPaginate modelPaginate, String message);
            }

            public interface AdapterAdsKey
            {
                void onClickItemAdKey(ModelAdsKey.ListAd modelAdsKey);
            }
        }

    }

    public static class AuctionAd
    {
        public interface intApiGetOfferList
        {
            void onResponseGetOfferList(boolean response, List<ModelAuctionOffer> auctionOfferList, String message);
        }

        public interface intApiCreateOffer
        {
            void onResponseCreateOffer(boolean response, String message);
        }
    }

    public interface Server
    {
        void onResponseCreateAd(boolean response, String message);
    }

    public interface Search
    {
        void onResponseSearchAds(boolean response, ModelSearchAds searchAds, String message);
    }

    public interface IAdapterCreateAds
    {
        void onClickAdapterListImage(ModelListImage modelListImage);
    }

    public interface IAdapterCategorySearchAds
    {
        void onClickCategorySearchAds(ModelSearchAds.Category category);
    }

    public interface IAdapterListSearchAds
    {
        void onClickItemSearchAds(ModelSearchAds.Ads ads);

        void onClickSaveCommentSearchAds(ModelSearchAds.Ads ads);
    }

    public interface IInfo
    {
        void onResponseGetAds(boolean response, ModelAd adDetails, String message);
    }




}

package ir.kooleh.app.Structure.Interface.Load;

import java.util.List;

import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Load.ModelListLoad;
import ir.kooleh.app.Structure.Model.Load.ModelLoad;

public class InterfaceLoad
{

    public interface Fragment
    {
        void onResponseListLoadFragment(boolean response, List<ModelListLoad> listLoad, ModelPaginate modelPaginate, String message);

        void onOnclickItemLoadFragment(ModelListLoad load);
    }


    public interface Activity
    {
        void onResponseLoadActivity(boolean response, ModelLoad loader, String message);

    }

}

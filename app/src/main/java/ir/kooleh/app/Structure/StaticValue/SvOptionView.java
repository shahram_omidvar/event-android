package ir.kooleh.app.Structure.StaticValue;

public class SvOptionView
{
    public static String MAX_LENGTH = "maxـlength";
    public static String MIN_LENGTH = "minـlength";
    public static String HELPER_TEXT = "helper_text";
    public static String HINT = "hint";
    public static String MARGIN = "margin";
    public static String PADDING = "padding";
    public static String PADDING_VERTICAL = "padding_vertical";
    public static String PADDING_HORIZONTAL = "padding_horizontal";

}

package ir.kooleh.app.Structure.Model.Search;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelSearchItem
{

    @SerializedName("ok")
    @Expose
    private Boolean ok;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("status")
    @Expose
    private String status;

    public Boolean getOk()
    {
        return ok;
    }

    public void setOk(Boolean ok)
    {
        this.ok = ok;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public List<Result> getResult()
    {
        return result;
    }

    public void setResult(List<Result> result)
    {
        this.result = result;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public class Result
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("key")
        @Expose
        private String key;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private Object image;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getModel()
        {
            return model;
        }

        public void setModel(String model)
        {
            this.model = model;
        }

        public String getTitle()
        {
            return title;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }

        public Object getImage()
        {
            return image;
        }

        public void setImage(Object image)
        {
            this.image = image;
        }

        public String getKey()
        {
            return key;
        }

        public void setKey(String key)
        {
            this.key = key;
        }
    }
}





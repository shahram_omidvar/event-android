
package ir.kooleh.app.Structure.Model.User.Profile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ModelTargetProfile
{

        @SerializedName("info")
        @Expose
        private Info info;
        @SerializedName("ads")
        @Expose
        private List<Object> ads = null;

        public Info getInfo() {
            return info;
        }

        public void setInfo(Info info) {
            this.info = info;
        }

        public List<Object> getAds() {
            return ads;
        }

        public void setAds(List<Object> ads) {
            this.ads = ads;
        }

    public class Info {

        @SerializedName("action_btn")
        @Expose
        private String actionBtn;
        @SerializedName("info_connection")
        @Expose
        private InfoConnection infoConnection;
        @SerializedName("info_profile")
        @Expose
        private InfoProfile infoProfile;

        public String getActionBtn() {
            return actionBtn;
        }

        public void setActionBtn(String actionBtn) {
            this.actionBtn = actionBtn;
        }

        public InfoConnection getInfoConnection() {
            return infoConnection;
        }

        public void setInfoConnection(InfoConnection infoConnection) {
            this.infoConnection = infoConnection;
        }

        public InfoProfile getInfoProfile() {
            return infoProfile;
        }

        public void setInfoProfile(InfoProfile infoProfile) {
            this.infoProfile = infoProfile;
        }

    }

    public class InfoConnection {

        @SerializedName("count_follower")
        @Expose
        private Integer countFollower;
        @SerializedName("count_following")
        @Expose
        private Integer countFollowing;
        @SerializedName("count_travel")
        @Expose
        private Integer countTravel;
        @SerializedName("count_tour")
        @Expose
        private Integer countTour;

        public Integer getCountFollower() {
            return countFollower;
        }

        public void setCountFollower(Integer countFollower) {
            this.countFollower = countFollower;
        }

        public Integer getCountFollowing() {
            return countFollowing;
        }

        public void setCountFollowing(Integer countFollowing) {
            this.countFollowing = countFollowing;
        }

        public Integer getCountTravel() {
            return countTravel;
        }

        public void setCountTravel(Integer countTravel) {
            this.countTravel = countTravel;
        }

        public Integer getCountTour() {
            return countTour;
        }

        public void setCountTour(Integer countTour) {
            this.countTour = countTour;
        }

    }


    public class InfoProfile {

        @SerializedName("full_name")
        @Expose
        private String fullName;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("bio")
        @Expose
        private Object bio;

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getBio() {
            return bio;
        }

        public void setBio(Object bio) {
            this.bio = bio;
        }

    }

}




package ir.kooleh.app.Structure.Model.Bills;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelBillCategories
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fa_name")
    @Expose
    private String faName;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("text_color")
    @Expose
    private String textColor;
    @SerializedName("back_color")
    @Expose
    private String backColor;
    @SerializedName("children")
    @Expose
    private List<Child> children = null;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getFaName() { return faName; }

    public void setFaName(String faName) { this.faName = faName; }

    public Object getImage() { return image; }

    public void setImage(Object image) { this.image = image; }

    public String getTextColor() { return textColor; }

    public void setTextColor(String textColor) { this.textColor = textColor; }

    public String getBackColor() { return backColor; }

    public void setBackColor(String backColor) { this.backColor = backColor; }

    public List<Child> getChildren() { return children; }

    public void setChildren(List<Child> children) { this.children = children; }





    public class Child {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("f_name")
        @Expose
        private String fName;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("text_color")
        @Expose
        private String textColor;
        @SerializedName("back_color")
        @Expose
        private String backColor;
        @SerializedName("type_request")
        @Expose
        private String typeRequest;
        @SerializedName("count_bill_id")
        @Expose
        private String countBillID;
        @SerializedName("children")
        @Expose
        private Object children;

        public String getId() { return id; }

        public void setId(String id) { this.id = id; }

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public String getfName() { return fName; }

        public void setfName(String fName) { this.fName = fName; }

        public String getImage() { return image; }

        public void setImage(String image) { this.image = image; }

        public String getTextColor() { return textColor; }

        public void setTextColor(String textColor) { this.textColor = textColor; }

        public String getBackColor() { return backColor; }

        public void setBackColor(String backColor) { this.backColor = backColor; }

        public String getTypeRequest() { return typeRequest; }

        public void setTypeRequest(String typeRequest) { this.typeRequest = typeRequest; }

        public String getCountBillID() { return countBillID; }

        public void setCountBillID(String countBillID) { this.countBillID = countBillID; }

        public Object getChildren() { return children; }

        public void setChildren(Object children) { this.children = children; }
    }

}

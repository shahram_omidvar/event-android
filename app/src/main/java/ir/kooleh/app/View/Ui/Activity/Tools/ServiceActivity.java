package ir.kooleh.app.View.Ui.Activity.Tools;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;


import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.FragmentCommunicator;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceSlider;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterSliderAdDetails;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterSliderServiceActivity;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Volley.Controller;
import ir.kooleh.app.View.Ui.Fragment.AboutServiceFragment;
import ir.kooleh.app.View.Ui.Fragment.CommentServiceFragment;
import ir.kooleh.app.View.Ui.Fragment.FragmentSlider;
import ir.kooleh.app.View.Ui.Fragment.FragmentSliderService;
import ir.kooleh.app.View.Ui.Fragment.InformationServiceFragment;

public class ServiceActivity extends AppCompatActivity implements View.OnClickListener,
        AppBarLayout.OnOffsetChangedListener,
        InterfaceSlider,
        FragmentCommunicator
{

    public static final String SERVICE_ID = "serviceId";
    public static final String JSON_SERVICE = "JSON_SERVICE";
    public static String Title = "";
    public static final String TAG = "ServiceActivity";

    private List<Fragment> serviceFragments;
    private List<String> titleFragments;
    private AboutServiceFragment aboutServiceFragment;
    private InformationServiceFragment informationServiceFragment;
    private CommentServiceFragment commentServiceFragment;
    private ServiceObject serviceObject;
    private ServicePagerAdapter servicePagerAdapter;
    private ImageServicePagerAdapter imageServicePagerAdapter;
    private FrameLayout fragmentContainer;

    private AdapterSliderServiceActivity adapterSliderServices;
    private ViewPager2 viewPager2Slider;
    private DotsIndicator dotsIndicator;

    private ServiceApi serviceApi;
    private String serviceId;
    private String userId;
    private BasicService basicService;

    private Handler handler;
    private Runnable runnable;

    private CoordinatorLayout coordinatorLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private AppBarLayout appBarLayout;
    private ProgressBar pbLoadingService;
    private LinearLayout llTitleLocationService;
    private LinearLayout llBuyService;
    private TextView tvTopTitleService;
    private TextView tvTitleService;
    private TextView tvLocationService;

    private TextView tvOffPercentService;
    private TextView tvOriginalPriceService;
    private TextView tvFinalOffService;

    private Button btnBuyService;
    private Toolbar toolbar;
    private ImageButton ivBackService;
    private ImageButton ivShareService;
    private ImageButton ivBookmarkService;
    private TabLayout tabService;
    private UserDefault userDefault;
    private ViewPager vpTabPagesService;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        // ChengeColorStatusBar.chengeColorStatusBar(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        init();
        findViews();
        getDataFromIntent();
        getService();
//
//        Intent intent = getIntent();
//        String action = intent.getAction();
//        Uri data = intent.getData();
//        Log.d(TAG, "onCreate: action "+ action);
//        Log.d(TAG, "onCreate: service"+ data);

    }

    private void getDataFromIntent()
    {
        Bundle bundle = getIntent().getExtras();
        String sj = "";
        if (bundle != null)
        {
            sj = bundle.getString(JSON_SERVICE, "");
        }
        if (!sj.equals(""))
        {
            serviceObject = new Gson().fromJson(sj, ServiceObject.class);
            tvTitleService.setText(serviceObject.getTitle());
            tvLocationService.setText(serviceObject.getLocationName());
            imageServicePagerAdapter = new ImageServicePagerAdapter(this, serviceObject.getImage());
        }
        if (userDefault.getUser() != null && userDefault.getUser().getId() != null)
        {
            userId = "" + userDefault.getUser().getId();
        }


    }

    public void init()
    {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            serviceId = bundle.getString(SERVICE_ID, "98");
        }
        else
        {
            serviceId = "98";
        }

        handler = new Handler();

        userDefault = UserDefault.getInstance();
        serviceApi = ServiceApi.getInstance(this);
        aboutServiceFragment = AboutServiceFragment.newInstance(serviceId);
        informationServiceFragment = InformationServiceFragment.newInstance(serviceId);
        commentServiceFragment = CommentServiceFragment.newInstance(serviceId);
        serviceFragments = new ArrayList<>();
        titleFragments = new ArrayList<>();

//        serviceFragments.add(aboutServiceFragment);
//        serviceFragments.add(informationServiceFragment);
//        serviceFragments.add(commentServiceFragment);
//        servicePagerAdapter = new ServicePagerAdapter(getSupportFragmentManager(), serviceFragments);

    }


    private void findViews()
    {
        fragmentContainer = findViewById(R.id.contaienr_fragment);
        coordinatorLayout = findViewById(R.id.coordinator_service);
        collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        appBarLayout = findViewById(R.id.app_bar_service);
        pbLoadingService = findViewById(R.id.pb_loading_service);
        llTitleLocationService = findViewById(R.id.ll_title_location_service);
        tvTopTitleService = findViewById(R.id.tv_top_title_service);
        tvTitleService = findViewById(R.id.tv_title_service);
        tvOffPercentService = findViewById(R.id.tv_off_percent_service);
        tvOriginalPriceService = findViewById(R.id.tv_original_price_service);
        tvFinalOffService = findViewById(R.id.tv_final_price_service);
        tvLocationService = findViewById(R.id.tv_location_service);
        btnBuyService = findViewById(R.id.btn_buy_service);
        toolbar = findViewById(R.id.toolbar);
        ivBackService = findViewById(R.id.iv_back_service);
        ivShareService = findViewById(R.id.iv_share_service);
        ivBookmarkService = findViewById(R.id.iv_bookmark_service);
        tabService = findViewById(R.id.tb_service);
        vpTabPagesService = findViewById(R.id.nsv_new_event);
        llBuyService = findViewById(R.id.ll_buy_service);

        viewPager2Slider = findViewById(R.id.viewpater2_slider_services);
        dotsIndicator = findViewById(R.id.dots_indicator_slider_services);

        appBarLayout.addOnOffsetChangedListener(this);
        btnBuyService.setOnClickListener(this);
        ivBackService.setOnClickListener(this);
        ivShareService.setOnClickListener(this);
        ivBookmarkService.setOnClickListener(this);

        viewPager2Slider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback()
        {
            @Override
            public void onPageSelected(int position)
            {
                super.onPageSelected(position);
                startSliderCycle(position);
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
                super.onPageScrollStateChanged(state);
                if (state == ViewPager2.SCROLL_STATE_DRAGGING)
                {
                    startSliderCycle(viewPager2Slider.getCurrentItem());
                }
            }
        });

    }

    private void startSliderCycle(int position)
    {
        if (runnable != null)
        {
            handler.removeCallbacks(runnable);
        }
        runnable = () ->
        {
            int numPages = viewPager2Slider.getAdapter().getItemCount();
            viewPager2Slider.setCurrentItem((position + 1) % numPages);
            handler.postDelayed(runnable, 5000);
        };
        handler.postDelayed(runnable, 5000);
    }


    @Override
    public void onClick(View v)
    {


        if (v == ivBackService)
        {
            onBackPressed();
        }
        else if (v == ivShareService)
        {
            if (basicService == null || basicService.getShareLink() == null)
            {
                return;
            }
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, basicService.getTitle() + "\n" + basicService.getShareLink());
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        }
//        else if (v == ivBookmarkService)
//        {
//            // Handle clicks for ivBookmarkService
//            if (basicService == null)
//            {
//                return;
//            }
//
//            if (userDefault.getUser() == null || userDefault.getUser().getApiToken() == null)
//            {
//                //todo use other pages
//                CustomSnackbar.makeAction(Controller.context, coordinatorLayout, "لطفا ابتدا ثبت نام کنید", "ورود", Snackbar.LENGTH_INDEFINITE, new View.OnClickListener()
//                {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        startActivity(new Intent(ServiceActivity.this, LoginRegisterActivity.class));
//                        finish();
//                    }
//                }).show();
//                return;
//            }
        pbLoadingService.setVisibility(View.VISIBLE);
        int isSet = basicService.getIsBookmark() == 1 ? 0 : 1;

        serviceApi.doBookmark(userDefault.getUser().getApiToken(), serviceId, isSet, new ServiceApi.CompletionDoBookmark()
        {
            @Override
            public void response(boolean isOk, int isBookmark, String message)
            {
                pbLoadingService.setVisibility(View.GONE);
                CustomToast customToast = new CustomToast(ServiceActivity.this);
                customToast.showError(message);
                if (isOk)
                {
                    basicService.setIsBookmark(isBookmark);
                    if (isBookmark == 1)
                    {
                        ivBookmarkService.setImageDrawable(getResources().getDrawable(R.drawable.ic_share));
                    }
                    else
                    {
                        ivBookmarkService.setImageDrawable(getResources().getDrawable(R.drawable.ic_bookmark));
                    }
                }
            }
        });
    }


    private void getService()
    {
        pbLoadingService.setVisibility(View.VISIBLE);

        serviceApi.getService(serviceId, userId, new ServiceApi.CompletionGetService()
        {
            @Override
            public void onGetServiceResponse(boolean isOk, final BasicService basicService, final TabAboutService aboutService, final TabInformationService informationService, final String message)
            {
                pbLoadingService.setVisibility(View.VISIBLE);
                if (isOk)
                {

                    setupBasicDate(basicService);
                    setupViewPager(aboutService, informationService);


                }
                else
                {
                    CustomSnackbar.makeAction(Controller.context, coordinatorLayout, message, "تلاش مجدد", Snackbar.LENGTH_INDEFINITE, new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            getService();
                        }
                    }).show();
                }

            }
        });
    }

    private void setupViewPager(TabAboutService aboutService, final TabInformationService informationService)
    {

        serviceFragments.clear();
        serviceFragments.add(aboutServiceFragment);
        serviceFragments.add(informationServiceFragment);
        serviceFragments.add(commentServiceFragment);

        titleFragments.clear();
        titleFragments.add("نظرات");
        titleFragments.add(informationService.getTabName());
        titleFragments.add(aboutService.getTabName());
        servicePagerAdapter = new ServicePagerAdapter(getSupportFragmentManager(), serviceFragments, titleFragments);

        aboutServiceFragment.setTabAboutService(aboutService);

        // informationServiceFragment.setTabInformationService(informationService);
        tabService.setupWithViewPager(vpTabPagesService);
        vpTabPagesService.setOffscreenPageLimit(3);
        vpTabPagesService.setAdapter(servicePagerAdapter);
        vpTabPagesService.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {


            }

            @Override
            public void onPageSelected(int position)
            {

                if (position == 0)
                {

                }
                else if (position == 1 && informationServiceFragment.tabInformationService == null)
                {
                    informationServiceFragment.tabInformationService = informationService;
                    informationServiceFragment.setUpData();

                }
                else if (position == 2 && (commentServiceFragment.comments == null || commentServiceFragment.percentagePoints == null))
                {

                    commentServiceFragment.getPercentagePoints();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });
        setFontOnViewPager();
    }


    private void setupBasicDate(BasicService basicService)
    {
        this.basicService = basicService;

        // pbLoadingService.setVisibility(View.INVISIBLE);
        tvTopTitleService.setText(basicService.getTitle());
        tvTitleService.setText(basicService.getTitle());
        tvLocationService.setText(basicService.getLocationName());
        if (basicService.getIsBookmark() == 1)
        {
            ivBookmarkService.setImageDrawable(getResources().getDrawable(R.drawable.ic_share));
        }


        if (basicService.getNextLink() != null)
        {
            llBuyService.setVisibility(View.VISIBLE);
            btnBuyService.setText(basicService.getTitleLink());

            if (basicService.getOff() != null && Double.valueOf(basicService.getOff().toString()) != 0 && Double.valueOf(basicService.getPrice()) != -1)
            {
                double off = Double.valueOf(basicService.getOff().toString());
                tvOffPercentService.setVisibility(View.VISIBLE);
                tvOffPercentService.setText(off + "٪ ");
                tvOriginalPriceService.setText(basicService.getPrice() + "");
                tvOriginalPriceService.setTextColor(getResources().getColor(R.color.red_100));
                tvOriginalPriceService.setPaintFlags(tvOriginalPriceService.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                double offerPrice = (int) (Double.valueOf(basicService.getPrice()) - (Double.valueOf(off) * Double.valueOf(basicService.getPrice()) / 100));
                tvFinalOffService.setText(offerPrice + " تومان");
                tvFinalOffService.setTextColor(getResources().getColor(R.color.teal_400));
            }
            else if (Integer.valueOf(basicService.getPrice()) != -1)
            {

                tvOffPercentService.setVisibility(View.GONE);
                tvOriginalPriceService.setText(basicService.getPrice() + " تومان");
                tvOriginalPriceService.setTextColor(getResources().getColor(R.color.teal_400));
                tvOriginalPriceService.setPaintFlags(tvOriginalPriceService.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }


        }
        else
        {
            llBuyService.setVisibility(View.GONE);
        }

        adapterSliderServices = new AdapterSliderServiceActivity(this, basicService.getSlider());
        viewPager2Slider.setAdapter(adapterSliderServices);
        dotsIndicator.setViewPager2(viewPager2Slider);
        imageServicePagerAdapter = new ImageServicePagerAdapter(this, basicService.getSlider());
        imageServicePagerAdapter.setOnclick(new ImageServicePagerAdapter.OnClickPictures()
        {
            @Override
            public void OnClickPitures(View view, int position)
            {
                showPicturesDialog();
            }
        });
        pbLoadingService.setVisibility(View.INVISIBLE);
        createOptionMenu();
    }

    private void createOptionMenu()
    {



    }



    private void setFontOnViewPager()
    {
        ViewGroup vg = (ViewGroup) tabService.getChildAt(0);
        int tabsCount = vg.getChildCount();
//        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/iran_sans.ttf");
        for (int j = 0; j < tabsCount; j++)
        {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++)
            {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView)
                {
                    ((TextView) tabViewChild).setTypeface(Controller.MyTypeface, Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                }
            }
        }
    }

    private void showDots(int pageNumber, LinearLayout linearLayout, ViewPager viewPager)
    {
        TextView[] textViewDot = new TextView[viewPager.getAdapter().getCount()];
        linearLayout.removeAllViews();

        for (int i = 0; i < textViewDot.length; i++)
        {
            textViewDot[i] = new TextView(this);
            textViewDot[i].setText(Html.fromHtml("&#8226;"));
            textViewDot[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, 35);
            textViewDot[i].setTextColor(ContextCompat.getColor(this,
                    (i == pageNumber ? R.color.green_100 : R.color.red_100)
            ));
            linearLayout.addView(textViewDot[i]);
        }
    }


    private void showPicturesDialog()
    {
        final Dialog dialog_pictures = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog_pictures.setContentView(R.layout.dialog_image_service);
        ImageView imgClose = dialog_pictures.findViewById(R.id.iv_close_image_dialog);
        imgClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog_pictures.dismiss();
            }
        });

        final ViewPager viewPagerFullScreen = (ViewPager) dialog_pictures.findViewById(R.id.vp_image_dialog);
        final LinearLayout linearLayoutFullScreen = (LinearLayout) dialog_pictures.findViewById(R.id.ll_dots_image_dialog);
        viewPagerFullScreen.setAdapter(imageServicePagerAdapter);
        showDots(viewPagerFullScreen.getCurrentItem(), linearLayoutFullScreen, viewPagerFullScreen);
        viewPagerFullScreen.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                showDots(position, linearLayoutFullScreen, viewPagerFullScreen);
                if (position == viewPagerFullScreen.getAdapter().getCount() - 1)
                {
                }
                else
                {
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });
        dialog_pictures.show();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i)
    {


        if (i < -440)
        {
            tvTopTitleService.setVisibility(View.VISIBLE);
        }
        else
            tvTopTitleService.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onClickedSlider(int currentItemPos)
    {
        if (basicService != null && basicService.getSlider() != null)
        {
            fragmentContainer.setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().replace(R.id.contaienr_fragment,
                    new FragmentSliderService(this, basicService.getSlider(), currentItemPos)).addToBackStack(null).commit();
        }
    }

    @Override
    public void onClosedSlider()
    {
        fragmentContainer.setVisibility(View.GONE);
    }

    public void fragmentDetached()
    {
        fragmentContainer.setVisibility(View.GONE);
    }

}

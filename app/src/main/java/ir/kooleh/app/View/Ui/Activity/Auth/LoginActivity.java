package ir.kooleh.app.View.Ui.Activity.Auth;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.messaging.FirebaseMessaging;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.auth.ApiAuth;
import ir.kooleh.app.Server.Api.auth.ApiCode;
import ir.kooleh.app.Structure.Enum.EnumListCommendApi;
import ir.kooleh.app.Structure.Enum.EnumVerifyCode;
import ir.kooleh.app.Structure.Interface.Auth.InterfaceAuth;
import ir.kooleh.app.Structure.Interface.Auth.InterfaceCode;
import ir.kooleh.app.Structure.Model.User.ModelUser;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.SettingKeyboard;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.ViewLoading;


public class LoginActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfaceAuth.Login,
        InterfaceCode.SendCode
{
    private ImageView ivBack;
    private CEditText etPasswordLogin;
    private TextInputLayout txtInputPasswordLogin;
    private Button btnRestPass, btnLoginToApp, btnLoginByCode;

    private CustomToast customToast;
    private Intent intent;

    private ViewLoading viewLoading;
    private UserShared userShared;
    private ApiAuth apiAuth;
    private ApiCode apiCode;

    public static final String PHONE_NUMBER = "phone_number";
    private String phoneNumber = "-1";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findView();
        getIntentData();
        createVar();
    }


    @SuppressLint("ClickableViewAccessibility")
    private void findView()
    {
        ivBack = findViewById(R.id.iv_back_login);
        etPasswordLogin = findViewById(R.id.et_password_login);
        txtInputPasswordLogin = findViewById(R.id.txt_input_password_login);
        btnRestPass = findViewById(R.id.btn_rest_pass_login);
        btnLoginToApp = findViewById(R.id.btn_login_app);
        btnLoginByCode = findViewById(R.id.btn_login_by_code);

        ivBack.setOnClickListener(this);
        btnRestPass.setOnClickListener(this);
        btnLoginToApp.setOnClickListener(this);
        btnLoginByCode.setOnClickListener(this);

        etPasswordLogin.setOnTouchListener((v, event) ->
        {
            txtInputPasswordLogin.setErrorEnabled(false);
            return false;
        });

        etPasswordLogin.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.length() > 0)
                {
                    txtInputPasswordLogin.setErrorEnabled(false);
                }

            }
        });
    }

    private void createVar()
    {
        viewLoading = new ViewLoading(this);
        userShared = new UserShared(this);
        apiAuth = new ApiAuth(this);
        apiCode = new ApiCode(this);
    }

    private void getIntentData()
    {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        phoneNumber = bundle.getString(PHONE_NUMBER, "-1");

        if (phoneNumber.equals("-1"))
        {
            customToast = new CustomToast(this);
            customToast.showError(getString(R.string.error_send_data_intent));
            finish();
        }
    }


    @Override
    public void onClick(View view)
    {
        if (view == ivBack)
            finish();
        else if (view == btnLoginToApp)
            loginToApp();
        else if (view == btnLoginByCode)
        {
            viewLoading.show();
            apiCode.SendCode(this, phoneNumber, EnumVerifyCode.LoginByCode);
        }
        else if (view == btnRestPass)
        {
            viewLoading.show();
            apiCode.SendCode(this, phoneNumber, EnumVerifyCode.ResetPassword);
        }
    }


    private void loginToApp()
    {
        SettingKeyboard.hideKeyboard(this);
        if (etPasswordLogin.getText() != null && etPasswordLogin.getText().length() >= 6)
        {
            viewLoading.show();
            FirebaseMessaging.getInstance().getToken()
                    .addOnCompleteListener(task ->
                    {
                        if (!task.isSuccessful())
                        {
                            customToast = new CustomToast(this);
                            customToast.showWarnig("هشدار متاسفانه دستگاه شما یافت نشد");
                            apiAuth.Login(this,
                                    phoneNumber,
                                    etPasswordLogin.getText().toString(),
                                    "")
                            ;
                        }

                        apiAuth.Login(this,
                                phoneNumber,
                                etPasswordLogin.getText().toString(),
                                task.getResult());
                    });

        }
        else
        {
            txtInputPasswordLogin.setError("خطا گذرواژه حداقل شیش کارکتر می باشد");
        }
    }

    @Override
    public void onResponseLogin(boolean response, String message, ModelUser user)
    {
        viewLoading.close();
        if (response)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            try
            {
                userShared.setData(user);
                finish();
                Intent intent = new Intent(this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
            catch (Exception e)
            {
                customToast = new CustomToast(this);
                customToast.showError(e.getMessage());
            }
        }

        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onResponseSendCode(boolean response, String message, String commend, int lengthCode)
    {
        viewLoading.close();
        if (response)
        {
            if (commend.equals(EnumListCommendApi.VerifyResetPassword))
            {
                finish();
                intent = new Intent(LoginActivity.this, VerifyCodeActivity.class);
                intent.putExtra(VerifyCodeActivity.PHONE_NUMBER, phoneNumber);
                intent.putExtra(VerifyCodeActivity.TYPE, EnumVerifyCode.ResetPassword);
                intent.putExtra(VerifyCodeActivity.LENGTH_CODE, lengthCode);
                startActivity(intent);
            }

            else if (commend.equals(EnumListCommendApi.VerifyLoginByCode))
            {
                finish();
                intent = new Intent(LoginActivity.this, VerifyCodeActivity.class);
                intent.putExtra(VerifyCodeActivity.PHONE_NUMBER, phoneNumber);
                intent.putExtra(VerifyCodeActivity.TYPE, EnumVerifyCode.LoginByCode);
                intent.putExtra(VerifyCodeActivity.LENGTH_CODE, lengthCode);
                startActivity(intent);
            }

            else
            {
                customToast = new CustomToast(this);
                customToast.showError("خطا دستور پیدا نشد");
            }
        }

        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }
}
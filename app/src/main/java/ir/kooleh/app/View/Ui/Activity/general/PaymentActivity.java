package ir.kooleh.app.View.Ui.Activity.general;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Bill.ApiBills;
import ir.kooleh.app.Server.Api.Wallet.ApiBuys;
import ir.kooleh.app.Structure.Enum.EnumWalletModule;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceUserBill;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceRecyclerViewSelectGateway;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceBuys;
import ir.kooleh.app.Structure.Model.Wallet.ModelGateway;
import ir.kooleh.app.Utility.Adapter.Wallet.AdapterGateways;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.Utility.Class.MessageHandlerUtils;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class PaymentActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfaceRecyclerViewSelectGateway,
        InterfaceBuys.Buys.IntGetListGateWay,
        InterfaceBuys.Buys.IntBuyApp,
        InterfaceUserBill.PayBill.IntPayBill,
        InterfaceMessageHandler.FragmentResult
{
    private ImageView ivClose;
    private CTextView tvTitleToolbar;
    private RecyclerView rvGateways;
    private CardView btnPay;
    private CTextView tvBtnPay;
    private FrameLayout containerMessageHandler;
    private NestedScrollView nsvRoot;

    private FragmentMessageHandler fragmentMessageHandler;

    private AdapterGateways adapterGateways;

    private ApiBuys apiBuys;
    private ApiBills apiBills;
    private ViewLoading viewLoading;

    private String gatewayName;
    private String price;
    private String modelPayment;
    private String titleToolbar;

    private String billID;
    private String paymentID;

    public static final String KEY_PRICE_PAYMENT_ACTIVITY = "pricePaymentActivity";
    public static final String KEY_TITLE_TOOLBAR_PAYMENT_ACTIVITY = "titleToolbarPaymentActivity";
    public static final String KEY_MODEL_PAYMENT_ACTIVITY = "modelPaymentActivity";
    public static final String KEY_AD_OR_PACKAGE_ID_PAYMENT_ACTIVITY = "adOrPackageIDPaymentActivity";

    public static final String KEY_BILL_ID_PAYMENT_ACTIVITY = "billIDPaymentActivity";
    public static final String KEY_PAYMENT_ID_PAYMENT_ACTIVITY = "paymentIDPaymentActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        initViews();
        initVars();
    }

    private void initViews()
    {
        ivClose = findViewById(R.id.iv_back_toolbar);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        rvGateways = findViewById(R.id.rv_gateways_select_payment_activity);
        btnPay = findViewById(R.id.btn_pay_payment_activity);
        tvBtnPay = findViewById(R.id.tv_btn_pay_payment_activity);
        containerMessageHandler = findViewById(R.id.container_message_handler_fragment);
        nsvRoot = findViewById(R.id.nsv_payment_activity);

        price = getIntent().getExtras().getString(KEY_PRICE_PAYMENT_ACTIVITY, "0");
        modelPayment = getIntent().getExtras().getString(KEY_MODEL_PAYMENT_ACTIVITY);
        titleToolbar = getIntent().getExtras().getString(KEY_TITLE_TOOLBAR_PAYMENT_ACTIVITY);


        tvBtnPay.setText("پرداخت " + NumberUtils.digitDivider(price) + " ریال");
        tvTitleToolbar.setText(titleToolbar);


        ivClose.setOnClickListener(this);
        btnPay.setOnClickListener(this);

        btnPay.setClickable(false);
    }

    private void initVars()
    {
        apiBuys = new ApiBuys(this);
        apiBills = new ApiBills(this);
        viewLoading = new ViewLoading(this);

        adapterGateways = new AdapterGateways(this, this, null);

        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setJustifyContent(JustifyContent.CENTER);
        rvGateways.setLayoutManager(flexboxLayoutManager);
        rvGateways.setAdapter(adapterGateways);

        viewLoading.show();
        apiBuys.getListGateway(this);
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivClose)
        {
            finish();
        }
        else if (v == btnPay)
        {
            viewLoading.show();
            switch (modelPayment)
            {
                case EnumWalletModule.BuyModels.MODEL_WALLET:
                    apiBuys.BuyApp(this, gatewayName, EnumWalletModule.BuyModels.MODEL_WALLET,
                            price, "");
                    break;
                case EnumWalletModule.BuyModels.MODEL_PACKAGE:
                    apiBuys.BuyApp(this, gatewayName, EnumWalletModule.BuyModels.MODEL_PACKAGE,
                            price, getIntent().getExtras().getString(KEY_AD_OR_PACKAGE_ID_PAYMENT_ACTIVITY));
                    break;
                case EnumWalletModule.BuyModels.MODEL_AD:
                    apiBuys.BuyApp(this, gatewayName, EnumWalletModule.BuyModels.MODEL_AD,
                            price, getIntent().getExtras().getString(KEY_AD_OR_PACKAGE_ID_PAYMENT_ACTIVITY));
                    break;
                case EnumWalletModule.BuyModels.MODEL_BILL:
                    apiBills.payBill(this,
                            getIntent().getExtras().getString(KEY_BILL_ID_PAYMENT_ACTIVITY),
                            getIntent().getExtras().getString(KEY_PAYMENT_ID_PAYMENT_ACTIVITY),
                            gatewayName);
                    break;
            }
        }
    }

    @Override
    public void onResponseGetListGateWay(boolean response, String message, List<ModelGateway> gatewayList)
    {
        viewLoading.close();
        containerMessageHandler.setVisibility(View.GONE);
        nsvRoot.setVisibility(View.GONE);
        if (response)
        {
            nsvRoot.setVisibility(View.VISIBLE);
            adapterGateways.setList(gatewayList);
        }
        else
        {
            containerMessageHandler.setVisibility(View.VISIBLE);
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, MessageHandlerUtils.getMessageTypeByMessage(message));
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }

    @Override
    public void onResponseSelectGateway(String gatewayName)
    {
        this.gatewayName = gatewayName;
        if (!gatewayName.equals(""))
        {
            btnPay.setClickable(true);
            btnPay.setAlpha(1f);
        }
        else
        {
            btnPay.setClickable(false);
            btnPay.setAlpha(.5f);
        }
    }

    @Override
    public void onResponseBuyApp(boolean response, String tokenOne, String tokenTwo, String message)
    {
        viewLoading.close();
        if (response)
        {
            String uri = Uri.parse("https://application.kooleh.ir/payment/Gateway")
                    .buildUpon()
                    .appendQueryParameter("token_one", tokenOne)
                    .appendQueryParameter("token_two", tokenTwo)
                    .build().toString();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(browserIntent);
        }
        else
        {
            CustomToast customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        viewLoading.show();
        apiBuys.getListGateway(this);
    }

    @Override
    public void onResponsePayBill(boolean response, String tokenOne, String tokenTwo, String message)
    {
        viewLoading.close();
        if (response)
        {
            String uri = Uri.parse("https://application.kooleh.ir/payment/Gateway")
                    .buildUpon()
                    .appendQueryParameter("token_one", tokenOne)
                    .appendQueryParameter("token_two", tokenTwo)
                    .build().toString();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(browserIntent);
        }
        else
        {
            CustomToast customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }
}
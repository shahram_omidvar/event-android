package ir.kooleh.app.View.Ui.Dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.NumberPicker;

import androidx.cardview.widget.CardView;

import java.util.Locale;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.View.InterfacePickers;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class CustomDialogTimePicker implements View.OnClickListener
{
    private AlertDialog dialog;
    private CardView btnConfirm;

    private NumberPicker pickerHour, pickerMin;
    private InterfacePickers.IntTimePickerDialogResult intTimePickerDialogResult;

    private int hour, min;

    public void make(Context context, InterfacePickers.IntTimePickerDialogResult intTimePickerDialogResult, String strTimeAmount)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.dialog_time_picker, null);
        builder.setView(inflater);
        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initView(inflater);
        initVars();
        this.intTimePickerDialogResult =  intTimePickerDialogResult;
        parseTimeAmount(strTimeAmount);
    }


    private void initView(View view)
    {

        pickerHour = view.findViewById(R.id.time_picker_hour);
        pickerMin = view.findViewById(R.id.time_picker_min);
        btnConfirm = view.findViewById(R.id.btn_confirm_dialog_time_picker);
        CTextView btnCancel = view.findViewById(R.id.btn_close_dialog_time_picker);

        btnConfirm.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }


    private void initVars()
    {
        String[] hourArr = NumberUtils.numberArraySequenceGenerator(0, 24);
        String[] minArr = NumberUtils.numberArraySequenceGenerator(0, 60);
        hourArr = NumberUtils.englishToPersianNumberConvertor(hourArr);
        minArr = NumberUtils.englishToPersianNumberConvertor(minArr);
        pickerHour.setDisplayedValues(hourArr);
        pickerMin.setDisplayedValues(minArr);

        pickerHour.setMinValue(0);
        pickerHour.setMaxValue(hourArr.length - 1);

        pickerMin.setMinValue(0);
        pickerMin.setMaxValue(minArr.length - 1);
    }


    public void show()
    {
        if (dialog != null)
        {
            dismiss();
            dialog.show();
        }
    }


    public void dismiss()
    {
        if (dialog.isShowing())
        {
            dialog.dismiss();
            dialog = null;
            intTimePickerDialogResult = null;
            btnConfirm = null;
            pickerHour = null;
            pickerMin = null;
        }
    }

    @Override
    public void onClick(View view)
    {
        if (view == btnConfirm)
        {
            hour = pickerHour.getValue();
            min = pickerMin.getValue();
            String strPickedTime = String.format(Locale.ENGLISH, "%02d", min) + " : " + String.format(Locale.ENGLISH, "%02d", hour);
            intTimePickerDialogResult.onReponseTimePickerDialog(strPickedTime);
        }
        dismiss();
    }

    private void parseTimeAmount(String strTimeAmount)
    {
        if (strTimeAmount != null)
        {
            String[] time = strTimeAmount.split(":");
            min = Integer.parseInt(time[0].trim());
            hour = Integer.parseInt(time[1].trim());
            pickerHour.setValue(hour);
            pickerMin.setValue(min);
        }
    }
}

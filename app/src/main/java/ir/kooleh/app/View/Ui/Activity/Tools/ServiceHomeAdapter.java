package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;


import ir.kooleh.app.R;

public class ServiceHomeAdapter extends RecyclerView.Adapter<ServiceHomeAdapter.AdapterHolder> {


    private Context context;
    private List<ServiceObject> list;
    private static final String TAG = "Cannot invoke method";
    private OnClickItemListener itemListener ;

    public ServiceHomeAdapter(Context context , List<ServiceObject> list , OnClickItemListener itemListener) {

        this.context = context;
        this.list = list;
        this.itemListener = itemListener;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final AdapterHolder viewHolder = new AdapterHolder(LayoutInflater.from(context).inflate(R.layout.item_service_home, parent , false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdapterHolder holder, int position) {
        final ServiceObject item = list.get(position);
        holder.tvLocation.setText(item.getLocationName());
        Log.d(TAG, "onBindViewHolder: " + item.getLocationName());

        if (item.getIconRate()!=null && !item.getIconRate().equals(""))
            Glide.with(context).load(item.getIconRate()).error(R.drawable.img_splash_gibar).placeholder(R.drawable.img_splash_gibar).into(holder.ivImoji);
        else
            Glide.with(context).load(R.drawable.img_splash_gibar).into(holder.ivImoji);

        if (item.getImage()!=null && !item.getImage().equals(""))
            Glide.with(context).load(item.getImage()).error(R.drawable.img_splash_gibar).placeholder(R.drawable.img_splash_gibar).into(holder.ivProduct);
        else
            Glide.with(context).load(R.drawable.img_splash_gibar).into(holder.ivProduct);

        if (item.getSourceIcon()!=null && !item.getSourceIcon().equals(""))
            Glide.with(context).load(item.getSourceIcon()).into(holder.ivLocation);
        else
            Glide.with(context).load(R.drawable.ic_location).into(holder.ivLocation);



        holder.rvGroups.setAdapter(new GroupTypeAdapter(context, item.getGroups(), new GroupTypeAdapter.OnClickItemListener() {
            @Override
            public void onClickItem(View view, String item) {

            }
        }));



        if (item.getPrice()!=0 &&  item.getPrice() != -1 ) {

            if (item.getOff() != 0) {
                holder.tvPercentOff.setVisibility(View.VISIBLE);
                holder.tvPercentOff.setText(item.getOff() + "٪ تخفیف");
                holder.tvOriginalPrice.setText(item.getPrice() + " تومان");
                holder.tvOriginalPrice.setTextColor(context.getResources().getColor(R.color.red_100));
                int offerPrice = (int) (Double.valueOf(item.getPrice()) - (Double.valueOf(item.getOff()) * Double.valueOf(item.getPrice()) / 100));
                holder.tvFinalPrice.setText(offerPrice + " تومان");
                holder.tvFinalPrice.setTextColor(context.getResources().getColor(R.color.teal_300));
                holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {

                holder.tvPercentOff.setVisibility(View.GONE);

                //holder.tvPercentOff.setText(item.getOff()+"٪ تخفیف");
                holder.tvOriginalPrice.setText(item.getPrice() + " تومان");
                holder.tvOriginalPrice.setTextColor(context.getResources().getColor(R.color.teal_400));
                holder.tvOriginalPrice.setPaintFlags( holder.tvOriginalPrice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
//            int offerPrice =(int)( Double.valueOf(item.getPrice())-( Double.valueOf(item.getOff())*Double.valueOf(item.getPrice())/100));
//            holder.tvFinalPrice.setText(offerPrice+" تومان");
//            holder.tvFinalPrice.setTextColor(context.getResources().getColor(R.color.greenBlueDoLogin));
                //if (item.getc)
            }
        }else if  (item.getPrice() == -1) {
            holder.tvPercentOff.setVisibility(View.GONE);
            holder.tvOriginalPrice.setVisibility(View.GONE);

        }else {
            holder.tvPercentOff.setVisibility(View.GONE);
            holder.tvOriginalPrice.setPaintFlags( holder.tvOriginalPrice.getPaintFlags() & (~Paint.UNDERLINE_TEXT_FLAG));
            holder.tvOriginalPrice.setText("رایگان");
            holder.tvOriginalPrice.setTextColor(context.getResources().getColor(R.color.teal_400));
        }

        holder.tvTitle.setText(item.getTitle());
        holder.tvImojiPercent.setText(item.getRate());
        holder.tvDescription.setText(item.getStory());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemListener.onClickItem(v,item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class AdapterHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle, tvDescription;
        private ImageView ivProduct;
        private TextView tvLocation;
        private TextView tvImojiPercent;
        private TextView tvPercentOff;
        private TextView tvOriginalPrice;
        private TextView tvFinalPrice;
        private ImageView ivImoji;
        private ImageView ivLocation;

        private RecyclerView rvGroups;

        public AdapterHolder(View itemView) {
            super(itemView);
            ivProduct = (ImageView) itemView.findViewById(R.id.iv_picture_service_item);
            tvTitle = (TextView) itemView.findViewById  (R.id.tv_title_service_item);

            tvDescription = (TextView) itemView.findViewById(R.id.tv_date_service_item);
            tvLocation = (TextView) itemView.findViewById(R.id.tv_location_service_item);
            tvImojiPercent = (TextView) itemView.findViewById(R.id.tv_percent_emoji_service_item);
            tvPercentOff = (TextView) itemView.findViewById(R.id.tv_percent_off_service_item);
            tvOriginalPrice = (TextView) itemView.findViewById(R.id.tv_original_price_service_item);
            tvFinalPrice = (TextView) itemView.findViewById(R.id.tv_final_price_service_item);

            ivImoji = (ImageView) itemView.findViewById(R.id.iv_emoji_service_item);
            ivLocation = (ImageView) itemView.findViewById(R.id.iv_location_service_item);
            rvGroups = (RecyclerView)itemView.findViewById(R.id.rv_group_service_item);
        }
    }

    public interface OnClickItemListener{
        void onClickItem(View view, ServiceObject item);
    }
}

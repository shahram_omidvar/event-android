package ir.kooleh.app.View.Ui.Fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import ir.kooleh.app.R;
import ir.kooleh.app.View.ViewCustom.CTextView;


public class FragmentWebView extends Fragment
{

    private View view;
    private ImageView ivClose;
    private CTextView tvTitle;
    private SwipeRefreshLayout swipeRefreshLayout;
    private WebView webView;
    private ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_web_view, container, false);

        showWebView();
        return view;
    }

    private void showWebView()
    {

        ivClose = view.findViewById(R.id.iv_close_toolbar_web_view);
        tvTitle = view.findViewById(R.id.tv_title_toolbar_web_view);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_fragment_webview);
        ivClose.setVisibility(View.GONE);
        progressBar = view.findViewById(R.id.progress_webview_fragment);
        webView = view.findViewById(R.id.webview_fragment);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.primary), getResources().getColor(R.color.color_primary));

        webView.setWebViewClient(WebViewClient);
        webView.setWebChromeClient(webChromeClient);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(getArguments().getString("value"));

        swipeRefreshLayout.setOnRefreshListener(() ->
        {
            swipeRefreshLayout.setRefreshing(true);
            progressBar.setProgress(10);
            webView.reload();
        });
    }





    private final android.webkit.WebViewClient WebViewClient = new WebViewClient()
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);
            swipeRefreshLayout.setRefreshing(false);
        }
    };

    private final WebChromeClient webChromeClient = new WebChromeClient()
    {
        @Override
        public void onReceivedTitle(WebView view, String title)
        {
            super.onReceivedTitle(view, title);
            if (tvTitle != null)
            {
                tvTitle.setText(title);
            }
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            if(newProgress < 100 && progressBar.getVisibility() == ProgressBar.GONE)
            {
                progressBar.setVisibility(ProgressBar.VISIBLE);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                progressBar.setProgress(newProgress, true);
            }
            else
            {
                progressBar.setProgress(newProgress);
            }
            if (newProgress == 100)
            {
                progressBar.setVisibility(ProgressBar.GONE);
            }
        }
    };

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            if (keyCode == KeyEvent.KEYCODE_BACK)
            {
                if (webView.canGoBack())
                {
                    webView.goBack();
                    return true;
                }
            }
        }
        return false;
    }



    @Override
    public void onPause() {
        webView.onPause();
        super.onPause();

    }

    @Override
    public void onResume() {
        webView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        webView.destroy();
        super.onDestroy();
    }
}


package ir.kooleh.app.View.Ui.Activity.Tools;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TabAboutService  {

    @SerializedName("tab_name")
    @Expose
    private String tabName;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("compliments")
    @Expose
    private List<Compliment> compliments = null;
    @SerializedName("related_items")
    @Expose
    private List<ServiceObject> serviceObjects = null;
    @SerializedName("creator")
    @Expose
    private Creator creator;

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public List<Compliment> getCompliments() {
        return compliments;
    }

    public void setCompliments(List<Compliment> compliments) {
        this.compliments = compliments;
    }

    public List<ServiceObject> getServiceObjects() {
        return serviceObjects;
    }

    public void setServiceObjects(List<ServiceObject> serviceObjects) {
        this.serviceObjects = serviceObjects;
    }

    public Creator getCreator() {
        return creator;
    }

    public void setCreator(Creator creator) {
        this.creator = creator;
    }

}

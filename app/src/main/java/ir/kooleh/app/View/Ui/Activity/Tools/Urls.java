package ir.kooleh.app.View.Ui.Activity.Tools;

public class Urls {

    public static final String BASE_URL = "https://usbplus.ir/api/v1/";
//    public static final String BASE_URL = "http://192.168.43.152:8000/api/v1/";
//    public static final String BASE_URL = "http://parlife.ir/api/v1/";
    ///login url
    public static final String CHECK_EXISTS_USER = BASE_URL + "checkExistsUser";
    public static final String SEND_CODE = BASE_URL + "sendCode";
    public static final String Login = BASE_URL + "login";
    public static final String Register = BASE_URL + "register";
    public static final String VERIFY_CODE = BASE_URL + "verifyCode";
    public static final String VERIFY_CODE_LOGIN = BASE_URL + "verifyCode";
    public static final String CHECK_USER_NAME = BASE_URL + "checkUsername";




    ///service page
    public static final String GET_SLIDER = BASE_URL + "getSlider";
    public static final String GET_FILTER = BASE_URL + "getFilters";
    public static final String GET_SERVICE_FILTER = BASE_URL + "getServicesWithFilter";
    public static final String GET_SERVICE = BASE_URL + "getService";
    public static final String SAVE_FEED_COMMENT = BASE_URL + "saveFeedComment";
    public static final String GET_FEED_COMMENT = BASE_URL + "getFeedComment";
    public static final String SET_BOOK_MARK = BASE_URL + "setBookMark";
    public static final String GET_SERVICE_COMMENTS = BASE_URL + "getServiceComments";
    public static final String CALC_PERCENTAGE_POINTS = BASE_URL + "calcPercentagePoints";
    public static final String EDIT_PROFILE = BASE_URL + "editProfile";
    public static final String ACTIVITY_REG_SERVICE = BASE_URL + "activityRegService";

    public static final String REG_ACTION_WITH_QR = BASE_URL + "regActionWithQR";


    ///search page
    public static final String SEARCH = BASE_URL + "search";
    public static final String SEARCH_AUTO_COMPLETE = BASE_URL + "searchAutoComplete";
    public static final String SEARCH_MODEL_App = BASE_URL + "searchModelUserApp";

    //profile
    public static final String GET_MY_PROFILE = BASE_URL + "getMyProfile";

    //tops
    public static final String GET_CATEGORIES = BASE_URL + "getCategories";
    public static final String GET_TOP_USER = BASE_URL + "getTopUser";

    //Feed
    public static final String CREATE_FEED = BASE_URL + "createFeed";

}

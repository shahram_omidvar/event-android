package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Comment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.kooleh.app.R;
import ir.kooleh.app.Utility.Volley.Controller;


public class ServiceApi {

    public static ServiceApi instance;
    private Context context;
    private Gson gsonParser;
    private static final String TAG = "Cannot invoke method";

    private ServiceApi(Context context) {
        this.context = context;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonParser = gsonBuilder.create();
    }

    public static ServiceApi getInstance(Context context) {
        if (instance == null) {
            instance = new ServiceApi(context);
        }
        return instance;
    }

    /* web services*/
    public void getService(final String serviceId, @Nullable final String userId, final CompletionGetService completion) {

        StringRequest request = new StringRequest(Request.Method.POST, Urls.GET_SERVICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, "onResponse: " + response);
                            JSONObject jsonResponse = new JSONObject(response);
                            completion.onGetServiceResponse(true,
                                    gsonParser.fromJson(jsonResponse.getJSONObject("basic").toString(), BasicService.class),
                                    gsonParser.fromJson(jsonResponse.getJSONObject("tab1").toString(), TabAboutService.class),
                                    gsonParser.fromJson(jsonResponse.getJSONObject("tab2").toString(), TabInformationService.class),
                                    "درخواست انجام شد");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Log.e(TAG, "onErrorResponse: " + error);
                completion.onGetServiceResponse(false, null, null, null,
                        "لطفا تنظیمات اینترنت را بررسی کنید");
            }
        }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", serviceId);
                if (userId != null) {
                    params.put("user_id", userId);
                }
//                params.put("android_id",usernameShared.getAndroid_id());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public interface CompletionGetService {
        void onGetServiceResponse(boolean isOk, BasicService basicService,
                                  TabAboutService aboutService, TabInformationService informationService, String message);
    }

    /* web services*/
    public void getPercentagePoints(final String serviceId, final CompletionPercentagePoints completion) {
        Log.d(TAG, "getPercentagePoints: "+serviceId);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.CALC_PERCENTAGE_POINTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, "onResponse: " + response);
                            JSONObject jsonResponse = new JSONObject(response);
                            completion.onPercentagePointsResponse(jsonResponse.getBoolean("ok"),
                                    gsonParser.fromJson(jsonResponse.getJSONObject("result").toString(), PercentagePoints.class),
                                    "درخواست انجام شد");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Log.e(TAG, "onErrorResponse: ", error);
                completion.onPercentagePointsResponse(false, null,
                        "لطفا تنظیمات اینترنت را بررسی کنید");
            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("service_id", serviceId);
                //params.put("user_id", userId);
//                params.put("android_id",usernameShared.getAndroid_id());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public interface CompletionPercentagePoints {
        void onPercentagePointsResponse(boolean isOk, PercentagePoints basicService, String message);
    }

    public void getServiceComments(final String serviceId, final CompletionServiceComments completion) {
        StringRequest request = new StringRequest(Request.Method.POST, Urls.GET_SERVICE_COMMENTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, "onResponse: " + response);
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.getBoolean("ok"))
                                completion.onCommentResponse(true,
                                        Arrays.asList(gsonParser.fromJson(jsonResponse.getJSONArray("result").toString(), Comment[].class)), "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                completion.onCommentResponse(false, null,
                        "لطفا تنظیمات اینترنت را بررسی کنید");
            }
        }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("service_id", serviceId);
                //params.put("user_id", userId);
//                params.put("android_id",usernameShared.getAndroid_id());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public interface CompletionServiceComments {
        void onCommentResponse(boolean isOk, List<Comment> comments, String message);
    }

    /* web services*/
    public void getUserFeedForService(final String apiToken, final String serviceId, final CompletionUserFeed completion) {
        StringRequest request = new StringRequest(Request.Method.POST, Urls.GET_MY_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, "onResponse: " + response);
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.getBoolean("ok")) {
                                completion.response(true,gsonParser.fromJson(jsonResponse.getJSONObject("result").toString(),FeedBack.class) ,
                                        "درخواست انجام شد");
                            } else {

                                completion.response(false, null, jsonResponse.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            completion.response(false, null, Controller.context.getString(R.string.report_to_support));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Log.e(TAG, "onErrorResponse: " + error);
                completion.response(false, null, "لطفا تنظیمات اینترنت را بررسی کنید");
            }
        }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + apiToken);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("service_id", serviceId);
//                params.put("android_id",usernameShared.getAndroid_id());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public interface CompletionUserFeed {
        void response(boolean isOk, FeedBack feedBack, String message);
    }


    /* web services*/
    public void sendComment(final String apiToken, final String serviceId, final String text, final String rate, final CompletionSendComment completion) {
        StringRequest request = new StringRequest(Request.Method.POST, Urls.SAVE_FEED_COMMENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, "onResponse: " + response);
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.getBoolean("ok")) {
                                completion.response(true,
                                        jsonResponse.getString("message"));
                            } else {

                                completion.response(false,   jsonResponse.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            completion.response(false, Controller.context.getString(R.string.report_to_support));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Log.e(TAG, "onErrorResponse: " + error);
                completion.response(false, "لطفا تنظیمات اینترنت را بررسی کنید");
            }
        }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + apiToken);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("service_id", serviceId);
                params.put("text", text);
                params.put("point", rate);
//                params.put("android_id",usernameShared.getAndroid_id());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public interface CompletionSendComment {
        void response(boolean isOk, String message);
    }


    /* web services*/
    public void doBookmark(final String apiToken, final String serviceId, final int isBookmark, final CompletionDoBookmark completion) {
        StringRequest request = new StringRequest(Request.Method.POST, Urls.SET_BOOK_MARK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.getBoolean("ok")) {
                                completion.response(true,isBookmark
                                        ,jsonResponse.getString("message"));
                            } else {

                                completion.response(false,  ~isBookmark, jsonResponse.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            completion.response(false,~isBookmark, Controller.context.getString(R.string.report_to_support));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Log.e(TAG, "onErrorResponse: " + error);
                completion.response(false, 0,"لطفا تنظیمات اینترنت را بررسی کنید");
            }
        }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + apiToken);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("service_id", serviceId);
                params.put("is_set",""+isBookmark );
//                params.put("android_id",usernameShared.getAndroid_id());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public interface CompletionDoBookmark {
        void response(boolean isOk,int isBookmark, String message);
    }


    public void activityRegService(final String apiToken, final String serviceId, final String  link, final String lat,final String lng, final CompletionActivityRegService completion) {
        StringRequest request = new StringRequest(Request.Method.POST, Urls.ACTIVITY_REG_SERVICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, "onResponse: " + response);
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.getBoolean("ok")) {
                                completion.response(true,
                                        jsonResponse.getString("message"));
                            } else {

                                completion.response(false,   jsonResponse.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            completion.response(false, Controller.context.getString(R.string.report_to_support));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Log.e(TAG, "onErrorResponse: " + error);
                completion.response(false, "لطفا تنظیمات اینترنت را بررسی کنید");
            }
        }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + apiToken);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("service_id", serviceId);
                params.put("link",link );
                if (lat != null && !lat.equals("")&& lng != null && !lng.equals("")) {
                    params.put("lat", lat);
                    params.put("lng", lng);
                }
//                params.put("android_id",usernameShared.getAndroid_id());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public interface CompletionActivityRegService {
        void response(boolean isOk, String message);
    }

}

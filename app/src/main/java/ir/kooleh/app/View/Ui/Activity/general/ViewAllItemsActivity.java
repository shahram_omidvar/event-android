package ir.kooleh.app.View.Ui.Activity.general;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Bill.ApiBills;
import ir.kooleh.app.Server.Api.Wallet.ApiPackage;
import ir.kooleh.app.Server.Api.Wallet.ApiWallet;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Enum.PaginateValues;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceUserBill;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Interface.Wallet.InterfacePackage;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceWallet;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.Structure.Model.Bills.ModelUserBill;
import ir.kooleh.app.Structure.Model.Wallet.ModelLogWallet;
import ir.kooleh.app.Structure.Model.Wallet.ModelUserPackage;
import ir.kooleh.app.Utility.Adapter.Bill.AdapterUserBill;
import ir.kooleh.app.Utility.Adapter.Wallet.AdapterLogWallet;
import ir.kooleh.app.Utility.Adapter.Wallet.AdapterUserPackages;
import ir.kooleh.app.Utility.Class.CustomLayoutPaginate;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.Utility.Class.MessageHandlerUtils;
import ir.kooleh.app.View.Ui.Activity.Bill.BillDetailsActivity;
import ir.kooleh.app.View.Ui.Activity.Bill.BillInquiryActivity;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class ViewAllItemsActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfaceMessageHandler.FragmentResult,
        InterfacePackage.Package.IntGetUserPackages,
        InterfaceWallet.Wallet.IntGetLogsWalletUser,
//        InterfaceUserBill.UserBill.IntGetUserBills,
        InterfaceUserBill.BillInquiry.IntClickBillItem,
        InterfaceTryAgainPaginate,
        Paginate.Callbacks, InterfaceUserBill.BillInquiry.IntInquireBill
{
    private ImageView ivBackToolbar;
    private CTextView tvtitleToolbar;
    private LinearLayout lnrViewAllItems;
    private FrameLayout containerFragmentMessageHandler;
    private FragmentMessageHandler fragmentMessageHandler;
    private RecyclerView rvViewAllItems;

    private AdapterUserPackages adapterUserPackages;
    private AdapterLogWallet adapterLogWallet;
    private AdapterUserBill adapterUserBill;

    private String itemsType;

    public static final String TITLE_TOOLBAR = "titleToolbar";
    public static final String ITEMS_TYPE = "itemsType";
    public static final String ITEMS_TYPE_PACKAGE = "itemsTypePackage";
    public static final String ITEMS_TYPE_WALLET = "itemsTypeWallet";
    public static final String ITEMS_TYPE_BILL = "itemsTypeBill";

    private ApiPackage apiPackage;
    private ApiWallet apiWallet;
    private ApiBills apiBills;

    private ViewLoading viewLoading;

    private ModelPaginate modelPaginate;
    private List<ModelUserPackage> userPackageList = new ArrayList<>();
    private List<ModelLogWallet> logWalletList = new ArrayList<>();
    private List<ModelUserBill> userBillList = new ArrayList<>();
    private Paginate paginate;

    private boolean loading = true;
    private int nextPage = 1;

    private ModelBillCategories.Child modelBillType;

    private Parcelable recylerViewState;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_items);
        initViews();
        initVars();
    }

    private void initViews()
    {
        ivBackToolbar = findViewById(R.id.iv_back_toolbar);
        tvtitleToolbar = findViewById(R.id.tv_title_toolbar);
        lnrViewAllItems = findViewById(R.id.lnr_view_all_items);
        rvViewAllItems = findViewById(R.id.rv_view_all_items);
        containerFragmentMessageHandler = findViewById(R.id.container_message_handler_fragment);

        ivBackToolbar.setOnClickListener(this);
    }

    private void initVars()
    {
        itemsType = getIntent().getExtras().getString(ITEMS_TYPE, ITEMS_TYPE_PACKAGE);
        tvtitleToolbar.setText(getIntent().getExtras().getString(TITLE_TOOLBAR, "لیست بسته ها"));

        viewLoading = new ViewLoading(this);
        viewLoading.show();

        rvViewAllItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        switch (itemsType)
        {
            case ITEMS_TYPE_PACKAGE:
                apiPackage = new ApiPackage(this);
                adapterUserPackages = new AdapterUserPackages(this);
                rvViewAllItems.setAdapter(adapterUserPackages);
                apiPackage.getUserPackages(this, nextPage, PaginateValues.itemsPerPage);
                break;
            case ITEMS_TYPE_WALLET:
                apiWallet = new ApiWallet(this);
                adapterLogWallet = new AdapterLogWallet(this);
                rvViewAllItems.setAdapter(adapterLogWallet);
                apiWallet.getLogsWalletUser(this, nextPage, PaginateValues.itemsPerPage);
                break;
            case ITEMS_TYPE_BILL:
//                GsonBuilder builder = new GsonBuilder();
//                Gson gson = builder.create();
//                modelBillType = gson.fromJson(getIntent().getExtras().getString(BillInquiryActivity.KEY_OBJECT_BILL_TYPE), ModelBillCategories.Child.class);
//
//                findViewById(R.id.toolbar_app).setBackgroundColor(Color.parseColor(modelBillType.getBackColor()));
//                ivBackToolbar.setColorFilter(getResources().getColor(R.color.gray_650), PorterDuff.Mode.SRC_IN);
//
//                Window window = getWindow();
//                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//                window.setStatusBarColor(ColorUtils.darken(
//                        Color.parseColor(modelBillType.getBackColor()), .2));
//                tvtitleToolbar.setTextColor(getResources().getColor(R.color.gray_650));
//
//                apiBills = new ApiBills(this);
//                adapterUserBill = new AdapterUserBill(this, modelBillType.getName(), modelBillType.getBackColor(), modelBillType.getImage());
//                rvViewAllItems.setAdapter(adapterUserBill);
//                apiBills.getUserBills(this, modelBillType.getId(), nextPage, PaginateValues.itemsPerPage);
                break;
        }
    }

    private void showEmptyListMessage()
    {
        containerFragmentMessageHandler.setVisibility(View.VISIBLE);
        fragmentMessageHandler = new FragmentMessageHandler(this);
        Bundle args = new Bundle();
        args.putString("message", "آیتمی برای نمایش پیدا نشد");
        args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.EMPTY);
        fragmentMessageHandler.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
    }

    private void showError(String message)
    {
        containerFragmentMessageHandler.setVisibility(View.VISIBLE);
        fragmentMessageHandler = new FragmentMessageHandler(this);
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, MessageHandlerUtils.getMessageTypeByMessage(message));
        fragmentMessageHandler.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container_message_handler_fragment, fragmentMessageHandler).commitAllowingStateLoss();
    }

//    @Override
//    public void onResponseGetUserBills(boolean response, String message, List<ModelUserBill> userBillList, ModelPaginate modelPaginate)
//    {
//        viewLoading.close();
//        lnrViewAllItems.setVisibility(View.GONE);
//        containerFragmentMessageHandler.setVisibility(View.GONE);
//        if (response)
//        {
//            lnrViewAllItems.setVisibility(View.VISIBLE);
//            loading = false;
//            this.userBillList.addAll(userBillList);
//            this.modelPaginate = modelPaginate;
//            if (this.userBillList != null && this.userBillList.size() == 0)
//            {
//                lnrViewAllItems.setVisibility(View.GONE);
//                showEmptyListMessage();
//            }
//            else
//            {
//                adapterUserBill.addList(userBillList);
//                if (paginate == null)
//                {
//                    paginate = Paginate.with(rvViewAllItems, this)
//                            .setLoadingTriggerThreshold(PaginateValues.threshold)
//                            .addLoadingListItem(true)
//                            .setLoadingListItemCreator(new CustomLayoutPaginate())
//                            .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
//                            .build();
//                    rvViewAllItems.getLayoutManager().onRestoreInstanceState(recylerViewState);
//                }
//            }
//        }
//        else
//        {
//            if (nextPage == 1)
//            {
//                showError(message);
//            }
//            else
//            {
//                lnrViewAllItems.setVisibility(View.VISIBLE);
//                adapterUserBill.showBtnTryAgain();
//                if (paginate != null)
//                {
//                    paginate.unbind();
//                }
//            }
//        }
//    }

    @Override
    public void onResponseGetUserPackages(boolean response, String message, List<ModelUserPackage> userPackageList, ModelPaginate modelPaginate)
    {
        viewLoading.close();
        lnrViewAllItems.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);

        if (response)
        {
            lnrViewAllItems.setVisibility(View.VISIBLE);
            loading = false;
            this.userPackageList.addAll(userPackageList);
            this.modelPaginate = modelPaginate;
            if (userPackageList != null && userPackageList.size() == 0)
            {
                lnrViewAllItems.setVisibility(View.GONE);
                showEmptyListMessage();
            }
            else
            {
                adapterUserPackages.addList(userPackageList);
                if (paginate == null)
                {
                    paginate = Paginate.with(rvViewAllItems, this)
                            .setLoadingTriggerThreshold(PaginateValues.threshold)
                            .addLoadingListItem(true)
                            .setLoadingListItemCreator(new CustomLayoutPaginate())
                            .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
                            .build();
                    rvViewAllItems.getLayoutManager().onRestoreInstanceState(recylerViewState);
                }
            }
        }
        else {
            if (nextPage == 1)
            {
                showError(message);
            }
            else
            {
                lnrViewAllItems.setVisibility(View.VISIBLE);
                adapterUserPackages.showBtnTryAgain();
                if (paginate != null)
                {
                    paginate.unbind();
                }
            }
        }
    }

    @Override
    public void onResponseGetLogsWalletUser(boolean response, String message, List<ModelLogWallet> logWalletList, ModelPaginate modelPaginate)
    {
        viewLoading.close();
        lnrViewAllItems.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);
        if (response)
        {
            lnrViewAllItems.setVisibility(View.VISIBLE);
            loading = false;
            this.logWalletList.addAll(logWalletList);
            this.modelPaginate = modelPaginate;
            if (logWalletList != null && logWalletList.size() == 0)
            {
                lnrViewAllItems.setVisibility(View.GONE);
                showEmptyListMessage();
            }
            else
            {
                adapterLogWallet.addList(logWalletList);
                if (paginate == null)
                {
                    paginate = Paginate.with(rvViewAllItems, this)
                            .setLoadingTriggerThreshold(PaginateValues.threshold)
                            .addLoadingListItem(true)
                            .setLoadingListItemCreator(new CustomLayoutPaginate())
                            .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
                            .build();
                    rvViewAllItems.getLayoutManager().onRestoreInstanceState(recylerViewState);
                }
            }
        }
        else
        {
            if (nextPage == 1)
            {
                showError(message);
            }
            else
            {
                lnrViewAllItems.setVisibility(View.VISIBLE);
                adapterLogWallet.showBtnTryAgain();
                if (paginate != null)
                {
                    paginate.unbind();
                }
            }
        }
    }

    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        viewLoading.show();
        resetList();
    }

    private void resetList()
    {
        nextPage = 1;
        if (paginate != null)
        {
            paginate.unbind();
            paginate = null;
        }

        switch (itemsType)
        {
            case ITEMS_TYPE_PACKAGE:
                userPackageList = new ArrayList<>();
                rvViewAllItems.setAdapter(adapterUserPackages = new AdapterUserPackages(this));
                apiPackage.getUserPackages(this, nextPage, PaginateValues.itemsPerPage);
                break;
            case ITEMS_TYPE_WALLET:
                logWalletList = new ArrayList<>();
                rvViewAllItems.setAdapter(adapterLogWallet = new AdapterLogWallet(this));
                apiWallet.getLogsWalletUser(this, nextPage, PaginateValues.itemsPerPage);
                break;
            case ITEMS_TYPE_BILL:
//                userBillList = new ArrayList<>();
//                rvViewAllItems.setAdapter(adapterUserBill = new AdapterUserBill(this, modelBillType.getName(), modelBillType.getBackColor(), modelBillType.getImage()));
//                apiBills.getUserBills(this, modelBillType.getId(), nextPage, PaginateValues.itemsPerPage);
                break;
        }

    }

    @Override
    public void onLoadMore()
    {
        Log.i("paginateOnloadMore", String.valueOf(rvViewAllItems.getAdapter().getItemCount()));
        loading = true;
        nextPage++;
        switch (itemsType)
        {
            case ITEMS_TYPE_PACKAGE:
                apiPackage.getUserPackages(this, nextPage, PaginateValues.itemsPerPage);
                break;
            case ITEMS_TYPE_WALLET:
                apiWallet.getLogsWalletUser(this, nextPage, PaginateValues.itemsPerPage);
                break;
            case ITEMS_TYPE_BILL:
//                apiBills.getUserBills(this, modelBillType.getId(), nextPage, PaginateValues.itemsPerPage);
                break;
        }
    }

    @Override
    public boolean isLoading()
    {
        Log.i("paginateisloading", String.valueOf(loading));
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems()
    {
        if (modelPaginate != null)
        {
            Log.i("paginateHasloadedAll", String.valueOf(Integer.parseInt(modelPaginate.getTotal())));
            switch (itemsType)
            {
                case ITEMS_TYPE_PACKAGE:
                    return userPackageList.size() == Integer.parseInt(modelPaginate.getTotal());
                case ITEMS_TYPE_WALLET:
                    return logWalletList.size() == Integer.parseInt(modelPaginate.getTotal());
                case ITEMS_TYPE_BILL:
                    return userBillList.size() == Integer.parseInt(modelPaginate.getTotal());
            }
        }
        return  false;
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivBackToolbar)
        {
            finish();
        }
    }

    @Override
    public void onResponseTryAgainPaginate()
    {
        paginate = null;
        recylerViewState = rvViewAllItems.getLayoutManager().onSaveInstanceState();

        switch (itemsType)
        {
            case ITEMS_TYPE_PACKAGE:
                apiPackage.getUserPackages(this, nextPage, PaginateValues.itemsPerPage);
                break;
            case ITEMS_TYPE_WALLET:
                apiWallet.getLogsWalletUser(this, nextPage, PaginateValues.itemsPerPage);
                break;
            case ITEMS_TYPE_BILL:
//                apiBills.getUserBills(this, modelBillType.getId(), nextPage, PaginateValues.itemsPerPage);
                break;
        }
    }

    @Override
    public void onResponseIntClickBillItem(String billID)
    {
        viewLoading.show();
        apiBills.inquireBill(this, modelBillType.getName(), billID);
    }

    @Override
    public void onResponseInquireBill(boolean response, String message, String billInquiryResult)
    {
        viewLoading.close();
        if (response)
        {
            Intent intent = new Intent(this, BillDetailsActivity.class);
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            intent.putExtra(BillInquiryActivity.KEY_OBJECT_BILL_TYPE, gson.toJson(modelBillType));
            intent.putExtra(BillDetailsActivity.KEY_BILL_DETAILS, billInquiryResult);
            startActivity(intent);
        }
        else
        {
            CustomToast customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }
}

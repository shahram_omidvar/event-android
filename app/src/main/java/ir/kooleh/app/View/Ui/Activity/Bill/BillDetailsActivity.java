package ir.kooleh.app.View.Ui.Activity.Bill;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.cachapa.expandablelayout.ExpandableLayout;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Wallet.ApiBuys;
import ir.kooleh.app.Structure.Enum.EnumWalletModule;
import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.Structure.Model.Bills.ModelBillInquiry;
import ir.kooleh.app.Utility.Adapter.Bill.AdapterBillDetails;
import ir.kooleh.app.Utility.Class.ColorUtils;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Activity.general.PaymentActivity;
import ir.kooleh.app.View.ViewCustom.CRadioButton;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class BillDetailsActivity extends AppCompatActivity implements View.OnClickListener
{
    private Toolbar toolbar;
    private ImageView ivBackToolbar;
    private CTextView tvTitleToolbar;
    private CTextView tvBillID, tvPaymentID, tvPrice, tvPaymentStatus;
    private CTextView tvTitleBillDetails;
    private ImageView ivIconBill;
    private CardView containerTitleBillDetails;
    private RecyclerView rvBaseBillDetails;
    private RecyclerView rvExtraBillDetails;
    private FrameLayout btnExpandBillDetails;
    private ExpandableLayout exLayoutMoreBillDetails;
    private ImageView ivIconMoreBillDetails;
    private CTextView tvMoreBillDetails;
    private CardView btnPayBill;
    private RadioGroup radioGroup;

    private ApiBuys apiBuys;
    private ViewLoading viewLoading;

    private RotateAnimation rotateAnimationUp, rotateAnimationDown;
    private CRadioButton[] cRadioButtons;



    private ModelBillInquiry modelBillInquiry;
    private AdapterBillDetails adapterBaseBillDetails;
    private AdapterBillDetails adapterExtraBillDetails;

    private String price;
    private String selectedPaymentID;

    public static final String KEY_BILL_DETAILS = "billDetails";

    ModelBillCategories.Child modelBillType;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_details);
        initViews();
        initVars();
    }

    private void initViews()
    {
        toolbar = findViewById(R.id.toolbar_app);
        ivBackToolbar = findViewById(R.id.iv_back_toolbar);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        tvTitleBillDetails = findViewById(R.id.tv_title_bill_details);
        tvBillID = findViewById(R.id.tv_bill_id_bill_details);
        tvPaymentID = findViewById(R.id.tv_payment_id_bill_details);
        tvPrice = findViewById(R.id.tv_price_bill_details);
        tvPaymentStatus = findViewById(R.id.tv_bill_pay_status_bill_details);
        ivIconBill = findViewById(R.id.iv_icon_bill_details);
        containerTitleBillDetails = findViewById(R.id.container_title_bill_details);
        rvBaseBillDetails = findViewById(R.id.rv_base_bill_details);
        rvExtraBillDetails = findViewById(R.id.rv_extra_bill_details);
        btnExpandBillDetails = findViewById(R.id.btn_expand_collapse_more_bill_details);
        exLayoutMoreBillDetails = findViewById(R.id.ex_layout_bill_extra_details);
        ivIconMoreBillDetails = findViewById(R.id.iv_icon_expand_collapse_more_bill_details);
        tvMoreBillDetails = findViewById(R.id.tv_expand_collapse_more_bill_details);
        btnPayBill = findViewById(R.id.btn_pay_bill);
        radioGroup = findViewById(R.id.radio_group_bill_details);

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        modelBillType = gson.fromJson(getIntent().getExtras().getString(BillInquiryActivity.KEY_OBJECT_BILL_TYPE), ModelBillCategories.Child.class);

        tvTitleToolbar.setText(modelBillType.getfName());
        tvTitleToolbar.setTextColor(getResources().getColor(R.color.gray_650));

        Glide.with(this).load(modelBillType.getImage()).into(ivIconBill);
        ivIconBill.setColorFilter(Color.parseColor(modelBillType.getBackColor()), PorterDuff.Mode.SRC_IN);

        containerTitleBillDetails.setCardBackgroundColor(Color.parseColor(modelBillType.getBackColor()));
        toolbar.setBackgroundColor(Color.parseColor(modelBillType.getBackColor()));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ColorUtils.darken(
                Color.parseColor(modelBillType.getBackColor()), .2));

        ivIconMoreBillDetails.setColorFilter(getResources().getColor(R.color.primary), PorterDuff.Mode.SRC_IN);

        rvBaseBillDetails.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvExtraBillDetails.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        ivBackToolbar.setOnClickListener(this);
        btnExpandBillDetails.setOnClickListener(this);
        btnPayBill.setOnClickListener(this);
    }

    private void initVars()
    {
        adapterBaseBillDetails = new AdapterBillDetails(this);
        adapterExtraBillDetails = new AdapterBillDetails(this);

        apiBuys = new ApiBuys(this);
        viewLoading = new ViewLoading(this);

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        modelBillInquiry = gson.fromJson(getIntent().getExtras().getString(KEY_BILL_DETAILS), ModelBillInquiry.class);
        tvTitleBillDetails.setText("جزییات قبض " + modelBillInquiry.getName());

        rvBaseBillDetails.setAdapter(adapterBaseBillDetails);
        rvExtraBillDetails.setAdapter(adapterExtraBillDetails);

        adapterBaseBillDetails.setList(modelBillInquiry.getData().getBase());
        adapterExtraBillDetails.setList(modelBillInquiry.getData().getExtraInfo());

        tvBillID.setText("شناسه قبض: " + modelBillInquiry.getBillId());
        tvPaymentID.setText("شناسه پرداخت: " + modelBillInquiry.getPaymentId());
        tvPrice.setText("مبلغ قبض: " + NumberUtils.digitDivider(modelBillInquiry.getPrice()) + " ریال");
        tvPaymentStatus.setText("وضعیت: " + modelBillInquiry.getPaymentStatusName());

        price = modelBillInquiry.getPrice();
        selectedPaymentID = modelBillInquiry.getPaymentId();

        createRadioButton();

        if (modelBillInquiry.getPaymentStatus().equals("310"))
        {
            tvPrice.setVisibility(View.GONE);
            btnPayBill.setVisibility(View.GONE);
            tvPaymentStatus.setTextColor(getResources().getColor(R.color.green_400));
        }
        else
        {
            tvPaymentStatus.setTextColor(getResources().getColor(R.color.red_400));
        }

        if (modelBillInquiry.getCycle().size() == 0)
        {
            btnExpandBillDetails.setVisibility(View.GONE);
        }

        rotateAnimationUp = new RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimationUp.setRepeatCount(0);
        rotateAnimationUp.setDuration(300);
        rotateAnimationUp.setFillAfter(true);

        rotateAnimationDown = new RotateAnimation(180f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimationDown.setRepeatCount(0);
        rotateAnimationDown.setDuration(300);
        rotateAnimationDown.setFillAfter(true);

        radioGroup.setOnCheckedChangeListener((group, checkedId) ->
        {
            for (int i = 0; i < cRadioButtons.length; i++)
            {
                if (cRadioButtons[i].isChecked())
                {
                    price = modelBillInquiry.getCycle().get(i).getAmount();
                    selectedPaymentID = modelBillInquiry.getCycle().get(i).getPaymentId();
                    tvPaymentID.setText("شناسه پرداخت: " + modelBillInquiry.getCycle().get(i).getPaymentId());
                    tvPrice.setText("مبلغ قبض: " + NumberUtils.digitDivider(modelBillInquiry.getCycle().get(i).getAmount()) + " ریال");
                    Log.i("billdetails", "initVars: " + price);
                }
            }
        });
    }


    private void createRadioButton()
    {
        cRadioButtons = new CRadioButton[modelBillInquiry.getCycle().size()];
        radioGroup.setOrientation(RadioGroup.VERTICAL);
        for (int i = 0; i < modelBillInquiry.getCycle().size(); i++)
        {
            cRadioButtons[i] = new CRadioButton(this);
            radioGroup.addView(cRadioButtons[i]);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) cRadioButtons[i].getLayoutParams();
            layoutParams.topMargin = (int) getResources().getDimension(R.dimen.iiii_size);
            layoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.iiii_size);
            cRadioButtons[i].setTitleWith(modelBillInquiry.getCycle().get(i).getFaName());
            cRadioButtons[i].setValueWith(NumberUtils.digitDivider(modelBillInquiry.getCycle().get(i).getAmount()) + " ریال");
            if (i == modelBillInquiry.getCycle().size() - 1)
            {
                cRadioButtons[i].setChecked(true);
            }
        }
    }


    @Override
    public void onClick(View v)
    {
        if (v == btnPayBill)
        {
            Intent intent = new Intent(this, PaymentActivity.class);
            intent.putExtra(PaymentActivity.KEY_PRICE_PAYMENT_ACTIVITY, price);
            intent.putExtra(PaymentActivity.KEY_TITLE_TOOLBAR_PAYMENT_ACTIVITY, "پرداخت قبض " + tvTitleToolbar.getText().toString());
            intent.putExtra(PaymentActivity.KEY_MODEL_PAYMENT_ACTIVITY, EnumWalletModule.BuyModels.MODEL_BILL);
            intent.putExtra(PaymentActivity.KEY_PAYMENT_ID_PAYMENT_ACTIVITY, selectedPaymentID);
            intent.putExtra(PaymentActivity.KEY_BILL_ID_PAYMENT_ACTIVITY, modelBillInquiry.getId());
            startActivity(intent);
        }
        else if (v == ivBackToolbar)
        {
            finish();
        }
        else if (v == btnExpandBillDetails)
        {
            if (exLayoutMoreBillDetails.isExpanded())
            {
                tvMoreBillDetails.setText("جزییات بیشتر");
                tvMoreBillDetails.requestLayout();
                exLayoutMoreBillDetails.setVisibility(View.GONE);
                exLayoutMoreBillDetails.collapse();
                ivIconMoreBillDetails.startAnimation(rotateAnimationDown);
            }
            else
            {
                tvMoreBillDetails.setText("بستن");
                tvMoreBillDetails.requestLayout();
                exLayoutMoreBillDetails.setVisibility(View.VISIBLE);
                exLayoutMoreBillDetails.expand();
                ivIconMoreBillDetails.startAnimation(rotateAnimationUp);
            }
        }
    }
}
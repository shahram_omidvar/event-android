package ir.kooleh.app.View.Ui.Activity.Ads;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;

import android.net.Uri;
import android.os.Bundle;

import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.BuildConfig;
import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Ads.ApiAds;
import ir.kooleh.app.Server.Api.View.ApiView;
import ir.kooleh.app.Structure.Enum.EnCustomMessage;
import ir.kooleh.app.Structure.Enum.ListPermission;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomMessage;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomPreviewAd;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceGetPermissions;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.ListView.InterfaceView;
import ir.kooleh.app.Structure.Model.Ads.ModelPreviewAd;
import ir.kooleh.app.Structure.Model.Image.ModelListImage;

import ir.kooleh.app.Structure.Model.View.ModelView;
import ir.kooleh.app.Structure.Model.MultiSelection.ModelMultiSelection;

import ir.kooleh.app.Structure.Model.View.ModelViewValue;
import ir.kooleh.app.Utility.Adapter.Ads.Create.AdapterAdsCreateImage;
import ir.kooleh.app.Utility.Adapter.View.AdapterViewButton;
import ir.kooleh.app.Utility.Adapter.View.AdapterViewEditText;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogMessage;
import ir.kooleh.app.Utility.Class.CustomToast;

import ir.kooleh.app.Utility.Class.Image.ImageCropUtils;
import ir.kooleh.app.Utility.Class.ShowPermissionDialog;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogPreviewAd;
import ir.kooleh.app.Utility.Class.SettingKeyboard;
import ir.kooleh.app.View.Layout.AlertDialogView;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class CreateAdActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfaceAds.Server,
        InterfaceAds.IAdapterCreateAds,
        InterfaceCustomMessage,
        InterfaceView.EditText.AdapterEditText,
        InterfaceView.Server,
        InterfaceView.Button.AdapterButton,
        InterfaceView.AlertDialog.AlertDialogView,
        InterfaceCustomPreviewAd,
        InterfaceGetPermissions,
        InterfaceMessageHandler.FragmentResult
{
    //view
    private TextView tvTitle;
    private RecyclerView rvImageCreateAd;
    private EditText etTitleAd, etExplainAd;
    private TextInputLayout txtInputAdType;
    private CEditText etAdType;
    private TextInputLayout txtInputAuctionCountdownPicker;
    private CEditText etAuctionCountdownPicker;
    private RecyclerView rvViewEditTextCreateAd;
    private RecyclerView rvViewBtnCreateAd;
    private Button btnCreate;
    private ImageView ivBack;
    private NestedScrollView nsvLayout;
    private FrameLayout containerFragmentMessageHandler;


    //var
    private ViewLoading viewLoading;
    private CustomToast customToast;
    private CustomDialogPreviewAd customDialogPreviewAd;
    private ShowPermissionDialog showPermissionDialog;
    private FragmentMessageHandler fragmentMessageHandler;

    private ApiAds apiAds;
    private ApiView apiView;

    private AdapterViewEditText adapterViewEditText;
    private AdapterViewButton adapterViewButton;
    private AdapterAdsCreateImage adapterAdsCreateImage;

    private ModelListImage modelListImage;
    private ModelMultiSelection selectCategory;
    private ModelView modelView;

    private List<ModelListImage> listImage;
    private List<ModelPreviewAd> modelPreviewAdModelViewValueList;

    private AlertDialogView alertDialogView;
    private AlertDialog dial;
    private View menu;

    private String strAuctionTimeAmount;
    private List<ModelViewValue> listTypeAds;

    private String typeAdID = "9";
    private String auctionCountdown = "0";
    private int backCount = 0;
    private static final int REQUEST_CODE_CATEGORY_AD = 9000;
    private static final int REQUEST_CODE_GALLERY = 9005;
    private static final int REQUEST_CODE_CAMERA = 9006;
    private String idCategoryAd;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ad);

        initViews();
        initVars();
        getSelectedCategory();
    }


    private void initViews()
    {
        nsvLayout = findViewById(R.id.nsv_activity_create_ad);
        containerFragmentMessageHandler = findViewById(R.id.container_message_handler_fragment);
        tvTitle = findViewById(R.id.tv_title_toolbar);
        ivBack = findViewById(R.id.iv_back_toolbar);
        etTitleAd = findViewById(R.id.et_title_create_ad);
        etExplainAd = findViewById(R.id.et_explain_create_ad);
        rvImageCreateAd = findViewById(R.id.rv_image_create_ad);
        rvViewEditTextCreateAd = findViewById(R.id.rv_view_edit_text_create_ad);
        rvViewBtnCreateAd = findViewById(R.id.rv_view_button_create_ad);
        txtInputAdType = findViewById(R.id.txt_input_ad_type);
        etAdType = findViewById(R.id.et_ad_type);
        txtInputAuctionCountdownPicker = findViewById(R.id.txt_input_auction_countdown_picker);
        etAuctionCountdownPicker = findViewById(R.id.et_auction_countdown_picker);
        btnCreate = findViewById(R.id.btn_create_ad);

        tvTitle.setText("ایجاد آگهی");

        btnCreate.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        etAdType.setOnClickListener(this);
        etAdType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_next_red, 0, 0, 0);
        etAuctionCountdownPicker.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_next_red, 0, 0, 0);
    }


    private void initVars()
    {
        viewLoading = new ViewLoading(this);

        apiAds = new ApiAds(this);
        apiView = new ApiView(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateAdActivity.this);
        menu = LayoutInflater.from(CreateAdActivity.this).inflate(R.layout.layout_custom_choice_pic, null);
        builder.setView(menu);
        dial = builder.create();

        modelPreviewAdModelViewValueList = new ArrayList<>();

        showPermissionDialog = new ShowPermissionDialog(this, this);

        listImage = new ArrayList<>();

        for (int i = 0; i < 6; i++)
            listImage.add(new ModelListImage(i, "https://kooleh.ir/images/app/ic_image.png", "no_select", null));

        rvImageCreateAd.setAdapter(adapterAdsCreateImage = new AdapterAdsCreateImage(this, this));
        adapterAdsCreateImage.setListCreateImage(listImage);

        rvViewEditTextCreateAd.setAdapter(adapterViewEditText = new AdapterViewEditText(this, this));
        rvViewEditTextCreateAd.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        rvViewBtnCreateAd.setAdapter(adapterViewButton = new AdapterViewButton(this, getFragmentManager(), this));
        rvViewBtnCreateAd.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    }



    private void getSelectedCategory()
    {
        if (getIntent().getExtras().getString(MultiSelectionActivity.MULTI_SELECTION_RESULT, null) != null)
        {
            String result = getIntent().getExtras().getString(MultiSelectionActivity.MULTI_SELECTION_RESULT);
            selectCategory = new Gson().fromJson(result, ModelMultiSelection.class);
            idCategoryAd = selectCategory.getId();
            tvTitle.setText(String.format("آگهی : %s", selectCategory.getTitle()));
            getAttributeCategoryAd(selectCategory.getId());
        }
    }


    private String checkValueForCreateAd()
    {
        String msg = "";

        if (selectCategory == null)
            msg = "لطفا دسته بندی آگهی خود را انتخاب کنید";
        else if (etTitleAd.getText().toString().trim().length() < 2)
            msg = "لطفا عنوان آگهی رو وارد کنید (حداقل 2 کاراکتر)";
        else if (etExplainAd.getText().toString().trim().length() < 3)
            msg = "لطفا توضیحات آگهی رو وارد کنید (حداقل 3 کاراکتر)";
        else if (modelView.getListTypeAds().size() != 0 && etAdType.getText().toString().equals(""))
            msg = "لطفا نوع آگهی را انتخاب کنید";
        else if (typeAdID.equals("10") && etAuctionCountdownPicker.getText().toString().equals(""))
            msg = "لطفا میزان زمان مناقصه را وارد کنید(به دقیقه)";


        else if (modelView != null)
        {
            String msgBtn = "";

            if (modelView.getListButton().size() > 0)
                msgBtn = adapterViewButton.checkRequiredData();

            if (modelView.getListEditText().size() > 0)
                msg = adapterViewEditText.checkRequiredData();

            if (msg.equals("") && !msgBtn.equals(""))
                msg = msgBtn;
        }
        else
            msg = "خطا در دریافت اطلاعات لطفا دوباره تلاش کنید";

        return msg;
    }


    private void openCropActivity(Uri sourceUri, Uri destinationUri)
    {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(80);
        options.setToolbarTitle("ویراش تصویر");
        options.withMaxResultSize(512, 512);
        options.withAspectRatio(4, 3);
        options.setToolbarColor(getResources().getColor(R.color.color_primary));
        options.setStatusBarColor(getResources().getColor(R.color.color_primary));
        options.setCropFrameColor(getResources().getColor(R.color.primary));
        options.setCropGridColor(getResources().getColor(R.color.primary));
        options.setActiveControlsWidgetColor(getResources().getColor(R.color.primary));
        options.setToolbarWidgetColor(getResources().getColor(R.color.primary));
        options.setLogoColor(getResources().getColor(R.color.primary));
        UCrop uCrop = UCrop.of(sourceUri, destinationUri);
        uCrop.withOptions(options);
        uCrop.start(CreateAdActivity.this);
    }

    private void getAttributeCategoryAd(String idCategoryAd)
    {
        viewLoading.show();
        apiView.getListViewCategoryAds(this, idCategoryAd + "");
    }



    private void selectImage()
    {
        final Button gallery, camera;
        camera = menu.findViewById(R.id.btn_camara_choice_dialog);
        gallery = menu.findViewById(R.id.btn_gallery_choice_dialog);
        gallery.setOnClickListener(view -> openGallery());
        camera.setOnClickListener(view -> openCamera());
        dial.show();
    }


    private void openCamera()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
        {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                File file = ImageCropUtils.createImageFile();
                modelListImage.setLocalPath("file:" + file.getAbsolutePath());
                Uri uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
                dial.dismiss();
            }
            else
            {
                showPermissionDialog.getPermissionFromUser(ListPermission.WRITE_EXTERNAL_STORAGE, "camera");
            }
        }
        else
        {
            showPermissionDialog.getPermissionFromUser(ListPermission.CAMERA, "camera");
        }
    }


    private void openGallery()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
            pictureIntent.setType("image/*");
            pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            startActivityForResult(Intent.createChooser(pictureIntent,"Select Picture"), REQUEST_CODE_GALLERY);
            dial.dismiss();
        }
        else
        {
            showPermissionDialog.getPermissionFromUser(ListPermission.WRITE_EXTERNAL_STORAGE, "gallery");
        }
    }


    @Override
    public void onClick(View v)
    {
        if (v == etAdType)
        {
            alertDialogView = new AlertDialogView(this);
            alertDialogView.Create(this, "نوع آگهی", modelView.getListTypeAds());
            alertDialogView.show();
        }
        else if (v == ivBack)
        {
            finish();
        }
        else if (v == btnCreate)
        {
            String msg = checkValueForCreateAd();

            if (msg.equals(""))
            {
                List<ModelPreviewAd> modelPreviewAdList = new ArrayList<>();

                modelPreviewAdList.add(new ModelPreviewAd("عنوان", etTitleAd.getText().toString()));
                modelPreviewAdList.add(new ModelPreviewAd("توضیحات", etExplainAd.getText().toString()));

                if (modelView.getListTypeAds() != null)
                {
                    for (ModelViewValue modelViewValue : modelView.getListTypeAds())
                        if (modelViewValue.getId().equals(typeAdID))
                        {
                            modelPreviewAdList.add(new ModelPreviewAd("نوع آگهی", modelViewValue.getFaName()));
                            if (typeAdID.equals("10"))
                                modelPreviewAdList.add(new ModelPreviewAd("میزان زمان مناقصه(به دقیقه)", etAuctionCountdownPicker.getText().toString()));
                            break;
                        }
                }

                String jsonItem = "{";

                for (ModelView.EditText editText : modelView.getListEditText())
                {
                    if (editText.getInputType() != null && editText.getInputType().length() > 0)
                    {
                        String sItem = "\"" + editText.getName() + "\":\"" + editText.getUserInput() + "\",";
                        jsonItem += sItem;

                        ModelPreviewAd modelPreviewAd = new ModelPreviewAd();
                        modelPreviewAd.setField(editText.getFaName());
                        modelPreviewAd.setValue(editText.getUserInput());
                        modelPreviewAdList.add(modelPreviewAd);
                    }
                }

                for (ModelView.Button btn : modelView.getListButton())
                {
                    String sItem = "\"" + btn.getName() + "\":\"" + btn.getUserInput() + "\",";
                    jsonItem += sItem;

                    ModelPreviewAd modelPreviewAd = new ModelPreviewAd();
                    modelPreviewAd.setField(btn.getFaName());
                    modelPreviewAd.setValue(btn.getValue());
                    modelPreviewAdList.add(modelPreviewAd);
                }



                if (jsonItem.length() > 1)
                {
                    jsonItem = jsonItem.substring(0, jsonItem.length() - 1);
                }

                jsonItem += "}";
//                Gson gson = new Gson();
//                jsonItem = gson.toJson(jsonItem);

                if (!etAuctionCountdownPicker.getText().toString().equals(""))
                    auctionCountdown = etAuctionCountdownPicker.getText().toString();

                customDialogPreviewAd = new CustomDialogPreviewAd();
                customDialogPreviewAd.make(this, modelPreviewAdList, jsonItem);
                customDialogPreviewAd.show();

                SettingKeyboard.hideKeyboard(CreateAdActivity.this);
            }
            else
            {
                customToast = new CustomToast(this);
                customToast.showError(msg);
            }
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            try
            {
                if (requestCode == REQUEST_CODE_GALLERY || requestCode == REQUEST_CODE_CAMERA)
                {
                    if (requestCode == REQUEST_CODE_GALLERY && modelListImage != null && data != null)
                    {
                        Uri sourceUri = data.getData();
                        File file = ImageCropUtils.createImageFile();
                        modelListImage.setLocalPath("file:" + file.getAbsolutePath());
                        Uri destinationUri = Uri.fromFile(file);
                        openCropActivity(sourceUri, destinationUri);
                    }
                    else if (requestCode == REQUEST_CODE_CAMERA && modelListImage != null)
                    {
                        Uri uri = Uri.parse(modelListImage.getLocalPath());
                        openCropActivity(uri, uri);
                    }
                }

                else if (requestCode == UCrop.REQUEST_CROP && data != null) {
                    ImageCropUtils.setCroppedImage(data, modelListImage);
                    ImageCropUtils.deleteImage(modelListImage.getLocalPath().replace("file:", ""));
                    listImage.set(modelListImage.getId(), modelListImage);
                    adapterAdsCreateImage.setListCreateImage(listImage);
                }

                else if (requestCode == REQUEST_CODE_CATEGORY_AD)
                {
                    if (data.getExtras().getString(MultiSelectionActivity.MULTI_SELECTION_RESULT, null) == null)
                    {
                        customToast = new CustomToast(this);
                        customToast.showError("خطا در دریافت اطلاعات");
                        return;
                    }
                    String result = data.getExtras().getString(MultiSelectionActivity.MULTI_SELECTION_RESULT);
                    selectCategory = new Gson().fromJson(result, ModelMultiSelection.class);
                    tvTitle.setText(String.format("آگهی : %s", selectCategory.getTitle()));
                    getAttributeCategoryAd(selectCategory.getId());
                }
            }
            catch (Exception e)
            {
                Toast.makeText(this, e.getMessage() + "خطا در سیستم با پشتیبانی تماس بگیرید.", Toast.LENGTH_LONG).show();
            }

        }
    }


    @Override
    public void onBackPressed()
    {
        backCount++;
        if (backCount == 2)
        {
            finish();
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showHelp("برای برگشت به صفحه قبل دوباره دکمه بازگشت را کلیک کنید");
            new Handler().postDelayed(() -> backCount = 0, 3500);
        }
    }






    // interfaces callbacks------------------------------------------------------------------------------------------------

    @Override
    public void onResponseCreateAd(boolean response, String message)
    {
        viewLoading.close();
        customDialogPreviewAd.dismiss();

        if (response)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            finish();
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }

    }

    @Override
    public void onClickAdapterListImage(@NonNull ModelListImage modelListImage)
    {
        this.modelListImage = modelListImage;
        if (modelListImage.getModel().equals("no_select"))
        {
            selectImage();
        }
        else
        {
            CustomDialogMessage customMessage = new CustomDialogMessage();
            customMessage. makeMessage(this, this)
                    .setTitle("آیا از حذف عکس مطمن هستید ؟")
                    .show();
        }

    }



    @Override
    public void onClickCustomDialog(EnCustomMessage enCustomMessage)
    {
        if (enCustomMessage == EnCustomMessage.YES)
        {
            listImage.set(modelListImage.getId(), new ModelListImage(modelListImage.getId(), "https://kooleh.ir/images/app/ic_image.png", "no_select", null));
            adapterAdsCreateImage.setListCreateImage(listImage);
        }
    }


    @Override
    public void onResponseListView(boolean response, ModelView modelListView, String message)
    {
        this.modelView = null;
        viewLoading.close();
        nsvLayout.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);
        btnCreate.setVisibility(View.GONE);
        if (response)
        {
            nsvLayout.setVisibility(View.VISIBLE);
            btnCreate.setVisibility(View.VISIBLE);
            this.modelView = modelListView;
            adapterViewEditText.setListEditTextView(modelListView.getListEditText());
            adapterViewButton.setListEditTextView(modelListView.getListButton());
            if (modelListView.getListTypeAds().size() == 0)
            {
                txtInputAdType.setVisibility(View.GONE);
            }
            listTypeAds = modelListView.getListTypeAds();
        }
        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            containerFragmentMessageHandler.setVisibility(View.VISIBLE);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }



    @Override
    public void onResponseListViewValue(boolean response, List<ModelViewValue> modelViewValueList, String message, String model, String title)
    {
    }


    @Override
    public void onResponseAlertDialogView(ModelViewValue value, int requestCodeDialogView, String choosenSequence)
    {
        if (value != null)
        {
            etAdType.setText(value.getFaName());
            typeAdID = value.getId();

            if (value.getName().equals("auction_ad"))
            {
                txtInputAuctionCountdownPicker.setVisibility(View.VISIBLE);
            } else
            {
                etAuctionCountdownPicker.setText("");
                txtInputAuctionCountdownPicker.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onConfirmCustomPreviewAd(String jsonItem)
    {
        viewLoading.show();
        apiAds.createAds(this,
                selectCategory.getId(),
                etTitleAd.getText().toString(),
                etExplainAd.getText().toString(),
                String.valueOf(typeAdID),
                auctionCountdown,
                jsonItem,
                listImage);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        showPermissionDialog.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResultRequestPermissions(boolean isOk, String message, int CodePermission)
    {
        if (CodePermission == showPermissionDialog.REQUEST_CODE_CAMERA || CodePermission == showPermissionDialog.REQUEST_CODE_WRITE_STORAGE_CAMERA)
        {
            openCamera();
        }
        if (CodePermission == showPermissionDialog.REQUEST_CODE_WRITE_STORAGE_GALLERY)
        {
            openGallery();
        }
    }

    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        viewLoading.show();
        apiView.getListViewCategoryAds(this, idCategoryAd + "");
    }
}
package ir.kooleh.app.View.Ui.Activity.Profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;


import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.List;

import ir.kooleh.app.BuildConfig;
import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Driver.ApiDriver;
import ir.kooleh.app.Server.Api.Freight.ApiFreight;
import ir.kooleh.app.Server.Api.User.ApiProfile;
import ir.kooleh.app.Server.Api.View.ApiView;
import ir.kooleh.app.Structure.Enum.ListPermission;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceGetPermissions;
import ir.kooleh.app.Structure.Interface.Driver.InterfaceDriver;
import ir.kooleh.app.Structure.Interface.Freight.InterfaceFreight;
import ir.kooleh.app.Structure.Interface.User.InterfaceProfile;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.ListView.InterfaceView;
import ir.kooleh.app.Structure.Model.Driver.ModelDriver;
import ir.kooleh.app.Structure.Model.Freight.ModelFreight;
import ir.kooleh.app.Structure.Model.Image.ModelListImage;
import ir.kooleh.app.Structure.Model.User.Profile.ModelProfile;
import ir.kooleh.app.Structure.Model.User.Profile.ModelProfileAllTypeUser;
import ir.kooleh.app.Structure.Model.View.ModelView;
import ir.kooleh.app.Structure.Model.View.ModelViewValue;
import ir.kooleh.app.Structure.StaticValue.SVTypeUser;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogMessage;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.Image.ImageCropUtils;
import ir.kooleh.app.Utility.Class.ShowPermissionDialog;
import ir.kooleh.app.View.Layout.AlertDialogView;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class EditProfileActivity extends AppCompatActivity implements
        View.OnClickListener, InterfaceProfile.ActivityProfile,
        CompoundButton.OnCheckedChangeListener,
        InterfaceDriver.Info,
        InterfaceFreight.Info,
        InterfaceView.AlertDialog.AlertDialogView, InterfaceView.Server,
        InterfaceMessageHandler.FragmentResult,
        InterfaceGetPermissions
{

    // view
    private NestedScrollView nestedEditProfile;
    private LinearLayout lnrNoActiveYourAccount;
    private ImageView ivBackEditProfile, ivProfile;
    private TextView tvSelectImageUser, tvTitleToolbar, tvPhoneNumber;
    private Button btnSave, btnActiveNoActiveAccount;
    private CEditText etFirstName, etLastName, etUsername;
    private TextInputLayout txtInputUsername, txtInputFirstname, txtInputLastname;
    private RadioButton rbDriver, rbMasterLoad, rbFreight;
    private RadioButton rbSmartPhoneYes, rbSmartPhoneNo;
    private RadioButton rbReceivedSmsYes, rbReceivedSmsNo;
    private FragmentMessageHandler fragmentMessageHandler;
    private Toolbar toolbar;


    // driver
    private LinearLayout lnrEditProfileDriver;
    private CEditText etSmartCode, etNameFather;
    private TextInputLayout txtInputSmartCode, txtInputNameFather;
    private CEditText etSelectNameCity, etSelectNameFleet;
    private TextInputLayout txtInputSelectNameCity, txtInputSelectNameFleet;

    // freight
    private LinearLayout lnrEditProfileFreight;
    private CEditText etFreightName, etFreightRegisterNumber, etFreightManagerName, etFreightAddress, etFreightPhone;
    private TextInputLayout txtInputFreightName, txtInputFreightRegisterNumber, txtInputFreightManagerName, txtInputFreightAddress, txtInputFreightPhone;
    private CEditText etFreightSelectNameCity;
    private TextInputLayout txtInputFreightSelectNameCity;


    // var
    private CustomToast customToast;
    private Activity context;
    private ViewLoading viewLoading;
    private ApiProfile apiProfile;
    private ApiDriver apiDriver;
    private ApiFreight apiFreight;
    private ApiView apiView;
    private CustomDialogMessage customMessage;
    private ShowPermissionDialog showPermissionDialog;
    private String typeUser = SVTypeUser.Member;
    private AlertDialog dial;
    private View menu;

    private ModelListImage modelListImage;
    private ModelViewValue modelViewValueCity;
    private ModelViewValue modelViewValueFleet;

    //Request Code
    private final int REQUEST_CITY_CODE_DRIVER = 1000;
    private final int REQUEST_CITY_CODE_FREIGHT = 1500;
    private final int REQUEST_FLEET_CODE = 2000;

    private static final int REQUEST_CODE_GALLERY = 9005;
    private static final int REQUEST_CODE_CAMERA = 9006;

    private ModelProfileAllTypeUser profileUpdate;
    private AlertDialogView alertDialogView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initView();
        createVar();
        getProfile();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView()
    {

        nestedEditProfile = findViewById(R.id.nested_edit_profile);
        lnrNoActiveYourAccount = findViewById(R.id.lnr_no_active_your_account);
        toolbar = findViewById(R.id.toolbar_app);
        //image view
        ivBackEditProfile = findViewById(R.id.iv_back_toolbar);
        ivProfile = findViewById(R.id.iv_profile_user_edit_profile);
        //text view
        tvSelectImageUser = findViewById(R.id.tv_select_image_edit_profile);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        tvPhoneNumber = findViewById(R.id.tv_phone_number_edit_profile);

        //edit text
        etFirstName = findViewById(R.id.et_first_name_edit_profile);
        etLastName = findViewById(R.id.et_last_name_edit_profile);
        etUsername = findViewById(R.id.et_username_edit_profile);
        txtInputUsername = findViewById(R.id.txt_input_username_edit_profile);
        txtInputFirstname = findViewById(R.id.txt_input_firstname_edit_profile);
        txtInputLastname = findViewById(R.id.txt_input_lastname_edit_profile);
        //radio group

        //radio button
        rbDriver = findViewById(R.id.rb_driver_edit_profile);
        rbMasterLoad = findViewById(R.id.rb_master_load_edit_profile);
        rbFreight = findViewById(R.id.rb_freight_edit_profile);
        rbSmartPhoneYes = findViewById(R.id.rb_smart_phone_yes_edit_profile);
        rbSmartPhoneNo = findViewById(R.id.rb_smart_phone_no_edit_profile);
        rbReceivedSmsYes = findViewById(R.id.rb_get_news_yes_edit_profile);
        rbReceivedSmsNo = findViewById(R.id.rb_get_news_no_edit_profile);

        btnSave = findViewById(R.id.btn_save_profile_changes);
        btnActiveNoActiveAccount = findViewById(R.id.btn_back_no_active_your_account);

        //driver
        lnrEditProfileDriver = findViewById(R.id.lnr_driver_edit_profile);
        etSmartCode = findViewById(R.id.et_smart_code_edit_profile);
        etNameFather = findViewById(R.id.et_name_father_edit_profile);
        txtInputSmartCode = findViewById(R.id.txt_input_smart_code_edit_profile);
        txtInputNameFather = findViewById(R.id.txt_input_name_father_edit_profile);
        etSelectNameCity = findViewById(R.id.et_select_name_city_edit_profile);
        etSelectNameFleet = findViewById(R.id.et_select_name_fleets_edit_profile);
        txtInputSelectNameCity = findViewById(R.id.txt_input_select_name_city_edit_profile);
        txtInputSelectNameFleet = findViewById(R.id.txt_input_select_name_fleets_edit_profile);


        // freight
        lnrEditProfileFreight = findViewById(R.id.lnr_freight_edit_profile);
        etFreightName = findViewById(R.id.et_freight_name_edit_profile);
        etFreightRegisterNumber = findViewById(R.id.et_freight_register_number_edit_profile);
        etFreightManagerName = findViewById(R.id.et_freight_manager_name_edit_profile);
        etFreightAddress = findViewById(R.id.et_freight_address_edit_profile);
        etFreightPhone = findViewById(R.id.et_freight_phone_edit_profile);
        txtInputFreightName = findViewById(R.id.txt_input_freight_name_edit_profile);
        txtInputFreightRegisterNumber = findViewById(R.id.txt_input_freight_register_number_edit_profile);
        txtInputFreightManagerName = findViewById(R.id.txt_input_freight_manager_name_edit_profile);
        txtInputFreightAddress = findViewById(R.id.txt_input_freight_address_edit_profile);
        txtInputFreightPhone = findViewById(R.id.txt_input_freight_phone_edit_profile);
        etFreightSelectNameCity = findViewById(R.id.et_freight_select_name_city_edit_profile);
        txtInputFreightSelectNameCity = findViewById(R.id.txt_input_freight_select_name_city_edit_profile);

        ivProfile.setOnClickListener(this);
        ivBackEditProfile.setOnClickListener(this);
        etSelectNameFleet.setOnClickListener(this);
        etSelectNameCity.setOnClickListener(this);
        tvSelectImageUser.setOnClickListener(this);
        etFreightSelectNameCity.setOnClickListener(this);

        btnSave.setOnClickListener(this);
        btnActiveNoActiveAccount.setOnClickListener(this);

        rbDriver.setOnCheckedChangeListener(this);
        rbMasterLoad.setOnCheckedChangeListener(this);
        rbFreight.setOnCheckedChangeListener(this);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        etUsername.setOnTouchListener((v, event) -> { txtInputUsername.setErrorEnabled(false); return false; });
        etFirstName.setOnTouchListener((v, event) -> { txtInputFirstname.setErrorEnabled(false); return false; });
        etLastName.setOnTouchListener((v, event) -> { txtInputLastname.setErrorEnabled(false); return false; });

        etUsername.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputUsername.setErrorEnabled(false); }
        });

        etFirstName.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputFirstname.setErrorEnabled(false); }
        });

        etLastName.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputLastname.setErrorEnabled(false); }
        });


        //driver
        etSmartCode.setOnTouchListener((v, event) -> { txtInputSmartCode.setErrorEnabled(false); return false; });
        etNameFather.setOnTouchListener((v, event) -> { txtInputNameFather.setErrorEnabled(false); return false; });
        etSelectNameCity.setOnTouchListener((v, event) -> { txtInputSelectNameCity.setErrorEnabled(false); return false; });
        etSelectNameFleet.setOnTouchListener((v, event) -> { txtInputSelectNameFleet.setErrorEnabled(false); return false; });

        etSmartCode.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputSmartCode.setErrorEnabled(false); }
        });

        etNameFather.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputNameFather.setErrorEnabled(false); }
        });

        etSelectNameCity.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputSelectNameCity.setErrorEnabled(false); }
        });

        etSelectNameFleet.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputSelectNameFleet.setErrorEnabled(false); }
        });



        //freight
        etFreightName.setOnTouchListener((v, event) -> { txtInputFreightName.setErrorEnabled(false); return false; });
        etFreightRegisterNumber.setOnTouchListener((v, event) -> { txtInputFreightRegisterNumber.setErrorEnabled(false); return false; });
        etFreightManagerName.setOnTouchListener((v, event) -> { txtInputFreightManagerName.setErrorEnabled(false); return false; });
        etFreightAddress.setOnTouchListener((v, event) -> { txtInputFreightAddress.setErrorEnabled(false); return false; });
        etFreightPhone.setOnTouchListener((v, event) -> { txtInputFreightPhone.setErrorEnabled(false); return false; });
        etFreightSelectNameCity.setOnTouchListener((v, event) -> { txtInputFreightSelectNameCity.setErrorEnabled(false); return false; });

        etFreightName.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputFreightName.setErrorEnabled(false); }
        });

        etFreightRegisterNumber.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputFreightRegisterNumber.setErrorEnabled(false); }
        });

        etFreightManagerName.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputFreightManagerName.setErrorEnabled(false); }
        });

        etFreightAddress.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputFreightAddress.setErrorEnabled(false); }
        });

        etFreightPhone.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputFreightPhone.setErrorEnabled(false); }
        });

        etFreightSelectNameCity.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) txtInputFreightSelectNameCity.setErrorEnabled(false); }
        });
    }


    @SuppressLint("ClickableViewAccessibility")
    private void createVar()
    {
        context = this;
        apiView = new ApiView(context);
        viewLoading = new ViewLoading(context);
        apiProfile = new ApiProfile(context);
        apiDriver = new ApiDriver(context);
        apiFreight = new ApiFreight(context);
        customMessage = new CustomDialogMessage();
        tvTitleToolbar.setText("ویرایش پروفایل");
        profileUpdate = new ModelProfileAllTypeUser();
        showPermissionDialog = new ShowPermissionDialog(this, this);

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);

        menu = LayoutInflater.from(EditProfileActivity.this).inflate(R.layout.layout_custom_choice_pic, null);
        builder.setView(menu);
        dial = builder.create();
        dial.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        nestedEditProfile.setOnTouchListener((v, event) -> {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) this.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            if(inputMethodManager.isAcceptingText())
            {
                inputMethodManager.hideSoftInputFromWindow(
                        this.getCurrentFocus().getWindowToken(),
                        0
                );
            }
            return false;
        });

    }


    private void getProfile()
    {
        nestedEditProfile.setVisibility(View.GONE);
        viewLoading.show();
        apiProfile.getProfile(this);
    }

    @Override
    public void onResponseGetProfile(boolean response, ModelProfile profile, String message)
    {

        lnrEditProfileDriver.setVisibility(View.GONE);
        lnrNoActiveYourAccount.setVisibility(View.GONE);
        nestedEditProfile.setVisibility(View.GONE);
        btnSave.setVisibility(View.GONE);
        viewLoading.close();

        if (response)
        {
            nestedEditProfile.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.VISIBLE);
            if (profile.getTypeUser().equals(SVTypeUser.Member))
            {
                customMessage.makeMessage(context, "تکمیل پروفایل")
                        .setExplain(message + "")
                        .setVisibilityBtnNo(false)
                        .setVisibilityBtnYes(false)
                        .show();
            }

            setDataProfile(profile);
        }
        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }


    // set base data for profile
    private void setDataProfile(ModelProfile profile)
    {


        typeUser = profile.getTypeUser();
        profileUpdate.setBaseModelProfile(profile);


        if (profile.getProfileImage() == null)
        {
            ivProfile.setImageResource(R.drawable.ic_avatar);
            modelListImage = new ModelListImage(1, profile.getProfileImage(), "", "delete");
        }
        else
        {
            Glide.with(context).load(profile.getProfileImage()).
                    placeholder(R.drawable.ic_avatar).
                    into(ivProfile);
            modelListImage = new ModelListImage(1, profile.getProfileImage(), "", profile.getProfileImage());
        }

        tvPhoneNumber.setText(String.format("%s", profile.getPhoneNumber()));

        etFirstName.setText(String.format("%s", profile.getFirstName()));
        etLastName.setText(String.format("%s", profile.getLastName()));


        etUsername.setText(profile.getUsername());


        if (profile.getNotification().equals("1"))
            rbReceivedSmsYes.setChecked(true);
        else
            rbReceivedSmsNo.setChecked(true);


        if (profile.getSmartPhone().equals("1"))
            rbSmartPhoneYes.setChecked(true);
        else
            rbSmartPhoneNo.setChecked(true);


        if (profile.getTypeUser().equals(SVTypeUser.DRIVER))
        {
            rbDriver.setChecked(true);
            viewLoading.show();
            apiDriver.getDriver(this);
        }
        else if (profile.getTypeUser().equals(SVTypeUser.FREIGHT))
        {
            rbFreight.setChecked(true);
            viewLoading.show();
            apiFreight.getFreight(this);
        }
        else if (profile.getTypeUser().equals(SVTypeUser.MASTER_LOAD))
            rbMasterLoad.setChecked(true);



    }


    @Override
    public void onResponseGetDriver(boolean response, ModelDriver driver, String message)
    {
        lnrEditProfileDriver.setVisibility(View.GONE);
        lnrNoActiveYourAccount.setVisibility(View.GONE);
        nestedEditProfile.setVisibility(View.GONE);
        btnSave.setVisibility(View.GONE);
        viewLoading.close();
        if (response)
        {
            if (driver.getStatus().equals(0))
            {
                lnrNoActiveYourAccount.setVisibility(View.VISIBLE);
            }

            else
            {
                btnSave.setVisibility(View.VISIBLE);
                nestedEditProfile.setVisibility(View.VISIBLE);
                setDataDriver(driver);
                profileUpdate.setModelDriver(driver);
            }

        }
        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }


    }


    @Override
    public void onResponseGetFreight(boolean response, ModelFreight freight, String message)
    {
        lnrEditProfileDriver.setVisibility(View.GONE);
        lnrNoActiveYourAccount.setVisibility(View.GONE);
        nestedEditProfile.setVisibility(View.GONE);
        btnSave.setVisibility(View.GONE);
        viewLoading.close();
        if (response)
        {
            if (freight.getStatus().equals(0))
            {
                lnrNoActiveYourAccount.setVisibility(View.VISIBLE);
            }
            else
            {
                btnSave.setVisibility(View.VISIBLE);
                nestedEditProfile.setVisibility(View.VISIBLE);
                setDataFreight(freight);
                profileUpdate.setModelFreight(freight);
            }
        }
        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }


    private void setDataDriver(ModelDriver driver)
    {
        rbDriver.setChecked(true);
        lnrEditProfileDriver.setVisibility(View.VISIBLE);
        etNameFather.setText(String.format("%s", driver.getFatherName()));
        etSmartCode.setText(String.format("%s", driver.getSmartCode()));

        etSelectNameCity.setText(driver.getCityName());

        etSelectNameFleet.setText(driver.getFleetName());
    }


    private void setDataFreight(ModelFreight freight)
    {
        rbFreight.setChecked(true);
        lnrEditProfileFreight.setVisibility(View.VISIBLE);
        etFreightName.setText(String.format("%s", freight.getName()));
        etFreightRegisterNumber.setText(String.format("%s", freight.getRegisterNumber()));
        etFreightManagerName.setText(String.format("%s", freight.getManagerName()));
        etFreightAddress.setText(String.format("%s", freight.getAddress()));
        etFreightPhone.setText(String.format("%s", freight.getPhone()));

        etFreightSelectNameCity.setText(freight.getCityName());
    }


    //ذخیره پروفایل
    private void SaveProfileInfo()
    {

        String message = checkBaseInfo();

        if (message.equals("error"))
        {
            return;
        }

        if (profileUpdate.getBaseModelProfile() == null)
        {
            customToast = new CustomToast(this);
            customToast.showError("خطا اطلاعات ابتدایی شما پیدا نشد لطفا دوباره تلاش کنید");
            return;
        }


        if (typeUser.equals(SVTypeUser.DRIVER))
        {
            message = checkDriverInfo();
            if (!message.equals("error"))
            {
                if (profileUpdate.getModelDriver() == null)
                {
                    customToast = new CustomToast(this);
                    customToast.showError("خطا اطلاعات راهنما به صورت صحیح پر نشده است !!!");
                }

                else
                {
                    viewLoading.show();
                    apiProfile.updateProfile(this, profileUpdate);
                }
            }
        }
        else if (typeUser.equals(SVTypeUser.FREIGHT))
        {
            message = checkFreightInfo();
            if (!message.equals("error"))
            {
                if (profileUpdate.getModelFreight() == null)
                {
                    customToast = new CustomToast(this);
                    customToast.showError("خطا اطلاعات آژانس به صورت صحیح پر نشده است !!!");
                }

                else
                {
                    viewLoading.show();
                    apiProfile.updateProfile(this, profileUpdate);
                }
            }
        }

        else if (typeUser.equals(SVTypeUser.MASTER_LOAD))
        {
            viewLoading.show();
            apiProfile.updateProfile(this, profileUpdate);
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError("خطا این ساختار کاربر پیدا نشد");
        }

    }

    private String checkBaseInfo()
    {

        ModelProfile baseProfile = profileUpdate.getBaseModelProfile();

        if (etUsername.getText().toString().length() < 3)
        {
            txtInputUsername.setError("نام کاربری حداقل شامل سه کاراکتر باید باشد");
            etUsername.requestFocus();
            return "error";
        }
        else
        {
            baseProfile.setUsername(etUsername.getText().toString());
        }

        if (etFirstName.getText().toString().length() == 0)
        {
            txtInputFirstname.setError("لطفا نام خود را وارد کنید");
            etFirstName.requestFocus();
            return "error";
        }
        else
        {
            baseProfile.setFirstName(etFirstName.getText().toString());
        }

        if (etLastName.getText().toString().length() == 0)
        {
            txtInputLastname.setError("لطفا نام خانوادگی خود را وارد کنید");
            etLastName.requestFocus();
            return "error";
        }
        else
        {
            baseProfile.setLastName(etLastName.getText().toString());
        }

        if (!rbDriver.isChecked() && !rbMasterLoad.isChecked() && !rbFreight.isChecked())
            return "لطفا نوع کاربری خود را انتخاب کنید ( گردشگر - راهنما - آژانس )";
        else
            baseProfile.setTypeUser(SVTypeUser.DRIVER);


        if (rbSmartPhoneYes.isChecked())
            baseProfile.setSmartPhone("1");

        else if (rbSmartPhoneNo.isChecked())
            baseProfile.setSmartPhone("0");


        if (rbReceivedSmsYes.isChecked())
            baseProfile.setNotification("1");

        else
            baseProfile.setNotification("0");


        baseProfile.setTypeUser(typeUser);


        if (modelListImage.getBase64() != null && modelListImage.getBase64().length() > 0)
            baseProfile.setProfileImage(modelListImage.getBase64());
        else
            baseProfile.setProfileImage("");

        profileUpdate.setBaseModelProfile(baseProfile);
        return "";

    }


    private String checkDriverInfo()
    {

        ModelDriver driver = profileUpdate.getModelDriver();

        if (etSmartCode.getText().toString().length() == 0)
        {
            txtInputSmartCode.setError("لطفا کد هوشمند خود را وارد کنید");
            etSmartCode.requestFocus();
            return "error";
        }
        else
        {
            driver.setSmartCode(etSmartCode.getText().toString());
        }

        if (etNameFather.getText().toString().length() == 0)
        {
            txtInputNameFather.setError("لطفا نام پدر خود را وارد کنید");
            etNameFather.requestFocus();
            return "error";
        }
        else
        {
            driver.setFatherName(etNameFather.getText().toString());
        }

        String idCity;
        if (modelViewValueCity != null)
            idCity = modelViewValueCity.getId();

        else
            idCity = driver.getCityId();

        String idFleet;
        if (modelViewValueFleet != null)
            idFleet = modelViewValueFleet.getId();
        else
            idFleet = driver.getFleetId();


        if (Integer.parseInt(idCity) <= 0)
        {
            txtInputSelectNameCity.setError("لطفا شهر خود را انتخاب کنید");
            etSelectNameCity.requestFocus();
            return "error";
        }
        else
        {
            driver.setCityId(idCity);
        }

        if (Integer.parseInt(idFleet) <= 0)
        {
            txtInputSelectNameFleet.setError("لطفا نوع  وسیله نقلیه خود را انتخاب کنید");
            etSelectNameFleet.requestFocus();
            return "error";
        }
        else
        {
            driver.setFleetId(idFleet);
        }


        profileUpdate.setModelDriver(driver);

        return "";
    }


    private String checkFreightInfo()
    {
        ModelFreight freight = profileUpdate.getModelFreight();

        if (etFreightName.getText().toString().length() == 0)
        {
            txtInputFreightName.setError("لطفا نام آژانس را وارد کنید");
            etFreightName.requestFocus();
            return "error";
        }
        else
        {
            freight.setName(etFreightName.getText().toString().trim());
        }

        if (etFreightRegisterNumber.getText().toString().length() == 0)
        {
            txtInputFreightRegisterNumber.setError("لطفا شماره ثبت آژانس را وارد کنید");
            etFreightRegisterNumber.requestFocus();
            return "error";
        }
        else
        {
            freight.setRegisterNumber(etFreightRegisterNumber.getText().toString().trim());
        }

        if (etFreightManagerName.getText().toString().length() == 0)
        {
            txtInputFreightManagerName.setError("لطفا نام متصدی آژانس را وارد کنید");
            etFreightManagerName.requestFocus();
            return "error";
        }
        else
        {
            freight.setManagerName(etFreightManagerName.getText().toString().trim());
        }

        if (etFreightAddress.getText().toString().length() == 0)
        {
            txtInputFreightAddress.setError("لطفا آدرس آژانس را وارد کنید");
            etFreightAddress.requestFocus();
            return "error";
        }
        else
        {
            freight.setAddress(etFreightAddress.getText().toString().trim());
        }

        if (etFreightPhone.getText().toString().length() < 11)
        {
            txtInputFreightPhone.setError("لطفا شماره تماس ثابت آژانس را به صورت صحیح وارد کنید(۱۱ رقم)");
            etFreightPhone.requestFocus();
            return "error";
        }
        else
        {
            freight.setPhone(etFreightPhone.getText().toString().trim());
        }

        String idCity;
        if (modelViewValueCity != null)
            idCity = modelViewValueCity.getId();
        else
            idCity = freight.getCityID();


        if (Integer.parseInt(idCity) <= 0)
        {
            txtInputFreightSelectNameCity.setError("لطفا شهر خود را انتخاب کنید");
            etFreightSelectNameCity.requestFocus();
            return "error";
        }
        else
        {
            freight.setCityID(idCity);
        }

        profileUpdate.setModelFreight(freight);

        return "";
    }


    @Override
    public void onResponseUpdateProfile(boolean response, String message)
    {
        viewLoading.close();
        if (response)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            setResult(HomeActivity.RESULT_CODE_USER_TYPE_CHANGE);
            finish();
        }

        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }


    // method of Alert dialog for chose camera or gallery
    private void changeImage()
    {
        final Button gallery, camera, remove;
        camera = menu.findViewById(R.id.btn_camara_choice_dialog);
        gallery = menu.findViewById(R.id.btn_gallery_choice_dialog);
        remove = menu.findViewById(R.id.btn_remove_choice_dialog);
        gallery.setOnClickListener(view -> openGallery());
        camera.setOnClickListener(view -> openCamera());

        if (modelListImage.getBase64().equals("delete"))
        {
            remove.setVisibility(View.GONE);
        }
        else
        {
            remove.setVisibility(View.VISIBLE);
        }

        remove.setOnClickListener(v ->
        {
            ivProfile.setImageResource(R.drawable.ic_avatar);
            modelListImage.setBase64("delete");
            profileUpdate.getBaseModelProfile().setProfileImage("");
            dial.dismiss();
        });
        dial.show();
    }


    private void openCropActivity(Uri sourceUri, Uri destinationUri)
    {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(80);
        options.setToolbarTitle("ویراش تصویر");
        options.withMaxResultSize(512, 512);
        options.withAspectRatio(4, 4);
        options.setToolbarColor(getResources().getColor(R.color.color_primary));
        options.setStatusBarColor(getResources().getColor(R.color.color_primary));
        options.setCropFrameColor(getResources().getColor(R.color.primary));
        options.setCropGridColor(getResources().getColor(R.color.primary));
        options.setActiveControlsWidgetColor(getResources().getColor(R.color.primary));
        options.setToolbarWidgetColor(getResources().getColor(R.color.primary));
        UCrop uCrop = UCrop.of(sourceUri, destinationUri);
        uCrop.withOptions(options);
        uCrop.start(this); 

    }


    private void openCamera()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
        {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file = ImageCropUtils.createImageFile();
                modelListImage.setLocalPath("file:" + file.getAbsolutePath());
                Uri uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
                dial.dismiss();
            }
            else
            {
                showPermissionDialog.getPermissionFromUser(ListPermission.WRITE_EXTERNAL_STORAGE, "camera");
            }
        }
        else
        {
            showPermissionDialog.getPermissionFromUser(ListPermission.CAMERA, "camera");
        }
    }


    private void openGallery()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
            pictureIntent.setType("image/*");
            pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            startActivityForResult(Intent.createChooser(pictureIntent,"Select Picture"), REQUEST_CODE_GALLERY);
            dial.dismiss();
        }
        else
        {
            showPermissionDialog.getPermissionFromUser(ListPermission.WRITE_EXTERNAL_STORAGE, "gallery");
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        if (isChecked)
        {
            if (buttonView == rbDriver)
            {
                typeUser = SVTypeUser.DRIVER;
                lnrEditProfileDriver.setVisibility(View.VISIBLE);
                lnrEditProfileFreight.setVisibility(View.GONE);
                viewLoading.show();
                apiDriver.getDriver(this);
            }
            else if (buttonView == rbFreight)
            {
                typeUser = SVTypeUser.FREIGHT;
                lnrEditProfileFreight.setVisibility(View.VISIBLE);
                lnrEditProfileDriver.setVisibility(View.GONE);
                viewLoading.show();
                apiFreight.getFreight(this);
            }
            else if (buttonView == rbMasterLoad)
            {
                lnrEditProfileDriver.setVisibility(View.GONE);
                lnrEditProfileFreight.setVisibility(View.GONE);
                typeUser = SVTypeUser.MASTER_LOAD;
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            try
            {
                if (requestCode == REQUEST_CODE_GALLERY || requestCode == REQUEST_CODE_CAMERA)
                {
                    if (requestCode == REQUEST_CODE_GALLERY && modelListImage != null && data != null)
                    {
                        Uri sourceUri = data.getData();
                        File file = ImageCropUtils.createImageFile();
                        modelListImage.setLocalPath("file:" + file.getAbsolutePath());
                        modelListImage.setModel("gallery");
                        Uri destinationUri = Uri.fromFile(file);
                        openCropActivity(sourceUri, destinationUri);
                    }
                    else if (requestCode == REQUEST_CODE_CAMERA && modelListImage != null)
                    {
                        Uri uri = Uri.parse(modelListImage.getLocalPath());
                        openCropActivity(uri, uri);
                    }
                }

                else if (requestCode == UCrop.REQUEST_CROP && data != null) {
                    ImageCropUtils.setCroppedImage(data, modelListImage);
                    ImageCropUtils.deleteImage(modelListImage.getLocalPath().replace("file:", ""));
                    ivProfile.setImageBitmap(modelListImage.getBitmap());
                }

            }
            catch (Exception e)
            {
                Toast.makeText(context, e.getMessage() + "خطا در سیستم با پشتیبانی تماس بگیرید.", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onClick(View view)
    {
        if (view == ivBackEditProfile)
        {
            finish();
        }
        else if (view == tvSelectImageUser || view == ivProfile)
        {
            changeImage();
        }
        else if (view == etSelectNameCity)
        {
            getViewValueList("city");
        }
        else if (view == etSelectNameFleet)
        {
            getViewValueList("fleet");
        }
        else if (view == etFreightSelectNameCity)
        {
            getViewValueList("city");
        }
        else if (view == btnSave)
        {
            SaveProfileInfo();
        }
        else if (view == btnActiveNoActiveAccount)
        {
            finish();
        }

    }


    private void getViewValueList(String model)
    {
        viewLoading.show();
        apiView.getViewValueList(this, model);
    }



    @Override
    public void onResponseAlertDialogView(ModelViewValue modelViewValue, int requestCode, String choosenSequence)
    {
        if (modelViewValue != null)
        {
            if (requestCode == REQUEST_CITY_CODE_DRIVER)
            {
                this.modelViewValueCity = modelViewValue;
                etSelectNameCity.setText(String.format("%s", modelViewValue.getFaName()));
            }
            else if (requestCode == REQUEST_CITY_CODE_FREIGHT)
            {
                this.modelViewValueCity = modelViewValue;
                etFreightSelectNameCity.setText(String.format("%s", modelViewValue.getFaName()));
            }
            else if (requestCode == REQUEST_FLEET_CODE)
            {
                this.modelViewValueFleet = modelViewValue;
                etSelectNameFleet.setText(String.format("%s", modelViewValue.getFaName()));
            }
        }
    }


    @Override
    public void onResponseListViewValue(boolean response, List<ModelViewValue> modelViewValueList, String message, String model, String title)
    {
        viewLoading.close();
        if (response)
        {
            alertDialogView = new AlertDialogView(this);
            alertDialogView.Create(this, title, modelViewValueList);

            if (model.equals("city"))
            {
                if (rbDriver.isChecked())
                {
                    alertDialogView.show(REQUEST_CITY_CODE_DRIVER);
                }
                else
                {
                    alertDialogView.show(REQUEST_CITY_CODE_FREIGHT);
                }
            }
            else if (model.equals("fleet"))
            {
                alertDialogView.show(REQUEST_FLEET_CODE);
            }
        }
    }






    @Override
    public void onResponseListView(boolean response, ModelView modelListView, String message)
    {

    }

    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        getProfile();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        showPermissionDialog.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResultRequestPermissions(boolean isOk, String message, int CodePermission)
    {
        if (CodePermission == showPermissionDialog.REQUEST_CODE_CAMERA || CodePermission == showPermissionDialog.REQUEST_CODE_WRITE_STORAGE_CAMERA)
        {
            openCamera();
        }
        if (CodePermission == showPermissionDialog.REQUEST_CODE_WRITE_STORAGE_GALLERY)
        {
            openGallery();
        }
    }
}


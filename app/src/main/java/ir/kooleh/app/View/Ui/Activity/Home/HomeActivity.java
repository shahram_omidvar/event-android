package ir.kooleh.app.View.Ui.Activity.Home;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;


import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.App.ApiApp;
import ir.kooleh.app.Structure.Enum.EnCustomMessage;
import ir.kooleh.app.Utility.Class.OptionDictsAndUtils;
import ir.kooleh.app.Structure.Interface.App.InterfaceApp;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomMessage;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.App.ModelApp;
import ir.kooleh.app.View.Ui.Activity.Search.SearchAdsActivity;
import ir.kooleh.app.View.Ui.Activity.general.WebViewActivity;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogMessage;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.View.Ui.Activity.Auth.CheckExistsUserActivity;

import ir.kooleh.app.View.Ui.Activity.Ads.MultiSelectionActivity;
import ir.kooleh.app.View.Ui.Activity.Wallet.PackageActivity;
import ir.kooleh.app.View.Ui.Fragment.FragmentAdsKey;
import ir.kooleh.app.View.Ui.Fragment.FragmentDashboard;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.Ui.Fragment.FragmentOffer;
import ir.kooleh.app.View.Ui.Fragment.FragmentWebView;
import ir.kooleh.app.View.Ui.Fragment.FragmentLoad;
import ir.kooleh.app.View.Ui.Fragment.FragmentServices;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;


public class HomeActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        NavigationView.OnNavigationItemSelectedListener,
        InterfaceCustomMessage, View.OnClickListener, InterfaceApp.ServerApp,
        InterfaceMessageHandler.FragmentResult,
        InterfaceMessageHandler.HomeFragmentsResponse
{

    public static NavigationView navHome;
    private DrawerLayout drawLayout;
    private BottomNavigationView bottomNavHome;
    private CoordinatorLayout coordinatorLayoutHome;
    private RelativeLayout actionBarHome;
    private FrameLayout containerHomeFragment;


    private ImageView ivDrawer, ivSearch;
    private ImageView ivProfileNav;
    private CTextView tvNameDrawer;

    private FrameLayout messageHandlerFragmentContainer;
    private FragmentMessageHandler fragmentMessageHandler;
    private Fragment selectFragment;

    //var
    private ViewLoading viewLoading;
    private UserShared userShared;
    private CustomToast customToast;
    private OptionDictsAndUtils optionDictsAndUtils;
    private ApiApp apiApp;
    private int backCount = 0;

    public static ModelApp appValue;
    private int PreviousselectedItemID = 0;
    private Menu menu;
    public static final int RESULT_CODE_USER_TYPE_CHANGE = 1324;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        createVar();
        initView();
        getInfoApp();
        setDataView();
    }

    private void initView()
    {
        bottomNavHome = findViewById(R.id.bottom_nav_home);
        coordinatorLayoutHome = findViewById(R.id.coordinator_home);
        navHome = findViewById(R.id.nav_view_home);
        drawLayout = findViewById(R.id.drawer_layout);
        ivDrawer = findViewById(R.id.iv_drawer);
        ivSearch = findViewById(R.id.iv_search_nav_top);
        messageHandlerFragmentContainer = findViewById(R.id.container_message_handler_fragment);
        actionBarHome = findViewById(R.id.action_bar_home);
        containerHomeFragment = findViewById(R.id.fragment_home);

        bottomNavHome.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
        bottomNavHome.setItemTextColor(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));

        ivDrawer.setOnClickListener(this);
        ivSearch.setOnClickListener(this);

        bottomNavHome.setOnNavigationItemSelectedListener(this);
        navHome.setNavigationItemSelectedListener(this);
    }

    private void createVar()
    {
        viewLoading = new ViewLoading(this);
        apiApp = new ApiApp(this);
        userShared = new UserShared(this);

        optionDictsAndUtils = new OptionDictsAndUtils();
    }

    private void setDataView()
    {
        NavigationView navigationView = findViewById(R.id.nav_view_home);
        View header = navigationView.getHeaderView(0);
        tvNameDrawer = header.findViewById(R.id.tv_full_name_header_nav);
        ivProfileNav = header.findViewById(R.id.iv_profile_nav_dr);
        tvNameDrawer.setText(String.format("%s", userShared.getFullName()));
        tvNameDrawer.setSelected(true);
    }


    private void getInfoApp()
    {
        viewLoading.show();
        apiApp.getInfoApp(this);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        boolean navBottom = false;
        selectFragment = null;
        fragmentMessageHandler = null;
        Integer itemId = item.getItemId();
        TypedValue tv = new TypedValue();
        Bundle bundle = new Bundle();

        if (itemId.equals(optionDictsAndUtils.ids.get("fragment_load")))
        {
            selectFragment = new FragmentLoad(appValue.getKey_one(), appValue.getKey_two(), appValue.getTitle_one(), appValue.getTitle_two());
            PreviousselectedItemID = itemId;
            bottomNavHome.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            bottomNavHome.setItemTextColor(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            navBottom = true;
        }
        else if (itemId.equals(optionDictsAndUtils.ids.get("fragment_service")))
        {
            selectFragment = new FragmentServices();
            PreviousselectedItemID = itemId;
            bottomNavHome.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            bottomNavHome.setItemTextColor(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            navBottom = true;
        }
        else if (itemId.equals(optionDictsAndUtils.ids.get("fragment_crate_ad")))
        {
            Intent intent = new Intent(this, MultiSelectionActivity.class);
            intent.putExtra(MultiSelectionActivity.KEY_ACTIVITY_NAME, MultiSelectionActivity.HOME_ACTIVITY_NAME);
            bottomNavHome.getMenu().getItem(PreviousselectedItemID).setChecked(true);
            bottomNavHome.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            bottomNavHome.setItemTextColor(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            startActivity(intent);
        }
        else if (itemId.equals(optionDictsAndUtils.ids.get("fragment_web")))
        {
            selectFragment = new FragmentWebView();
            PreviousselectedItemID = itemId;
            bundle.putString("value", optionDictsAndUtils.values.get("fragment_web"));
            bottomNavHome.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            bottomNavHome.setItemTextColor(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            selectFragment.setArguments(bundle);
            navBottom = true;
        }
        else if (itemId.equals(optionDictsAndUtils.ids.get("fragment_key")))
        {
            selectFragment = new FragmentOffer();
            bundle.putString("value", optionDictsAndUtils.values.get("fragment_key"));
            PreviousselectedItemID = itemId;
            selectFragment.setArguments(bundle);
            bottomNavHome.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            bottomNavHome.setItemTextColor(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            navBottom = true;
        }
        else if (itemId.equals(optionDictsAndUtils.ids.get("fragment_dashboard")))
        {
            selectFragment = new FragmentDashboard();
            PreviousselectedItemID = itemId;
            bottomNavHome.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            bottomNavHome.setItemTextColor(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            navBottom = true;
        }


        //--------------------------------> nav dr<--------------------------------------------

        if (itemId == R.id.nav_dr_home_packages)
        {
            startActivity(new Intent(HomeActivity.this, PackageActivity.class));
            navBottom = false;
        }
        else if (itemId == R.id.nav_dr_home_support)
        {
            if (appValue != null)
            {
                startActivities(
                        new Intent[]
                                {new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + appValue.getInfoUser().getSupportNumber()))});
            }
            else
            {
                customToast = new CustomToast(this);
                customToast.showError("خطا در دریافت اطلاعات");
            }
        }
        else if (itemId == R.id.nav_dr_hone_invite)
        {
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            if (appValue != null)
            {
                String shareBody = appValue.getInfoUser().getInviteMessage();
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, getResources().getString(R.string.app_name)));
            }
            else
            {
                customToast = new CustomToast(this);
                customToast.showError("خطا در دریافت اطلاعات");
            }
            navBottom = false;
        }
        else if (itemId == R.id.nav_dr_home_about_us)
        {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("value", "https://kooleh.ir");
            intent.putExtra(WebViewActivity.KEY_NO_TOOLBAR, true);
            intent.putExtra(WebViewActivity.KEY_TITLE_TOOLBAR, "درباره ما");
            startActivity(intent);
        }
        else if (itemId == R.id.nav_dr_home_rules_and_conditions)
        {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("value", "https://kooleh.ir");
            intent.putExtra(WebViewActivity.KEY_TITLE_TOOLBAR, "قوانین و شرایط راهنما");
            intent.putExtra(WebViewActivity.KEY_NO_TOOLBAR, true);
            startActivity(intent);
        }

        else if (itemId == R.id.nav_dr_home_exit)
        {
            CustomDialogMessage customMessage = new CustomDialogMessage();
            customMessage.makeMessage(this, this)
                    .setTitle("خروج از حساب کاربری")
                    .setCancelable(false)
                    .setExplain("آیا از خروج از حساب کاربری مطمن هستید؟")
                    .show();
        }


        if (navBottom && selectFragment != null)
        {
            loadFragment(selectFragment);
            return true;
        }

        drawLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    private void logout()
    {
        finish();
        startActivity(new Intent(this, CheckExistsUserActivity.class));
    }


    private void loadFragment(Fragment fragment)
    {
        if (fragment == null)
            fragment = new FragmentServices();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_home, fragment)
                .commitNowAllowingStateLoss();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture)
    {

    }


    @Override
    public void onClickCustomDialog(EnCustomMessage enCustomMessage)
    {
        if (enCustomMessage == EnCustomMessage.YES)
        {
            userShared.deletePreferences("UsernameInfo");
            logout();
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivDrawer)
        {
            if (!drawLayout.isDrawerOpen(GravityCompat.START))
                drawLayout.openDrawer(GravityCompat.START);
            else
                drawLayout.closeDrawer(GravityCompat.START);
        }

        if (v == ivSearch)
        {
            startActivity(new Intent(HomeActivity.this, SearchAdsActivity.class));
        }

    }


    @Override
    public void onResponseInfoApp(boolean response, ModelApp modelApp, String message)
    {
        viewLoading.close();

        appValue = modelApp;

        messageHandlerFragmentContainer.setVisibility(View.GONE);
        coordinatorLayoutHome.setVisibility(View.GONE);


        if (response)
        {
            coordinatorLayoutHome.setVisibility(View.VISIBLE);
            actionBarHome.setVisibility(View.VISIBLE);
            optionDictsAndUtils.setOption(modelApp.getMenu());
            if (modelApp.getInfoUser().getImage() != null)
                Glide.with(this).load(modelApp.getInfoUser().getImage()).placeholder(R.drawable.ic_avatar).into(ivProfileNav);
            tvNameDrawer.setText(String.format("%s %s", modelApp.getInfoUser().getFirstName(), modelApp.getInfoUser().getLastName()));
            userShared.setFullName(String.format("%s %s", modelApp.getInfoUser().getFirstName(), modelApp.getInfoUser().getLastName()));

            selectFragment = optionDictsAndUtils.getFragmentByName(appValue.getMenu().get(0).getName());
            loadFragment(selectFragment);

            menu = bottomNavHome.getMenu();

            for (ModelApp.Menu option : modelApp.getMenu())
            {
                int icon = optionDictsAndUtils.icons.get(option.getName()) != null ? optionDictsAndUtils.icons.get(option.getName()) : 1;
                int id = optionDictsAndUtils.ids.get(option.getName()) != null ? optionDictsAndUtils.ids.get(option.getName()) : 1;
                menu.add(Menu.NONE, id, Menu.NONE, option.getFaName()).setIcon(icon);
            }

            int icon = optionDictsAndUtils.icons.get("fragment_dashboard") != null ? optionDictsAndUtils.icons.get("fragment_dashboard") : 1;
            int id = optionDictsAndUtils.ids.get("fragment_dashboard") != null ? optionDictsAndUtils.ids.get("fragment_dashboard") : 1;
            menu.add(Menu.NONE, id, Menu.NONE, "کوله من").setIcon(icon);

        }

        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            messageHandlerFragmentContainer.setVisibility(View.VISIBLE);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    messageHandlerFragmentContainer.getId(), fragmentMessageHandler).commitAllowingStateLoss();
        }
    }


    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        getInfoApp();
    }

    @Override
    public void onResponseHomeFragments(Bundle args)
    {
        fragmentMessageHandler = new FragmentMessageHandler(this, this);
        fragmentMessageHandler.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_home, fragmentMessageHandler).commitNowAllowingStateLoss();
    }

    @Override
    public void onResponseMessageHandler(String fragmentType)
    {
        Fragment fragment = optionDictsAndUtils.getFragmentByName(fragmentType);
        Bundle bundle = new Bundle();
        bundle.putString("value", optionDictsAndUtils.getFragmentValueByName(fragmentType));
        fragment.setArguments(bundle);
        loadFragment(fragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CODE_USER_TYPE_CHANGE)
        {
            finish();
            startActivity(getIntent());
        }
    }


    @Override
    protected void onStop()
    {
        navHome = null;
        super.onStop();
    }

    @Override
    public void onBackPressed()
    {
        if (selectFragment != null && !(selectFragment instanceof FragmentServices))
        {
            selectFragment = new FragmentServices();
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
            {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) containerHomeFragment.getLayoutParams();
            PreviousselectedItemID = optionDictsAndUtils.ids.get("fragment_service");
            actionBarHome.setVisibility(View.VISIBLE);
            layoutParams.topMargin = actionBarHeight;
            bottomNavHome.getMenu().getItem(PreviousselectedItemID).setChecked(true);
            bottomNavHome.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            bottomNavHome.setItemTextColor(AppCompatResources.getColorStateList(this, R.color.bottom_nav_select_color));
            loadFragment(selectFragment);
        }
        else
        {
            backCount++;
            if (backCount == 2)
            {
                finish();
            }
            else
            {
                customToast = new CustomToast(this);
                customToast.showHelp("برای خروج از برنامه دوباره دکمه بازگشت را کلیک کنید");
                new Handler().postDelayed(() -> backCount = 0, 3500);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (selectFragment instanceof FragmentWebView)
        {
            if (((FragmentWebView) selectFragment).onKeyDown(keyCode, event))
            {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}
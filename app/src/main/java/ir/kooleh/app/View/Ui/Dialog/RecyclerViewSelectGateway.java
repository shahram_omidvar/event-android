package ir.kooleh.app.View.Ui.Dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomDialogPaymentMethod;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceRecyclerViewSelectGateway;
import ir.kooleh.app.Structure.Model.Wallet.ModelGateway;
import ir.kooleh.app.Utility.Adapter.Wallet.AdapterGateways;

public class RecyclerViewSelectGateway implements
        View.OnClickListener,
        InterfaceRecyclerViewSelectGateway
{
    public AlertDialog dialog;
    private ImageView ivCloseDialog;
    private RecyclerView rvGateways;
    private AdapterGateways adapterGateways;
    private List<ModelGateway> gatewayList;

    private InterfaceCustomDialogPaymentMethod interfaceCustomDialogPaymentMethod;

    public RecyclerViewSelectGateway(InterfaceCustomDialogPaymentMethod interfaceCustomDialogPaymentMethod, List<ModelGateway> gatewayList)
    {
        this.gatewayList = gatewayList;
        this.interfaceCustomDialogPaymentMethod = interfaceCustomDialogPaymentMethod;
    }

//    private int packageID;

    public void make(Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.custom_dialog_select_gateway, null);
        builder.setView(inflater);
        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setDimAmount(0.8f);

        initView(inflater);

        dialog.setOnKeyListener((dialog, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
            {
                dismiss();
                return true;
            }
            return false;
        });

        dialog.setCanceledOnTouchOutside(false);
    }



    private void initView(View view)
    {
        ivCloseDialog = view.findViewById(R.id.iv_close_dialog_payment_method);
        rvGateways = view.findViewById(R.id.rv_gateways_select_gateway_dialog);
        ivCloseDialog.setOnClickListener(this);
        adapterGateways = new AdapterGateways(this, dialog.getContext(), gatewayList);

        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(dialog.getContext());
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setJustifyContent(JustifyContent.CENTER);
        rvGateways.setLayoutManager(flexboxLayoutManager);
        rvGateways.setAdapter(adapterGateways);
    }



    public void show()
    {
        if (dialog != null)
        {
            dismiss();
            dialog.show();
        }
    }


    public void dismiss()
    {
        if (dialog.isShowing())
        {
            dialog.dismiss();
            interfaceCustomDialogPaymentMethod = null;
            ivCloseDialog = null;
            dialog = null;
            rvGateways = null;
            adapterGateways = null;
        }
    }

    @Override
    public void onClick(View view)
    {
        if (view == ivCloseDialog)
        {
            dismiss();
        }
    }

    @Override
    public void onResponseSelectGateway(String gatewayName)
    {
        interfaceCustomDialogPaymentMethod.onResponsePaymentMethod(gatewayName);
        dismiss();
//        String uri;
//        if (type.equals(EnumWalletModule.BuyModels.MODEL_WALLET))
//        {
//            uri = Uri.parse("https://application.kooleh.ir/payment/Gateway")
//                    .buildUpon()
//                    .appendQueryParameter("gateway_name", gatewayName)
//                    .appendQueryParameter("model", type)
//                    .appendQueryParameter("price", String.valueOf(price))
//                    .appendQueryParameter("id", String.valueOf(adOrPackageID))
//                    .build().toString();
//        }
//        else
//        {
//            uri = Uri.parse("https://application.kooleh.ir/payment/Gateway")
//                    .buildUpon()
//                    .appendQueryParameter("gateway_name", gatewayName)
//                    .appendQueryParameter("model", type)
//                    .appendQueryParameter("id", String.valueOf(adOrPackageID))
//                    .build().toString();
//        }
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//        Bundle bundle = new Bundle();
//        bundle.putString("Accept", "application/json");
//        bundle.putString("Authorization", "Bearer " + userShared.getApiToken());
//        browserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
//        dialog.getContext().startActivity(browserIntent);
    }
}

package ir.kooleh.app.View.Ui.Fragment;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;


import java.util.Locale;

import im.delight.android.webview.AdvancedWebView;
import ir.kooleh.app.R;
import ir.kooleh.app.Utility.Volley.Controller;
import ir.kooleh.app.View.Ui.Activity.Tools.ComChannels;
import ir.kooleh.app.View.Ui.Activity.Tools.CustomSnackbar;
import ir.kooleh.app.View.Ui.Activity.Tools.MoreInfo;
import ir.kooleh.app.View.Ui.Activity.Tools.MoreItemsInfoAdapter;
import ir.kooleh.app.View.Ui.Activity.Tools.ServiceActivity;
import ir.kooleh.app.View.Ui.Activity.Tools.TabInformationService;
import ir.kooleh.app.View.Ui.Activity.Tools.UserDefault;
import ir.kooleh.app.View.ViewCustom.CTextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InformationServiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InformationServiceFragment extends Fragment implements /*OnMapReadyCallback*/ View.OnClickListener, AdvancedWebView.Listener
{

    private String serviceId;
    public TabInformationService tabInformationService;
    private MoreItemsInfoAdapter moreItemsInfoAdapter;
//    private GoogleMap googleMap;
    private boolean isMapReady = false;
    private UserDefault userDefault;
    private Button btnRouter;


    private RecyclerView rvMoreInformationService;
    private LinearLayout llTellInformationService;
    private CTextView tvTelNumberInformationService;
    private ImageView ivTelNumberInformationService;
    private LinearLayout llInstaInformationService;
    private CTextView tvInstaInformationService;
    private ImageView ivInstaInformationService;
    private LinearLayout llTelegramInformationService;
    private CTextView tvTelegramInformationService;
    private ImageView ivTelegramInformationService;
    private LinearLayout llWebInformationService;
    private CTextView tvWebInformationService;
    private ImageView ivWebInformationService;
//    private SupportMapFragment mapInformationService;
    private LinearLayout llLocationInformationService;
    private AdvancedWebView wvWebInformationService;


    public InformationServiceFragment()
    {
        // Required empty public constructor
    }

    public static InformationServiceFragment newInstance(String serviceId)
    {
        InformationServiceFragment fragment = new InformationServiceFragment();
        Bundle args = new Bundle();
        args.putString(serviceId, "1");
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            serviceId = getArguments().getString("1");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_information_service, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        btnRouter = (Button) view.findViewById(R.id.btn_routing_information_service);
        rvMoreInformationService = (RecyclerView) view.findViewById(R.id.rv_more_information_service);
        llTellInformationService = (LinearLayout) view.findViewById(R.id.ll_tell_information_service);
        tvTelNumberInformationService = (CTextView) view.findViewById(R.id.tv_tel_number_information_service);
        ivTelNumberInformationService = (ImageView) view.findViewById(R.id.iv_tel_number_information_service);
        llInstaInformationService = (LinearLayout) view.findViewById(R.id.ll_insta_information_service);
        tvInstaInformationService = (CTextView) view.findViewById(R.id.tv_insta_information_service);
        ivInstaInformationService = (ImageView) view.findViewById(R.id.iv_insta_information_service);
        llTelegramInformationService = (LinearLayout) view.findViewById(R.id.ll_telegram_information_service);
        tvTelegramInformationService = (CTextView) view.findViewById(R.id.tv_telegram_information_service);
        ivTelegramInformationService = (ImageView) view.findViewById(R.id.iv_telegram_information_service);
        llWebInformationService = (LinearLayout) view.findViewById(R.id.ll_web_information_service);
        llLocationInformationService = (LinearLayout) view.findViewById(R.id.ll_location_information_service);
        tvWebInformationService = (CTextView) view.findViewById(R.id.tv_web_information_service);
        ivWebInformationService = (ImageView) view.findViewById(R.id.iv_web_information_service);
        wvWebInformationService = (AdvancedWebView) view.findViewById(R.id.wv_information_service);

        btnRouter.setOnClickListener(this);

    }

    public void setTabInformationService(TabInformationService tabInformationService)
    {
        this.tabInformationService = tabInformationService;
    }

    public void setUpData()
    {

        if (tabInformationService == null)
        {
            return;
        }
        else
        {
//            SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_information_service);
//            supportMapFragment.getMapAsync(this);
            rvMoreInformationService.setLayoutManager(new LinearLayoutManager(getContext()));
            moreItemsInfoAdapter = new MoreItemsInfoAdapter(getContext(), tabInformationService.getMoreInfo(), new MoreItemsInfoAdapter.OnClickItemListener()
            {
                @Override
                public void onClickItem(View view, MoreInfo item)
                {

                }
            });
            rvMoreInformationService.setAdapter(moreItemsInfoAdapter);
            ComChannels channels = tabInformationService.getComChannels();

            if (channels == null)
                return;

            if (channels.getTelNumber() != null)
            {
                llTellInformationService.setBackgroundResource(R.drawable.border_info_link_on);
                ivTelNumberInformationService.setColorFilter(getActivity().getResources().getColor(R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvTelNumberInformationService.setTextColor(getResources().getColor(R.color.black));
                llTellInformationService.setOnClickListener(this);

            }

            if (channels.getInsta() != null)
            {
                llInstaInformationService.setBackgroundResource(R.drawable.border_info_link_on);
                ivInstaInformationService.setColorFilter(ContextCompat.getColor(getContext(), R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvInstaInformationService.setTextColor(getResources().getColor(R.color.black));
                llInstaInformationService.setOnClickListener(this);
            }

            if (channels.getTelegram() != null)
            {
                llTelegramInformationService.setBackgroundResource(R.drawable.border_info_link_on);
                ivTelegramInformationService.setColorFilter(ContextCompat.getColor(getContext(), R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvTelegramInformationService.setTextColor(getResources().getColor(R.color.black));
                llTelegramInformationService.setOnClickListener(this);
            }

            if (channels.getWebsite() != null)
            {
                llWebInformationService.setBackgroundResource(R.drawable.border_info_link_on);
                ivWebInformationService.setColorFilter(ContextCompat.getColor(getContext(), R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
                tvWebInformationService.setTextColor(getResources().getColor(R.color.black));
                llWebInformationService.setOnClickListener(this);
            }

        }

//        if (tabInformationService.getStadylink()) {
//            if (userDefault.getUser() != null && userDefault.getUser().getApiToken() != null) {
//                String apiToken = userDefault.getUser().getApiToken();
//                wvWebInformationService.addHttpHeader("Authorization", "Bearer " + apiToken);
//
//            }
        wvWebInformationService.setListener(getActivity(), this);
        if (tabInformationService.getStudyLink() != null && tabInformationService.getStudyLink().toString() != "")
        {
            wvWebInformationService.setVisibility(View.VISIBLE);
            wvWebInformationService.loadUrl(tabInformationService.getStudyLink().toString());

        }
        // }
    }

//
//    @Override
//    public void onMapReady(GoogleMap googleMap)
//    {
//
//        this.googleMap = googleMap;
//        isMapReady = true;
//        googleMap.getUiSettings().setRotateGesturesEnabled(false);
//        googleMap.getUiSettings().setAllGesturesEnabled(false);
//
//        if (tabInformationService != null)
//            setMarkerOnMap();
//    }

    public void setMarkerOnMap()
    {

        if (tabInformationService.getLocation() != null && tabInformationService.getLocation().getLat() != null && tabInformationService.getLocation().getLong() != null)
        {
            LatLng latLng = new LatLng(Double.valueOf(tabInformationService.getLocation().getLat()), Double.valueOf(tabInformationService.getLocation().getLong()));
//            googleMap.addMarker(new MarkerOptions().position(latLng).title(tabInformationService.getLocation().getFaName()));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
//            googleMap.animateCamera(cameraUpdate);
        }
        else
        {
            llLocationInformationService.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v)
    {

        ComChannels channels = tabInformationService.getComChannels();

        if (v == btnRouter)
        {
            if (tabInformationService.getLocation().getLat() != null && !tabInformationService.getLocation().getLat().equals("") &&
                    tabInformationService.getLocation().getLong() != null && !tabInformationService.getLocation().getLong().equals(""))
            {
                String uri = String.format(Locale.ENGLISH, "geo:0,0?q=") + Uri.encode(String.format("%s@%f,%f", ServiceActivity.Title, Double.valueOf(tabInformationService.getLocation().getLat()), Double.valueOf(tabInformationService.getLocation().getLong())), "UTF-8");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        }

        if (v == llTellInformationService)
        {
            if (channels.getTelNumber() != null)
            {
                //intent to call
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + channels.getTelNumber()));
                startActivity(intent);
            }
        }
        else if (v == llInstaInformationService)
        {
            if (channels.getInsta() != null && !channels.getInsta().equals(""))
            {
                Uri uri = Uri.parse(channels.getInsta());
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                likeIng.setPackage("com.instagram.android");

                try
                {
                    startActivity(likeIng);
                }
                catch (ActivityNotFoundException e)
                {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(channels.getInsta())));
                }

            }
        }
        else if (v == llTelegramInformationService)
        {
            if (channels.getTelegram() != null && !channels.getTelegram().equals(""))
            {
                //intent to call
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(channels.getTelegram().toString()));
                startActivity(i);
            }
        }
        else if (v == llWebInformationService)
        {
            if (channels.getWebsite() != null && !channels.getWebsite().equals(""))
            {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(channels.getWebsite()));
                startActivity(i);
            }
        }
    }

    public String getMimeType(String url)
    {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null)
        {
            if (extension.equals("js"))
            {
                return "text/javascript";
            }
            else if (extension.equals("woff"))
            {
                return "application/font-woff";
            }
            else if (extension.equals("woff2"))
            {
                return "application/font-woff2";
            }
            else if (extension.equals("ttf"))
            {
                return "application/x-font-ttf";
            }
            else if (extension.equals("eot"))
            {
                return "application/vnd.ms-fontobject";
            }
            else if (extension.equals("svg"))
            {
                return "image/svg+xml";
            }
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;

    }


    @Override
    public void onPageStarted(String url, Bitmap favicon)
    {

    }

    @Override
    public void onPageFinished(String url)
    {
        wvWebInformationService.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl)
    {

        wvWebInformationService.setVisibility(View.INVISIBLE);
        CustomSnackbar.makeAction(Controller.context, getView(), "در گرفتن اطلاعات خطایی رخ داده است", "تلاش مجدد", Snackbar.LENGTH_INDEFINITE, new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                wvWebInformationService.reload();
            }
        }).show();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent)
    {

    }

    @Override
    public void onExternalPageRequest(String url)
    {

    }
}

package ir.kooleh.app.View.ViewCustom;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import ir.kooleh.app.R;

public class ViewLoading
{
    private final AlertDialog alertDialog;
    private final Context context;

    public ViewLoading(Context context)
    {
        this.context = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.layout_view_loading, null);
        builder.setView(inflater);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }


    public void show()
    {
        if (alertDialog != null)
        {
            close();
            if (context instanceof Activity)
            {
                if (!((Activity) context).isFinishing())
                {
                    alertDialog.show();
                }
            }
        }
    }

    public void close()
    {
        if (alertDialog != null)
        {
            if (alertDialog.isShowing())
            {
                alertDialog.dismiss();
            }
        }
    }
}
package ir.kooleh.app.View.Layout;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Alert.InterfaceAlertShowList;
import ir.kooleh.app.Structure.Model.Alert.ModelAlertShowList;
import ir.kooleh.app.Utility.Adapter.Layout.Alert.AdapterAlertShowList;

public class AlertDialogShowList extends InterfaceAlertShowList implements InterfaceAlertShowList.Adapter
{

    private static AlertDialog alertDialog;
    private TextView tvTitleAlterShowList, tvResultAlterDialog;
    private ImageView ivCloseDialog;
    private RecyclerView rvAlertShowList;
    private final Context context;
    private List<ModelAlertShowList> listAlterShowList;
    private Response responseShowList;
    private InterfaceAlertShowList.Adapter iAdapterShowList;
    private AdapterAlertShowList adapterAlertShowList;

    public AlertDialogShowList(Context context)
    {
        this.context = context;

    }


    public void Create(String message, List<ModelAlertShowList> listAlterShowList, InterfaceAlertShowList.Response responseShowList)
    {

        this.listAlterShowList = listAlterShowList;
        this.responseShowList = responseShowList;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.layout_alert_dialog_show_list, null);
        builder.setView(inflater);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        initView(inflater);
        createVar(message);

        alertDialog.show();


    }


    private void initView(View view)
    {
        tvTitleAlterShowList = view.findViewById(R.id.tv_title_alter_dialog_show_list);
        tvResultAlterDialog = view.findViewById(R.id.tv_result_alter_dialog_show_list);
        rvAlertShowList = view.findViewById(R.id.rv_alert_dialog_show_list);
        ivCloseDialog = view.findViewById(R.id.iv_close_alert_show_list);

        ivCloseDialog.setOnClickListener(v ->
        {
            if (alertDialog != null)
                alertDialog.dismiss();
        });

    }

    private void createVar(String message)
    {
        setTitle(message);
        setTextResultAlter();


        rvAlertShowList.setAdapter(adapterAlertShowList = new AdapterAlertShowList(context, this));
        rvAlertShowList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        adapterAlertShowList.setListAlert(listAlterShowList);


    }

    private void setTextResultAlter()
    {
        tvResultAlterDialog.setText(String.format(" تعداد نتایج : %s", listAlterShowList.size()));

    }


    public void setTitle(String massage)
    {
        tvTitleAlterShowList.setText(String.format("%s", massage));
    }

    public void show()
    {
        if (alertDialog != null)
        {
            if (alertDialog.isShowing())
                alertDialog.dismiss();

            alertDialog.show();
        }


    }

    List<ModelAlertShowList> newListAlert = new ArrayList<>();

    @Override
    public void OnClickItemAlertShowDialog(ModelAlertShowList modelAlertShowList)
    {
        newListAlert.clear();
        for (ModelAlertShowList alert : listAlterShowList)
        {
            if (alert.getParentId().equals(modelAlertShowList.getParentId()))
                if (!modelAlertShowList.getId().equals(alert.getId()))
                    newListAlert.add(alert);
        }

        if (newListAlert.size() == 0)
        {
            alertDialog.dismiss();
            responseShowList.OnResponseAlterShowList(modelAlertShowList);
        }

        else
        {
            listAlterShowList.clear();
            listAlterShowList = newListAlert;
            adapterAlertShowList.setListAlert(newListAlert);
            setTextResultAlter();
        }

    }
}

package ir.kooleh.app.View.Ui.Activity.Auth;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.google.android.material.textfield.TextInputEditText;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.auth.ApiAuth;
import ir.kooleh.app.Structure.Interface.Auth.InterfaceAuth;
import ir.kooleh.app.Structure.Model.User.ModelUser;
import ir.kooleh.app.Structure.StaticValue.SVTypeUser;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.InfoAppAndDevice;
import ir.kooleh.app.Utility.Class.SettingKeyboard;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.ViewCustom.ViewLoading;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, InterfaceAuth.Register, CompoundButton.OnCheckedChangeListener
{
    private ImageView ivBack;
    private EditText etFirstName, etLastName;
    private TextInputEditText etPassword, etAgainPassword;
    private Button btnRegister;
    private ViewLoading viewLoading;
    private CustomToast customToast;
    private Activity context;
    private RadioButton rbDriver, rbMasterLoad, rbFreight;
    private UserShared userShared;
    private ApiAuth apiAuth;


    private String typeUser = SVTypeUser.DRIVER;
    public static final String PHONE_NUMBER = "phone_number";
    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = RegisterActivity.this;
        findView();
        getIntentData();
        createVar();
    }


    private void findView()
    {
        ivBack = findViewById(R.id.iv_back_register);
        etFirstName = findViewById(R.id.et_first_name_register);
        etLastName = findViewById(R.id.et_last_name_register);
        etPassword = findViewById(R.id.et_password_register);
        etAgainPassword = findViewById(R.id.et_again_password_register);
        btnRegister = findViewById(R.id.btn_register);
        rbDriver = findViewById(R.id.rb_driver_register);
        rbMasterLoad = findViewById(R.id.rb_master_load_register);
        rbFreight = findViewById(R.id.rb_freight_register);


        ivBack.setOnClickListener(this);

        rbDriver.setOnCheckedChangeListener(this);
        rbMasterLoad.setOnCheckedChangeListener(this);
        rbFreight.setOnCheckedChangeListener(this);

        btnRegister.setOnClickListener(this);
    }

    private void createVar()
    {
        viewLoading = new ViewLoading(this);
        userShared = new UserShared(context);
        apiAuth = new ApiAuth(context);
    }


    private void getIntentData()
    {
        Bundle bundle = getIntent().getExtras();
        phoneNumber = bundle.getString(PHONE_NUMBER, "-1");
        if (phoneNumber.equals("-1") || phoneNumber.length() != 11)
        {
            customToast = new CustomToast(this);
            customToast.showError(getString(R.string.error_send_data_intent));
            finish();
        }
    }

    @Override
    public void onClick(View view)
    {
        if (view == ivBack)
            finish();
        else if (view == btnRegister)
            registerUser();
    }

    private void registerUser()
    {
        String message = checkValue();
        if (!message.equals(""))
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
            return;
        }

        SettingKeyboard.hideKeyboard(context);
        viewLoading.show();
        apiAuth.Register
                (
                        this,
                        phoneNumber,
                        etFirstName.getText().toString(),
                        etLastName.getText().toString(),
                        etPassword.getText().toString(),
                        typeUser,
                        InfoAppAndDevice.getAndroidId()
                );
    }


    private String checkValue()
    {

        if (etFirstName.getText().toString().length() == 0)
            return "نام نمی تواند خالی باشد";
        else if (etLastName.getText().toString().length() == 0)
            return "نام خانوادگی نمی تواند خالی باشد";
        else if (etPassword.getText().toString().length() < 6)
            return "حداقل گذرواژه شیش کارکتر می باشد";
        else if (etAgainPassword.getText().toString().length() == 0)
            return "تکرار رمز  عبور شیش کارکتر می باشد";
        if (!etAgainPassword.getText().toString().equals(etPassword.getText().toString()))
            return "گذرواژه با تکرار گذرواژه یکسان نمی باشد";

        if (!typeUser.equals(SVTypeUser.DRIVER) && !typeUser.equals(SVTypeUser.MASTER_LOAD) && !typeUser.equals(SVTypeUser.FREIGHT))
            return "لطفا  نوع کاربری خود را انتخاب کنید";

        return "";
    }

    @Override
    public void OnResponseRegister(boolean response, ModelUser user, String message)
    {
        viewLoading.close();
        if (response)
        {
            try
            {
                customToast = new CustomToast(this);
                customToast.showSuccess(message);
                userShared.setData(user);
                Intent intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
            catch (Exception e)
            {
                customToast = new CustomToast(this);
                customToast.showError(e.getMessage());
            }

        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        if (isChecked)
        {
            if (buttonView == rbDriver)
                typeUser = SVTypeUser.DRIVER;

            else if (buttonView == rbMasterLoad)
                typeUser = SVTypeUser.MASTER_LOAD;

            else if (buttonView == rbFreight)
                typeUser = SVTypeUser.FREIGHT;
        }
    }
}
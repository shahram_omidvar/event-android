package ir.kooleh.app.View.Ui.Activity.Profile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.User.ApiProfile;
import ir.kooleh.app.Structure.Interface.User.InterfaceProfile;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.User.Profile.ModelTargetProfile;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterProfileDetails;
import ir.kooleh.app.Utility.Class.UtilStrings;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, InterfaceProfile.FragmentProfile,
        InterfaceMessageHandler.FragmentResult
{


    private Activity context;
    private ImageView ivProfile;
    private TextView tvFullNameProfile, tvPhoneNumberProfile;
    private CardView btnEditProfile;
    private FragmentMessageHandler fragmentMessageHandler;
    private RecyclerView rvProfileDetails;
    private CTextView tvTitleToolbar;
    private ImageView imgBackToolbar;

    private FrameLayout containerFragmentMessageHandler;
    private NestedScrollView nsvLayout;

    //var
    private ViewLoading viewLoading;
    private ApiProfile apiProfile;
    private AdapterProfileDetails adapterProfileDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initView();
        createVar();
        getProfile();
    }

    private void initView()
    {
        containerFragmentMessageHandler = findViewById(R.id.container_message_handler_fragment);
        nsvLayout = findViewById(R.id.nsv_activity_profile_details);

        ivProfile = findViewById(R.id.iv_profile_user);
        tvFullNameProfile =findViewById(R.id.tv_full_name_profile);

        btnEditProfile = findViewById(R.id.btn_edit_info_profile);
        rvProfileDetails = findViewById(R.id.rv_profile_details);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        imgBackToolbar = findViewById(R.id.iv_back_toolbar);

        tvFullNameProfile.setSelected(true);

        tvTitleToolbar.setText("مشخصات کاربری");

        btnEditProfile.setOnClickListener(this);
        imgBackToolbar.setOnClickListener(this);

//        nsvLayout.setVisibility(View.GONE);
    }

    private void createVar()
    {
        context = ProfileActivity.this;
        viewLoading = new ViewLoading(context);
        apiProfile = new ApiProfile(context);

        adapterProfileDetails = new AdapterProfileDetails(this);
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setJustifyContent(JustifyContent.CENTER);
        rvProfileDetails.setLayoutManager(flexboxLayoutManager);
        rvProfileDetails.setAdapter(adapterProfileDetails);
    }


    private void getProfile()
    {
        viewLoading.show();
        apiProfile.getProfileFragment(this);
    }

    @Override
    public void onClick(View view)
    {
        if (view == btnEditProfile)
        {
            startActivityForResult(new Intent(context, EditProfileActivity.class), 1);
        }
        else if (view == imgBackToolbar)
        {
            finish();
        }

    }

    @Override
    public void onResponseGetProfileFragment(boolean response, ModelTargetProfile profile, String message)
    {
        nsvLayout.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);
        viewLoading.close();
        if (response)
        {
            nsvLayout.setVisibility(View.VISIBLE);
            btnEditProfile.setVisibility(View.VISIBLE);
            setProfileInfo(profile);
        }
        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
//            containerFragmentMessageHandler.setVisibility(View.VISIBLE);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }

    }


    private void setProfileInfo(ModelTargetProfile profile)
    {

        tvFullNameProfile.setText(String.format("%s", profile.getInfo().getInfoProfile().getFullName()));
//        adapterProfileDetails.setList(UtilStrings.getProfileDetailsList(profile.getLineOne(), profile.getLineTwo()));
        if (profile.getInfo().getInfoProfile().getImage() != null && profile.getInfo().getInfoProfile().getImage().length() > 0)
        {
            Glide.with(context).load(profile.getInfo().getInfoProfile().getImage()).placeholder(R.drawable.ic_avatar).into(ivProfile);
        }
    }

    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        getProfile();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == HomeActivity.RESULT_CODE_USER_TYPE_CHANGE)
        {
            viewLoading.show();
            getProfile();
            setResult(HomeActivity.RESULT_CODE_USER_TYPE_CHANGE);
        }
    }
}

package ir.kooleh.app.View.Ui.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Ads.ApiAuction;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceBottomSheetCreateOffer;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class BottomSheetDialogCreateOffer extends BottomSheetDialogFragment implements
        View.OnClickListener,
        InterfaceAds.AuctionAd.intApiCreateOffer
{
    private ImageView ivClose;
    private CEditText etOfferAmount;
    private CardView btnApplyOffer;

    private ViewLoading viewLoading;
    private CustomToast customToast;
    private final InterfaceBottomSheetCreateOffer interfaceBottomSheetCreateOffer;
    private final ApiAuction apiAuction;

    private final String adID;

    public BottomSheetDialogCreateOffer(@NonNull Context context, String adID)
    {
        this.adID = adID;
        viewLoading = new ViewLoading(context);

        apiAuction = new ApiAuction(context);
        interfaceBottomSheetCreateOffer = (InterfaceBottomSheetCreateOffer) context;
    }


    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.bottom_sheet_dialog_create_offer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        ivClose = view.findViewById(R.id.iv_close_bottom_sheet_create_offer);
        etOfferAmount = view.findViewById(R.id.et_offer_amount_bottom_sheet_create_offer);
        btnApplyOffer = view.findViewById(R.id.btn_apply_offer_bottom_sheet_create_offer);

        ivClose.setOnClickListener(this);
        btnApplyOffer.setOnClickListener(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetTransparent);
        return super.onCreateDialog(savedInstanceState);
    }


    @Override
    public void onClick(View v)
    {
        if (v == ivClose)
        {
            this.dismiss();
        }
        else if (v == btnApplyOffer)
        {
            if (!etOfferAmount.getText().toString().equals(""))
            {
                viewLoading.show();
                apiAuction.createOffer(this, adID, Long.parseLong(etOfferAmount.getText().toString().replaceAll(",", "")));
            }
            else
            {
                customToast = new CustomToast(this.getContext());
                customToast.showError("لطفا مقدار پیشنهاد خود را وارد کنید");
            }
        }
    }


    @Override
    public void onResponseCreateOffer(boolean response, String message)
    {
        viewLoading.close();
        if (response)
        {
            customToast = new CustomToast(this.getContext());
            customToast.showSuccess(message);
            this.dismiss();
            interfaceBottomSheetCreateOffer.onResponseBottomSheetCreateOffer();
        }
        else
        {
            customToast = new CustomToast(this.getContext());
            customToast.showError(message);
        }
    }


}

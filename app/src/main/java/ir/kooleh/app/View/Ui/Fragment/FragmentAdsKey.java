package ir.kooleh.app.View.Ui.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Ads.ApiAds;

import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Enum.PaginateValues;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;

import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Ads.ModelAdsKey;

import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Utility.Adapter.Fragment.AdapterAdsKey;

import ir.kooleh.app.Utility.Class.CustomLayoutPaginate;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.View.Ui.Activity.Ads.AdDetailsActivity;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.Ui.Activity.Tools.ServiceActivity;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class FragmentAdsKey extends Fragment implements
        View.OnClickListener,
        InterfaceAds.Fragment.AdsKey.AdapterAdsKey,
        InterfaceAds.Fragment.AdsKey.ServerAdsKey,
        InterfaceTryAgainPaginate,
        Paginate.Callbacks
{
    //View
    private View view;
    private RecyclerView rvAdsKey;
    private SwipeRefreshLayout swipeRefresh;

    //Var
    private AdapterAdsKey adapterAdsKey;
    private ApiAds apiAds;
    private ViewLoading viewLoading;
    private Context context;

    //key
    private String key;
    private ModelPaginate modelPaginate;
    private List<ModelAdsKey.ListAd> listAdsKey = new ArrayList<>();
    private Paginate paginate;
    private boolean loading = true;
    private int nextPage = 1;

    private InterfaceMessageHandler.HomeFragmentsResponse homeFragmentsResponse;

    private Parcelable recylerViewState;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_ads_key, container, false);
//        initView(view);
        startActivity(new Intent(getActivity(), ServiceActivity.class));
//        createVar();
//        viewLoading = new ViewLoading(this.getContext());
//        viewLoading.show();
//        getIndexLoadFragment();
        return view;
    }

    public void initView(View view)
    {
        rvAdsKey = view.findViewById(R.id.rv_fragment_ads_key);
        swipeRefresh = view.findViewById(R.id.swipe_refresh_fragment_ads_key);
    }


    private void createVar()
    {
        context = getContext();
        apiAds = new ApiAds(context);
        rvAdsKey.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvAdsKey.setAdapter(adapterAdsKey = new AdapterAdsKey(getContext(), this));
        key = getArguments().getString("value");
        homeFragmentsResponse = (HomeActivity) getActivity();
        swipeRefresh.setOnRefreshListener(()->
        {
            nextPage = 1;
            rvAdsKey.setAdapter(adapterAdsKey = new AdapterAdsKey(getContext(), this));
            recylerViewState = rvAdsKey.getLayoutManager().onSaveInstanceState();
            listAdsKey = new ArrayList<>();
            if (paginate != null)
            {
                paginate.unbind();
                paginate = null;
            }
            getIndexLoadFragment();
        });
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.primary), getResources().getColor(R.color.color_primary));
    }


    @Override
    public void onClick(View v)
    {
    }


    private void getIndexLoadFragment()
    {
        apiAds.getAdsKey(this, key, nextPage, PaginateValues.itemsPerPage);
    }


    @Override
    public void onResponseAdsKey(boolean response, ModelAdsKey modelAdsKey, ModelPaginate modelPaginate, String message)
    {
        viewLoading.close();

        swipeRefresh.setRefreshing(false);

        if (response)
        {
            loading = false;
            this.modelPaginate = modelPaginate;
            this.listAdsKey.addAll(modelAdsKey.getListAd());
            if (modelAdsKey == null || modelAdsKey.getListAd() == null || (modelAdsKey.getListAd() != null && modelAdsKey.getListAd().size() == 0))
            {
                Bundle args = new Bundle();
                args.putString("message", "آیتمی برای نمایش پیدا نشد");
                args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.EMPTY);
                args.putString(InterClassDataPassKeys.FragmentMessageHandler.FRAGMENT_TYPE, "fragment_key");
                homeFragmentsResponse.onResponseHomeFragments(args);
                return;
            }
            adapterAdsKey.setList(modelAdsKey.getListTypeAd(), modelAdsKey.getListAd());
            if (paginate == null)
            {
                paginate = Paginate.with(rvAdsKey, this)
                        .setLoadingTriggerThreshold(PaginateValues.threshold)
                        .addLoadingListItem(true)
                        .setLoadingListItemCreator(new CustomLayoutPaginate())
                        .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
                        .build();
                rvAdsKey.getLayoutManager().onRestoreInstanceState(recylerViewState);
            }
        }
        else
        {
            if (nextPage == 1)
            {
                Bundle args = new Bundle();
                args.putString("message", message);
                args.putString(InterClassDataPassKeys.FragmentMessageHandler.FRAGMENT_TYPE, "fragment_key");
                args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.NO_NETWORK);
                homeFragmentsResponse.onResponseHomeFragments(args);
            }
            else
            {
                adapterAdsKey.showBtnTryAgain();
                if (paginate != null)
                {
                    paginate.unbind();
                }
            }
        }

    }


    @Override
    public void onDetach()
    {
        adapterAdsKey.killTimers();
        super.onDetach();
    }

    @Override
    public void onClickItemAdKey(ModelAdsKey.ListAd modelAdsKey)
    {
        Intent intent = new Intent(context, AdDetailsActivity.class);
        intent.putExtra(AdDetailsActivity.Ad_ID, modelAdsKey.getIdAd());
        intent.putExtra(AdDetailsActivity.ADS_TITLE, modelAdsKey.getStory());
        startActivity(intent);
    }


    @Override
    public void onLoadMore()
    {
        Log.i("paginateOnloadMore", String.valueOf(rvAdsKey.getAdapter().getItemCount()));
        loading = true;
        nextPage++;
        getIndexLoadFragment();
    }

    @Override
    public boolean isLoading()
    {
        Log.i("paginateisloading", String.valueOf(loading));
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems()
    {
        if (modelPaginate != null)
        {
            Log.i("paginateHasloadedAll", String.valueOf(Integer.parseInt(modelPaginate.getTotal())));
            return listAdsKey.size() == Integer.parseInt(modelPaginate.getTotal());
        }
        return  false;
    }

    @Override
    public void onResponseTryAgainPaginate()
    {
        paginate = null;
        recylerViewState = rvAdsKey.getLayoutManager().onSaveInstanceState();
        getIndexLoadFragment();
    }
}
package ir.kooleh.app.View.Ui.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Ads.ApiAds;
import ir.kooleh.app.Server.Api.Bill.ApiBills;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Enum.PaginateValues;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceBillType;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceOfferType;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Ads.ModelAdsKey;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.Structure.Model.Bills.ModelListSuggestion;
import ir.kooleh.app.Utility.Adapter.Bill.AdapterBillCategorySubList;
import ir.kooleh.app.Utility.Adapter.Bill.AdapterOfferCategorySubList;
import ir.kooleh.app.Utility.Adapter.Fragment.AdapterAdsKey;
import ir.kooleh.app.Utility.Class.CustomLayoutPaginate;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.View.Ui.Activity.Ads.AdDetailsActivity;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.Ui.Activity.Tools.ServiceActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class FragmentOffer extends Fragment implements
        InterfaceOfferType.OfferType.IntGetOfferType
{
    View view;

    private RecyclerView rvBillTypes;
    private AdapterOfferCategorySubList AdapterOfferCategorySubList;

//    private NestedScrollView nsvLayout;
//    private FrameLayout containerFragmentMessageHandler;
//    private FragmentMessageHandler fragmentMessageHandler;

    private ViewLoading viewLoading;
    private ApiBills apiBills;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_offer, container, false);
        initViews();
        initVars();
        return view;
    }

    private void initViews()
    {

        rvBillTypes = view.findViewById(R.id.rv_bill_category_types);
//        nsvLayout = view.findViewById(R.id.nsv_activity_select_bill_type);
//        containerFragmentMessageHandler = view.findViewById(R.id.container_message_handler_fragment);

    }

    private void initVars()
    {
        viewLoading = new ViewLoading(getActivity());
        apiBills = new ApiBills(getActivity());

        AdapterOfferCategorySubList = new AdapterOfferCategorySubList(getActivity());
        rvBillTypes.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvBillTypes.setAdapter(AdapterOfferCategorySubList);

        viewLoading.show();
        apiBills.getOffersType(this);
    }



//    @Override
//    public void onResponseMessageHandler()
//    {
//        getActivity().getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
//        fragmentMessageHandler = null;
//        viewLoading.show();
//        apiBills.getOffersType(this);
//    }

    @Override
    public void onResponseGetOfferType(boolean response, String message, List<ModelListSuggestion> modelListSuggestions)
    {
        viewLoading.close();
//        nsvLayout.setVisibility(View.GONE);
//        containerFragmentMessageHandler.setVisibility(View.GONE);
        if (response)
        {
//            nsvLayout.setVisibility(View.VISIBLE);
            AdapterOfferCategorySubList.setList(modelListSuggestions);
        }
        else
        {
//            fragmentMessageHandler = new FragmentMessageHandler(this);
//            containerFragmentMessageHandler.setVisibility(View.VISIBLE);
//            Bundle args = new Bundle();
//            args.putString("message", message);
//            fragmentMessageHandler.setArguments(args);
//            getActivity().getSupportFragmentManager().beginTransaction().replace(
//                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }
}
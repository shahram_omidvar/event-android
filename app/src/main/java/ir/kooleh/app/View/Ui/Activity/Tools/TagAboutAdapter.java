package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

import ir.kooleh.app.R;

public class TagAboutAdapter extends RecyclerView.Adapter<TagAboutAdapter.AdapterHolder> {

    private Context context;
    private List<String> list;
    private OnClickItemListener itemListener ;

    public TagAboutAdapter(Context context , List<String> list , OnClickItemListener itemListener) {

        this.context = context;
        this.list = list;
        this.itemListener = itemListener;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final AdapterHolder viewHolder = new AdapterHolder(LayoutInflater.from(context).inflate(R.layout.item_tag_about_service, parent , false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdapterHolder holder, int position) {
        final String item = list.get(position);
        holder.tvTitle.setText(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemListener.onClickItem(v,item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class AdapterHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;

        public AdapterHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_tag_name_service);
        }
    }

    public interface OnClickItemListener{
        void onClickItem(View view, String item);
    }
}

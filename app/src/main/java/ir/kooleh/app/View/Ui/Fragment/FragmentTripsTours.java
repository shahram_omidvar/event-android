package ir.kooleh.app.View.Ui.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Dashboard.ApiDashboard;
import ir.kooleh.app.Server.Api.Load.ApiLoadFragment;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Enum.PaginateValues;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Structure.Interface.Load.InterfaceLoad;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Dashboard.ModelMyAd;
import ir.kooleh.app.Structure.Model.Load.ModelListLoad;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterMyAdStep;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterViewPagerMyAds;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterViewPagerTripsTours;
import ir.kooleh.app.Utility.Adapter.Load.AdapterFragmentLoad;
import ir.kooleh.app.Utility.Class.CustomLayoutPaginate;
import ir.kooleh.app.Utility.Class.DashboardUtils;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.Utility.Class.MessageHandlerUtils;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.Ui.Activity.Load.LoadActivity;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class FragmentTripsTours extends Fragment implements
        View.OnClickListener,
        InterfaceLoad.Fragment,
        InterfaceTryAgainPaginate,
        Paginate.Callbacks
{
    private int position;
    private String keyOne, keyTwo;

    private View view;
    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView rvFragmentLoad;

    private AdapterFragmentLoad adapterFragmentLoad;
    private ApiLoadFragment apiLoadFragment;
    private ViewLoading viewLoading;
    private FragmentMessageHandler fragmentMessageHandler;
    private InterfaceMessageHandler.HomeFragmentsResponse homeFragmentsResponse;


    public static final String ID_AD = "id_ad";
    private ModelPaginate modelPaginate;
    private List<ModelListLoad> listLoads = new ArrayList<>();
    private Paginate paginate;
    private boolean loading = true;
    private int nextPage = 1;

    private Parcelable recylerViewState;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_trips_tours, container, false);
        initView(view);
        createVar();
        return view;
    }

    public void initView(View view)
    {
        swipeRefresh = view.findViewById(R.id.swipe_refresh_fragment_load);
        rvFragmentLoad = view.findViewById(R.id.rv_fragment_load);

        position = getArguments().getInt(AdapterViewPagerTripsTours.POSITION, 0);
        keyOne = getArguments().getString(AdapterViewPagerTripsTours.KEYONE, "");
        keyTwo = getArguments().getString(AdapterViewPagerTripsTours.KEYTWO, "");
    }

    private void createVar()
    {
        viewLoading = new ViewLoading(getContext());
        apiLoadFragment = new ApiLoadFragment(getContext());
        rvFragmentLoad.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvFragmentLoad.setAdapter(adapterFragmentLoad = new AdapterFragmentLoad(getContext(), this));
        homeFragmentsResponse = (HomeActivity) getActivity();
        swipeRefresh.setOnRefreshListener(()-> {
            nextPage = 1;
            rvFragmentLoad.setAdapter(adapterFragmentLoad = new AdapterFragmentLoad(getContext(), this));
            recylerViewState = rvFragmentLoad.getLayoutManager().onSaveInstanceState();
            listLoads = new ArrayList<>();
            if (paginate != null)
            {
                paginate.unbind();
                paginate = null;
            }
            viewLoading.show();
            getIndexLoadFragment();
        });
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.primary), getResources().getColor(R.color.color_primary));
    }


    private void getIndexLoadFragment()
    {
        if (position == 0)
        {
            apiLoadFragment.getIndexLoadFragment(this, nextPage, PaginateValues.itemsPerPage, keyOne);

        }
        else
        {
            apiLoadFragment.getIndexLoadFragment(this, nextPage, PaginateValues.itemsPerPage, keyTwo);

        }
    }


    @Override
    public void onResponseListLoadFragment(boolean response, List<ModelListLoad> listLoad, ModelPaginate modelPaginate, String message)
    {
        viewLoading.close();
        swipeRefresh.setRefreshing(false);
        if (response)
        {
            loading = false;
            this.modelPaginate = modelPaginate;
            this.listLoads.addAll(listLoad);
            if (listLoad != null && listLoad.size() == 0)
            {
                Bundle args = new Bundle();
                args.putString("message", "آیتمی برای نمایش پیدا نشد");
                args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.EMPTY);
                args.putString(InterClassDataPassKeys.FragmentMessageHandler.FRAGMENT_TYPE, "fragment_load");
                homeFragmentsResponse.onResponseHomeFragments(args);
                return;
            }
            adapterFragmentLoad.addList(listLoad);
            if (paginate == null)
            {
                paginate = Paginate.with(rvFragmentLoad, this)
                        .setLoadingTriggerThreshold(PaginateValues.threshold)
                        .addLoadingListItem(true)
                        .setLoadingListItemCreator(new CustomLayoutPaginate())
                        .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
                        .build();
                rvFragmentLoad.getLayoutManager().onRestoreInstanceState(recylerViewState);
            }
        }
        else
        {
            if (nextPage == 1)
            {
                Bundle args = new Bundle();
                args.putString("message", message);
                args.putString(InterClassDataPassKeys.FragmentMessageHandler.FRAGMENT_TYPE, "fragment_load");
                args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.NO_NETWORK);
                homeFragmentsResponse.onResponseHomeFragments(args);
            }
            else
            {
                adapterFragmentLoad.showBtnTryAgain();
                if (paginate != null)
                {
                    paginate.unbind();
                }
            }
        }
    }


    @Override
    public void onOnclickItemLoadFragment(ModelListLoad load)
    {
        Intent intent = new Intent(getContext(), LoadActivity.class);
        intent.putExtra(ID_AD, load.getId());
        startActivity(intent);
    }



    @Override
    public void setMenuVisibility(boolean menuVisible)
    {
        super.setMenuVisibility(menuVisible);
        if (menuVisible)
        {
            if (paginate != null)
            {
                paginate.unbind();
                paginate = null;
            }
            nextPage = 1;
            listLoads = new ArrayList<>();
            adapterFragmentLoad = new AdapterFragmentLoad(getContext(), this);
            rvFragmentLoad.setAdapter(adapterFragmentLoad);
            recylerViewState = null;
            viewLoading.show();
            fragmentMessageHandler = null;
            getIndexLoadFragment();
        }
    }









    @Override
    public void onDetach()
    {
        fragmentMessageHandler = null;
        super.onDetach();
    }


    @Override
    public void onLoadMore()
    {
        Log.i("paginateOnloadMore", String.valueOf(listLoads.size()));
        loading = true;
        nextPage++;
        getIndexLoadFragment();
    }

    @Override
    public boolean isLoading()
    {
        Log.i("paginateisloading", String.valueOf(loading));
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems()
    {
        if (modelPaginate != null)
        {
            Log.i("paginateHasloadedAll", String.valueOf(listLoads.size() == Integer.parseInt(modelPaginate.getTotal())));
            return listLoads.size() == Integer.parseInt(modelPaginate.getTotal());
        }
        return  false;
    }

    private void resetList()
    {
        adapterFragmentLoad = new AdapterFragmentLoad(getContext(), this);
        rvFragmentLoad.setAdapter(adapterFragmentLoad);
        listLoads = new ArrayList<>();
        recylerViewState = rvFragmentLoad.getLayoutManager().onSaveInstanceState();

        if (paginate != null)
        {
            paginate.unbind();
            paginate = null;
        }
        nextPage = 1;
        viewLoading.show();
        getIndexLoadFragment();
    }

    @Override
    public void onResponseTryAgainPaginate()
    {
        paginate = null;
        recylerViewState = rvFragmentLoad.getLayoutManager().onSaveInstanceState();
        getIndexLoadFragment();
    }

    @Override
    public void onClick(View v)
    {

    }
}

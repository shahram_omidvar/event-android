package ir.kooleh.app.View.Ui.Activity.Bill;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Bill.ApiBills;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceBillType;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.Utility.Adapter.Bill.AdapterBillCategorySubList;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class SelectBillTypeActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfaceBillType.BillType.IntGetBillTypes,
        InterfaceMessageHandler.FragmentResult
{
    private CTextView tvTitleToolbar;
    private ImageView ivBackToolbar;
    private RecyclerView rvBillTypes;
    private AdapterBillCategorySubList adapterBillCategorySubLista;
    private ImageView ivBackground;

    private NestedScrollView nsvLayout;
    private FrameLayout containerFragmentMessageHandler;
    private FragmentMessageHandler fragmentMessageHandler;

    private ViewLoading viewLoading;
    private ApiBills apiBills;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_bill_type);
        initViews();
        initVars();
    }

    private void initViews()
    {
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        ivBackToolbar = findViewById(R.id.iv_back_toolbar);
        rvBillTypes = findViewById(R.id.rv_bill_category_types);
        nsvLayout = findViewById(R.id.nsv_activity_select_bill_type);
        containerFragmentMessageHandler = findViewById(R.id.container_message_handler_fragment);
        ivBackground = findViewById(R.id.iv_background_select_bill_type);

        tvTitleToolbar.setText("پرداخت قبوض");
        ivBackToolbar.setOnClickListener(this);
    }

    private void initVars()
    {
        viewLoading = new ViewLoading(this);
        apiBills = new ApiBills(this);


        adapterBillCategorySubLista = new AdapterBillCategorySubList(this);
//        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(this);
//        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
//        flexboxLayoutManager.setJustifyContent(JustifyContent.CENTER);
        rvBillTypes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvBillTypes.setAdapter(adapterBillCategorySubLista);

        viewLoading.show();
        apiBills.getBillTypes(this);
    }


    @Override
    public void onClick(View v)
    {
        if (v == ivBackToolbar)
        {
            finish();
        }
    }

    @Override
    public void onResponseGetBillTypes(boolean response, String message, List<ModelBillCategories> billTypeList, String backgroundImage)
    {
        viewLoading.close();
        nsvLayout.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);
        if (response)
        {
            Glide.with(this).load(backgroundImage).into(ivBackground);
            nsvLayout.setVisibility(View.VISIBLE);
            adapterBillCategorySubLista.setList(billTypeList);
        }
        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            containerFragmentMessageHandler.setVisibility(View.VISIBLE);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }

    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        viewLoading.show();
        apiBills.getBillTypes(this);
    }
}
package ir.kooleh.app.View.Ui.Activity.SplashScreen;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.App.ApiApp;
import ir.kooleh.app.Structure.Enum.EnCustomMessage;
import ir.kooleh.app.Structure.Interface.App.InterfaceVersion;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceDialogVersionChecking;
import ir.kooleh.app.Structure.Model.App.ModelInfoAppDevice;
import ir.kooleh.app.Structure.Model.App.ModelInfoVersion;
import ir.kooleh.app.Utility.Class.InfoAppAndDevice;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.View.Ui.Activity.Auth.CheckExistsUserActivity;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogVersionChecking;
import ir.kooleh.app.View.ViewCustom.CTextView;


public class SplashActivity extends Activity implements
        InterfaceVersion.IntInfoVersion,
        InterfaceDialogVersionChecking
{
    private ProgressBar progressBar;
    private CTextView tvVersion;

    private ApiApp apiApp;
    private CustomDialogVersionChecking customDialogVersionChecking;
    private InfoAppAndDevice infoAppAndDevice;
    private ModelInfoAppDevice modelInfoAppDevice;
    private ModelInfoVersion modelInfoVersion;

    private static final int SPLASH_DISPLAY_LENGTH = 3000;
    public static final int REQUEST_CODE_UPDATE_APP = 2001;
    public static final int REQUEST_CODE_LOGOUT = 2002;
    public static final int REQUEST_CODE_CLOSE_APP = 2003;
    public static final int REQUEST_CODE_TRY_AGAIN = 2004;
    public static final int REQUEST_CODE_Dialog_chooser = 2005;

    @Override
    public void onCreate(Bundle icicle)
    {
        super.onCreate(icicle);
        setContentView(R.layout.layout_splash_screen);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        initViews();
        initVars();

        new Handler().postDelayed(() ->
        {
            apiApp.getInfoVersion(this, modelInfoAppDevice);
            progressBar.setVisibility(View.VISIBLE);
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void initViews()
    {
        tvVersion = findViewById(R.id.tv_version_app_splash);
        progressBar = findViewById(R.id.progress_splash);
    }

    private void initVars()
    {
        infoAppAndDevice = new InfoAppAndDevice(this);
        apiApp = new ApiApp(this);
        customDialogVersionChecking = new CustomDialogVersionChecking(this, this);
        modelInfoAppDevice = infoAppAndDevice.getInfoDeviceAndApp();

        try
        {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            tvVersion.setText(String.format(" نسخه %s", pInfo.versionName));
        }
        catch (PackageManager.NameNotFoundException ignored) { }
    }


    private void startSuitableActivity()
    {
        UserShared usernameShared = new UserShared(SplashActivity.this);
        Intent i;
        if (usernameShared.checkExist())
            i = new Intent(SplashActivity.this, HomeActivity.class);
        else
            i = new Intent(SplashActivity.this, CheckExistsUserActivity.class);
        startActivity(i);
        finish();
    }

    public void onResponseInfoVersion(boolean response, String message, ModelInfoVersion modelInfoVersion)
    {
        progressBar.setVisibility(View.GONE);
        this.modelInfoVersion = modelInfoVersion;
        if (response)
        {
            if (modelInfoVersion.getShow())
            {
                setInfoApp();
            }
            else
            {
                startSuitableActivity();
            }
        }
        else
        {
            customDialogVersionChecking.showDialog(message, "سعی مجدد",
                    false, REQUEST_CODE_TRY_AGAIN, false);
        }
    }


    private void setInfoApp()
    {
        CustomDialogVersionChecking customDialogVersionChecking = new CustomDialogVersionChecking(this,
                this);
        if (modelInfoVersion.getUpdateServer())
        {
            customDialogVersionChecking.showDialogOK(modelInfoVersion.getMessage(), "تایید", -1,
                    false);
            return;
        }
        if (modelInfoVersion.getUpdateApp())
        {
            if (modelInfoVersion.getCloseDialog())
            {
                customDialogVersionChecking.showDialog(modelInfoVersion.getMessage(), "دانلود",
                        true, REQUEST_CODE_UPDATE_APP, true);
            }
            else
            {
                customDialogVersionChecking.showDialogOK(modelInfoVersion.getMessage(), "دانلود",
                        REQUEST_CODE_UPDATE_APP, false);
            }
            return;
        }

        customDialogVersionChecking.showDialogOK(modelInfoVersion.getMessage(), "تایید",
                -1, false);
    }


    @Override
    public void onClickDialogVersionChecking(String message, EnCustomMessage enCustomMessage, int requestCode)
    {
        if (requestCode == REQUEST_CODE_UPDATE_APP)
        {
            if (enCustomMessage == EnCustomMessage.YES)
            {
                final String appPackageName = getPackageName();
                try
                {
                    startActivityForResult(
                            new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)),
                            REQUEST_CODE_Dialog_chooser);
                }
                catch (android.content.ActivityNotFoundException anfe)
                {
                    startActivityForResult(
                            new Intent(Intent.ACTION_VIEW, Uri.parse
                                    ("https://play.google.com/store/apps/details?id=" + appPackageName)),
                            REQUEST_CODE_Dialog_chooser);
                }
            }
            else if (enCustomMessage == EnCustomMessage.NO)
            {
                startSuitableActivity();
            }
        }
        else if (requestCode == REQUEST_CODE_CLOSE_APP)
        {
            finish();
        }
        else if (requestCode == REQUEST_CODE_TRY_AGAIN)
        {
            progressBar.setVisibility(View.VISIBLE);
            apiApp.getInfoVersion(this, modelInfoAppDevice);
        }
        else
        {
            startSuitableActivity();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_Dialog_chooser)
        {
            if (modelInfoVersion.getCloseDialog())
            {
                startSuitableActivity();
            }
        }
    }
}

package ir.kooleh.app.View.Ui.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Dashboard.ApiDashboard;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Dashboard.ModelDashboard;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterDashboardBtns;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterMostViewedDashboard;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterSliderDashboard;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.Ui.Activity.Profile.EditProfileActivity;
import ir.kooleh.app.View.Ui.Activity.Wallet.BuysActivity;
import ir.kooleh.app.View.Ui.Activity.Wallet.PackageActivity;
import ir.kooleh.app.View.Ui.Activity.Wallet.WalletActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class FragmentDashboard extends Fragment implements InterfaceDashboard.Apis.IntGetProfile, View.OnClickListener
{
    private View view;
    private NestedScrollView nsvFragmentDashboard;
    private CTextView tvFullname, tvExplainTwo;
//    private ImageView btnCloseToolbar;
//    private CTextView tvTitleToolbar;
    private ImageView ivCircleProfile;
    private CTextView tvWalletAmount, tvPackagesNumber, tvBuysNumber, tvBillsNumber;
    private RecyclerView rvDashboardButtons;
    private CardView btnEditProfileDashboard;
    private CardView btnWallet, btnPackages, btnBuys, btnBills;
    private RecyclerView rvMostViewed;
    private ViewPager2 viewPager2Slider;
    private DotsIndicator dotsIndicator;
    private CardView containerViewpagerSlider2;


    private Handler handler;
    private Runnable runnable;
    private InterfaceMessageHandler.HomeFragmentsResponse homeFragmentsResponse;

    private AdapterSliderDashboard adapterSliderDashboard;
    private ApiDashboard apiDashboard;
    private ViewLoading viewLoading;
    private AdapterDashboardBtns adapterDashboardBtns;
//    private AdapterMostViewedDashboard adapterMostViewedDashboard;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        initViews();
        initVars();
        return view;
    }


    private void initViews()
    {
        nsvFragmentDashboard = view.findViewById(R.id.nsv_fragment_dashboard);
        rvDashboardButtons = view.findViewById(R.id.rv_dashboard_buttons);
        tvFullname = view.findViewById(R.id.tv_fullname_dashboard);
        tvExplainTwo = view.findViewById(R.id.tv_explain_two_dashboard);
        ivCircleProfile = view.findViewById(R.id.iv_circle_profile_dashboard);
        tvWalletAmount = view.findViewById(R.id.tv_wallet_amount_dashboard);
        tvPackagesNumber = view.findViewById(R.id.tv_packages_number_dashboard);
        tvBuysNumber = view.findViewById(R.id.tv_buys_numbers_dashboard);
        tvBillsNumber = view.findViewById(R.id.tv_bills_numbers_dashboard);
        btnEditProfileDashboard = view.findViewById(R.id.btn_edit_profile_dashboard);
        btnWallet = view.findViewById(R.id.btn_wallet_dashboard);
        btnPackages = view.findViewById(R.id.btn_packages_dashboard);
        btnBuys = view.findViewById(R.id.btn_buys_dashboard);
        btnBills = view.findViewById(R.id.btn_bills_dashboard);
        rvMostViewed = view.findViewById(R.id.rv_most_viewed_dashboard);
        viewPager2Slider = view.findViewById(R.id.viewpater2_slider_dashboard);
        dotsIndicator = view.findViewById(R.id.dots_indicator_slider_dashboard);
        containerViewpagerSlider2 = view.findViewById(R.id.container_viewpager_slider2);

        tvFullname.setSelected(true);

        btnEditProfileDashboard.setOnClickListener(this);

        btnWallet.setOnClickListener(this);
        btnPackages.setOnClickListener(this);
        btnBuys.setOnClickListener(this);
        btnBills.setOnClickListener(this);

        rvDashboardButtons.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        nsvFragmentDashboard.setVisibility(View.GONE);
        containerViewpagerSlider2.setVisibility(View.GONE);
    }

    private void initVars()
    {
        apiDashboard = new ApiDashboard(getContext());
        apiDashboard.getProfile(this);
        viewLoading = new ViewLoading(getContext());
        viewLoading.show();
        handler = new Handler();

//        adapterMostViewedDashboard = new AdapterMostViewedDashboard(getActivity());
        rvMostViewed.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
//        rvMostViewed.setAdapter(adapterMostViewedDashboard);

        adapterSliderDashboard = new AdapterSliderDashboard(getActivity());
        viewPager2Slider.setAdapter(adapterSliderDashboard);
        homeFragmentsResponse = (HomeActivity) getActivity();


        viewPager2Slider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback()
        {
            @Override
            public void onPageSelected(int position)
            {
                super.onPageSelected(position);
                startSliderCycle(position);
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
                super.onPageScrollStateChanged(state);
                if (state == ViewPager2.SCROLL_STATE_DRAGGING)
                {
                    startSliderCycle(viewPager2Slider.getCurrentItem());
                }

            }
        });

    }


    private void startSliderCycle(int position)
    {
        if (runnable != null)
        {
            handler.removeCallbacks(runnable);
        }
        runnable = () ->
        {
            int numPages = viewPager2Slider.getAdapter().getItemCount();
            viewPager2Slider.setCurrentItem((position + 1) % numPages);
            handler.postDelayed(runnable, 5000);
        };
        handler.postDelayed(runnable, 5000);
    }


    @Override
    public void onResponseGetProfile(boolean response, ModelDashboard modelDashboard, String message)
    {
        viewLoading.close();

        if (response)
        {
            nsvFragmentDashboard.setVisibility(View.VISIBLE);
            if (modelDashboard.getDashboardData().getDashboardSliderList() != null && modelDashboard.getDashboardData().getDashboardSliderList().size() > 0)
            {
                containerViewpagerSlider2.setVisibility(View.VISIBLE);
                adapterSliderDashboard.setListSlider(modelDashboard.getDashboardData().getDashboardSliderList());
                dotsIndicator.setViewPager2(viewPager2Slider);
            }
//            adapterMostViewedDashboard.setList(modelDashboard.getMostViewedDashboardList());
            tvFullname.setText(modelDashboard.getProfile().getFullName());
            tvExplainTwo.setText(modelDashboard.getProfile().getExplain());
            tvWalletAmount.setText(NumberUtils.digitDivider(String.valueOf(modelDashboard.getMoney().getWallet())) + " ریال");
            tvPackagesNumber.setText(NumberUtils.digitDivider(String.valueOf(modelDashboard.getMoney().getPackages())));
            tvBuysNumber.setText(NumberUtils.digitDivider(String.valueOf(modelDashboard.getMoney().getBuyOther())));
            tvBillsNumber.setText(NumberUtils.digitDivider(String.valueOf(modelDashboard.getMoney().getBuyBill())));

            if (modelDashboard.getProfile().getImage() != null)
                Glide.with(getActivity()).load(modelDashboard.getProfile().getImage()).placeholder(R.drawable.ic_avatar).into(ivCircleProfile);

            adapterDashboardBtns = new AdapterDashboardBtns(getActivity(), modelDashboard.getDashboardData().getDashboardBtns());
            rvDashboardButtons.setAdapter(adapterDashboardBtns);
        }
        else
        {
            Bundle args = new Bundle();
            args.putString("message", message);
            args.putString(InterClassDataPassKeys.FragmentMessageHandler.FRAGMENT_TYPE, "fragment_dashboard");
            args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.NO_NETWORK);
            homeFragmentsResponse.onResponseHomeFragments(args);
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == btnEditProfileDashboard)
        {
            startActivityForResult(new Intent(getActivity(), EditProfileActivity.class), 1);
        }
        else if (v == btnWallet)
        {
            Intent intent = new Intent(getActivity(), WalletActivity.class);
            startActivityForResult(intent, 1);
        }
        else if (v == btnPackages)
        {
            Intent intent = new Intent(getActivity(), PackageActivity.class);
            startActivityForResult(intent, 2);
        }
        else if (v == btnBuys)
        {
            Intent intent = new Intent(getActivity(), BuysActivity.class);
            intent.putExtra(BuysActivity.KEY_MODEL_BUY, BuysActivity.VALUE_MODEL_OTHER);
            intent.putExtra(BuysActivity.KEY_TITLE_BUY, "خرید ها");
            startActivity(intent);
        }
        else if (v == btnBills)
        {
            Intent intent = new Intent(getActivity(), BuysActivity.class);
            intent.putExtra(BuysActivity.KEY_MODEL_BUY, BuysActivity.VALUE_MODEL_OTHER);
            intent.putExtra(BuysActivity.KEY_TITLE_BUY, "قبوض پرداختی");
            startActivity(intent);
        }
    }

    @Override
    public void onDestroy()
    {
        if (runnable != null)
        {
            handler.removeCallbacks(runnable);
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1)
        {
            viewLoading.show();
            apiDashboard.getProfile(this);
        }
    }
}

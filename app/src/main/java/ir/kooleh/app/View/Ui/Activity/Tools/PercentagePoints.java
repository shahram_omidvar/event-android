package ir.kooleh.app.View.Ui.Activity.Tools;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PercentagePoints {

@SerializedName("rate")
@Expose
private String rate;
@SerializedName("count_points")
@Expose
private Integer countPoints;
@SerializedName("good")
@Expose
private Integer good;
@SerializedName("medium")
@Expose
private Integer medium;
@SerializedName("weak")
@Expose
private Integer weak;

public String getRate() {
return rate;
}

public void setRate(String rate) {
this.rate = rate;
}

public Integer getCountPoints() {
return countPoints;
}

public void setCountPoints(Integer countPoints) {
this.countPoints = countPoints;
}

public Integer getGood() {
return good;
}

public void setGood(Integer good) {
this.good = good;
}

public Integer getMedium() {
return medium;
}

public void setMedium(Integer medium) {
this.medium = medium;
}

public Integer getWeak() {
return weak;
}

public void setWeak(Integer weak) {
this.weak = weak;
}

}
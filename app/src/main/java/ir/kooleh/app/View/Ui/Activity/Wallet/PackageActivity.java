package ir.kooleh.app.View.Ui.Activity.Wallet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Wallet.ApiPackage;
import ir.kooleh.app.Structure.Enum.EnumWalletModule;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceBottomSheetPaymentAd;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Interface.Wallet.InterfacePackage;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelPackage;
import ir.kooleh.app.Structure.Model.Wallet.ModelUserPackage;
import ir.kooleh.app.Utility.Adapter.Wallet.AdapterPackages;
import ir.kooleh.app.Utility.Adapter.Wallet.AdapterUserPackages;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.Utility.Class.MessageHandlerUtils;
import ir.kooleh.app.View.Ui.Activity.SplashScreen.SplashActivity;
import ir.kooleh.app.View.Ui.Activity.general.ViewAllItemsActivity;
import ir.kooleh.app.View.Ui.Dialog.BottomSheetDialogPayment;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class PackageActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfacePackage.Package.IntGetPackages,
        InterfacePackage.Package.IntGetUserPackages,
        InterfacePackage.Package.IntCallbackBtnBuyPackageItem,
        InterfaceMessageHandler.FragmentResult,
        InterfaceBottomSheetPaymentAd
{
    private CTextView tvTitleToolbar;
    private ImageView ivCloseActivity;
    private Toolbar toolbar;
    private CardView btnBuyPackage;
    private RecyclerView rvUserPackages;
    private RecyclerView rvPackages;
    private BottomSheetDialog bottomSheetDialog;
    private ImageView ivCloseBottomSheetPackages;
    private NestedScrollView nstActivityPackages;
    private CTextView tvEmptyListUserPackages;
    private CTextView tvViewAllItems;

    private FragmentMessageHandler fragmentMessageHandler;
    private FrameLayout containerFragmentMessageHandler;

    private AdapterUserPackages adapterUserPackages;
    private AdapterPackages adapterPackages;

    private ViewLoading viewLoading;
    private ApiPackage apiPackage;

    private final int nextPage = 1;
    private final int itemsPerPage = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package);
        setupBottomDialog();
        initViews();
        initVars();
    }

//    @Override
//    protected void onNewIntent(Intent intent)
//    {
//        super.onNewIntent(intent);
//        setIntent(intent);
//    }
//
//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        if (getIntent() != null)
//        {
//            Bundle data = getIntent().getExtras();
//            setIntent(null);
//            Log.i("package_activity", String.valueOf(data));
//            if (data != null)
//            {
//                String status = data.getString("status");
//                if (status.equals("ok"))
//                {
//                    setResult(1);
//                    viewLoading.show();
//                    resetUserPackageList();
//                }
//            }
//        }
//    }

    private void initViews()
    {
        toolbar = findViewById(R.id.toolbar_app);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        ivCloseActivity = findViewById(R.id.iv_back_toolbar);
        rvUserPackages = findViewById(R.id.rv_user_packages);
        btnBuyPackage = findViewById(R.id.btn_buy_package);
        nstActivityPackages = findViewById(R.id.nst_activity_packages);
        containerFragmentMessageHandler = findViewById(R.id.container_message_handler_fragment);
        tvEmptyListUserPackages = findViewById(R.id.tv_empty_list_activity_packages);
        tvViewAllItems = findViewById(R.id.tv_view_all_items);

        tvTitleToolbar.setText("بسته ها");
        tvTitleToolbar.setTextColor(getResources().getColor(R.color.black));
        ivCloseActivity.setImageResource(R.drawable.ic_back_black);
        toolbar.setBackgroundColor(getResources().getColor(R.color.yellow_100));
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.yellow_400));
        rvUserPackages.setNestedScrollingEnabled(false);

        ivCloseActivity.setOnClickListener(this);
        btnBuyPackage.setOnClickListener(this);
        tvViewAllItems.setOnClickListener(this);
    }


    private void initVars()
    {
        adapterUserPackages = new AdapterUserPackages(this);
        viewLoading = new ViewLoading(this);

        rvUserPackages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvUserPackages.setAdapter(adapterUserPackages);

        apiPackage = new ApiPackage(this);

        viewLoading.show();
        apiPackage.getPackages(this, nextPage, 1000);
        apiPackage.getUserPackages(this, nextPage, itemsPerPage);
    }


    private void setupBottomDialog() {
        bottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetTransparent);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_dialog_packages);
        ivCloseBottomSheetPackages = bottomSheetDialog.findViewById(R.id.iv_close_bottom_sheet_packages);
        rvPackages = bottomSheetDialog.findViewById(R.id.rv_packages);
        rvPackages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapterPackages = new AdapterPackages(this);
        rvPackages.setAdapter(adapterPackages);

        ivCloseBottomSheetPackages.setOnClickListener(this);
    }


    private void showError(String message)
    {
        fragmentMessageHandler = new FragmentMessageHandler(this);
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, MessageHandlerUtils.getMessageTypeByMessage(message));
        fragmentMessageHandler.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        containerFragmentMessageHandler.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v)
    {
        if (v == ivCloseActivity)
        {
            if (isTaskRoot())
            {
                startActivity(new Intent(this, SplashActivity.class));
            }
            finish();
        }
        else if (v == btnBuyPackage)
        {
            bottomSheetDialog.show();
        }
        else if (v == ivCloseBottomSheetPackages)
        {
            bottomSheetDialog.dismiss();
        }
        else if (v == tvViewAllItems)
        {
            Intent intent = new Intent(this, ViewAllItemsActivity.class);
            intent.putExtra(ViewAllItemsActivity.ITEMS_TYPE, ViewAllItemsActivity.ITEMS_TYPE_PACKAGE);
            intent.putExtra(ViewAllItemsActivity.TITLE_TOOLBAR, "لیست بسته ها");
            startActivity(intent);
        }
    }

    @Override
    public void onResponseGetUserPackages(boolean response, String message, List<ModelUserPackage> userPackageList, ModelPaginate modelPaginate)
    {
        viewLoading.close();
        rvUserPackages.setVisibility(View.GONE);
        tvEmptyListUserPackages.setVisibility(View.GONE);
        nstActivityPackages.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);

        if (response)
        {
            nstActivityPackages.setVisibility(View.VISIBLE);
            if (userPackageList.size() == 0)
            {
                tvEmptyListUserPackages.setVisibility(View.VISIBLE);
            }
            else
            {
                rvUserPackages.setVisibility(View.VISIBLE);
                adapterUserPackages.setList(userPackageList);
            }
        }
        else {
            showError(message);
        }
    }

    @Override
    public void onResponseGetPackages(boolean response, String message, List<ModelPackage> packageList)
    {
        viewLoading.close();
        nstActivityPackages.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);

        if (response)
        {
            nstActivityPackages.setVisibility(View.VISIBLE);
            adapterPackages.setList(packageList);
        }
        else {
            showError(message);
        }
    }

    @Override
    public void onResponseCallbackBtnBuyPackageItem(ModelPackage modelPackage)
    {
        bottomSheetDialog.dismiss();
        BottomSheetDialogPayment bottomSheetDialogPayment;
        bottomSheetDialogPayment = new BottomSheetDialogPayment(this, EnumWalletModule.BuyModels.MODEL_PACKAGE,
                "خرید بسته " + modelPackage.getName(), modelPackage.getPrice(), modelPackage.getId());
        bottomSheetDialogPayment.show(getSupportFragmentManager(), "bottomSheetPayment");
    }


    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        viewLoading.show();
        resetUserPackageList();
        apiPackage.getPackages(this, 1, 1000);
    }

    @Override
    public void onResponseBottomSheetPaymentAd()
    {
        setResult(1);
        viewLoading.show();
        resetUserPackageList();
    }

    @Override
    public void onBackPressed()
    {
        if (isTaskRoot())
        {
            startActivity(new Intent(this, SplashActivity.class));
            finish();
        }
        super.onBackPressed();
    }

    private void resetUserPackageList()
    {
        rvUserPackages.setAdapter(adapterUserPackages = new AdapterUserPackages(this));
        apiPackage.getUserPackages(this, nextPage, itemsPerPage);
    }
}
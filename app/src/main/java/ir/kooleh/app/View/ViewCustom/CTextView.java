package ir.kooleh.app.View.ViewCustom;




import android.content.Context;
import android.util.AttributeSet;

import ir.kooleh.app.Utility.Volley.Controller;


public class CTextView extends androidx.appcompat.widget.AppCompatTextView
{
    public CTextView(Context context)
    {
        super(context);
        if(!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        if(!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CTextView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }
}


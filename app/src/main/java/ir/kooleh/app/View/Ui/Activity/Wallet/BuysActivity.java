package ir.kooleh.app.View.Ui.Activity.Wallet;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Wallet.ApiBuys;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Enum.PaginateValues;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceBuys;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelBuy;
import ir.kooleh.app.Structure.Model.Wallet.ModelTransactionDetails;
import ir.kooleh.app.Utility.Adapter.Wallet.AdapterBuys;
import ir.kooleh.app.Utility.Class.CustomLayoutPaginate;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.Utility.Class.MessageHandlerUtils;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogTransactionDetails;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class BuysActivity extends AppCompatActivity
        implements View.OnClickListener,
        InterfaceBuys.Buys.IntGetBuysUser,
        InterfaceBuys.Buys.IntOpenMoreDetailsTransaction,
        InterfaceBuys.Buys.IntGetTransactionDetails,
        InterfaceMessageHandler.FragmentResult,
        InterfaceTryAgainPaginate,
        Paginate.Callbacks
{
    private Toolbar toolbar;
    private CTextView tvTitleToolbar;
    private ImageView ivCloseActivity;
    private LinearLayout lnrBuysActivity;

    private FragmentMessageHandler fragmentMessageHandler;
    private FrameLayout containerFragmentMessageHandler;

    private RecyclerView rvBuys;
    private AdapterBuys adapterBuys;

    private ViewLoading viewLoading;
    private ApiBuys apiBuys;

    private ModelPaginate modelPaginate;
    private List<ModelBuy> buyList = new ArrayList<>();
    private boolean loading = true;
    private Paginate paginate;
    private int nextPage = 1;

    private Parcelable recylerViewState;

    public static final String KEY_MODEL_BUY = "modelBuy";
    public static final String KEY_TITLE_BUY = "titleBuy";

    public static final String VALUE_MODEL_OTHER = "other";
    public static final String VALUE_MODEL_Bill = "bill";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buys);
        initViews();
        initVars();
    }

    private void initViews()
    {

        toolbar = findViewById(R.id.toolbar_app);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        ivCloseActivity = findViewById(R.id.iv_back_toolbar);
        rvBuys = findViewById(R.id.rv_buys);
        lnrBuysActivity = findViewById(R.id.lnr_buys_activity);
        containerFragmentMessageHandler = findViewById(R.id.container_message_handler_fragment);

        tvTitleToolbar.setText(getIntent().getExtras().getString(KEY_TITLE_BUY));
        tvTitleToolbar.setTextColor(getResources().getColor(R.color.black));
        ivCloseActivity.setImageResource(R.drawable.ic_back_black);
        toolbar.setBackgroundColor(getResources().getColor(R.color.blue_100));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.blue_300));

        ivCloseActivity.setOnClickListener(this);
    }

    private void initVars()
    {
        adapterBuys = new AdapterBuys(this);
        viewLoading = new ViewLoading(this);
        apiBuys = new ApiBuys(this);

        rvBuys.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvBuys.setAdapter(adapterBuys);

        viewLoading.show();
        apiBuys.getBuysUser(this, nextPage, PaginateValues.itemsPerPage, getIntent().getExtras().getString(KEY_MODEL_BUY));
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivCloseActivity)
        {
            finish();
        }
    }

    @Override
    public void onResponseGetBuysUser(boolean response, String message, List<ModelBuy> buyList, ModelPaginate modelPaginate)
    {
        viewLoading.close();
        lnrBuysActivity.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);
        if (response)
        {
            lnrBuysActivity.setVisibility(View.VISIBLE);
            loading = false;
            this.buyList.addAll(buyList);
            this.modelPaginate = modelPaginate;
            checkForEmptyList();
            adapterBuys.addList(buyList);
            if (paginate == null)
            {
                paginate = Paginate.with(rvBuys, this)
                        .setLoadingTriggerThreshold(PaginateValues.threshold)
                        .addLoadingListItem(true)
                        .setLoadingListItemCreator(new CustomLayoutPaginate())
                        .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
                        .build();
                rvBuys.getLayoutManager().onRestoreInstanceState(recylerViewState);
            }
        }
        else
        {
            if (nextPage == 1)
            {
                showError(message);
            }
            else
            {
                lnrBuysActivity.setVisibility(View.VISIBLE);
                adapterBuys.showBtnTryAgain();
                if (paginate != null)
                {
                    paginate.unbind();
                }
            }
        }
    }


    private void checkForEmptyList()
    {
        if (buyList != null && buyList.size() == 0)
        {
            lnrBuysActivity.setVisibility(View.GONE);
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", "آیتمی برای نمایش پیدا نشد");
            args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.EMPTY);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
            containerFragmentMessageHandler.setVisibility(View.VISIBLE);
        }
    }

    private void showError(String message)
    {
        fragmentMessageHandler = new FragmentMessageHandler(this);
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, MessageHandlerUtils.getMessageTypeByMessage(message));
        fragmentMessageHandler.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        containerFragmentMessageHandler.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        nextPage = 1;
        buyList = new ArrayList<>();
        rvBuys.setAdapter(adapterBuys = new AdapterBuys(this));
        if (paginate != null)
        {
            paginate.unbind();
            paginate = null;
        }
        viewLoading.show();
        apiBuys.getBuysUser(this, nextPage, PaginateValues.itemsPerPage, getIntent().getExtras().getString(KEY_MODEL_BUY));
    }

    @Override
    public void onLoadMore()
    {
        Log.i("paginateOnloadMore", String.valueOf(rvBuys.getAdapter().getItemCount()));
        loading = true;
        nextPage++;
        apiBuys.getBuysUser(this, nextPage, PaginateValues.itemsPerPage, getIntent().getExtras().getString(KEY_MODEL_BUY));
    }

    @Override
    public boolean isLoading()
    {
        Log.i("paginateisloading", String.valueOf(loading));
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems()
    {
        if (modelPaginate != null)
        {
            return buyList.size() == Integer.parseInt(modelPaginate.getTotal());
        }
        return  false;
    }

    @Override
    public void onResponseTryAgainPaginate()
    {
        paginate = null;
        recylerViewState = rvBuys.getLayoutManager().onSaveInstanceState();
        apiBuys.getBuysUser(this, nextPage, PaginateValues.itemsPerPage, getIntent().getExtras().getString(KEY_MODEL_BUY));
    }

    @Override
    public void onResponseOpenMoreDetailsTransaction(String transactionID)
    {
        viewLoading.show();
        apiBuys.getTransctionDetails(this, transactionID);
    }

    @Override
    public void onResponseGetTransactionDetails(boolean response, String message, ModelTransactionDetails modelTransactionDetails)
    {
        viewLoading.close();
        if (response)
        {
            CustomDialogTransactionDetails customDialogTransactionDetails = new CustomDialogTransactionDetails(modelTransactionDetails);
            customDialogTransactionDetails.make(this);
            customDialogTransactionDetails.show();
        }
        else
        {
            CustomToast customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }
}

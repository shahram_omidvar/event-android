package ir.kooleh.app.View.Ui.Activity.general;

import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import ir.kooleh.app.R;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class WebViewActivity extends AppCompatActivity implements View.OnClickListener
{
    private ImageView ivClose;
    private CTextView tvTitle;
    private Toolbar toolbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private WebView webView;
    private ProgressBar progressBar;

    public static final String KEY_NO_TOOLBAR = "noToolbar";
    public static final String KEY_TITLE_TOOLBAR = "titleToolbar";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        showWebView();
    }

    private void showWebView()
    {
        ivClose = findViewById(R.id.iv_close_toolbar_web_view);
        tvTitle = findViewById(R.id.tv_title_toolbar_web_view);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_activity_webview);
        progressBar = findViewById(R.id.progress_webview_activity);
        webView = findViewById(R.id.webview_activity);

        toolbar = findViewById(R.id.toolbar_webview);
        if (getIntent().getExtras().getBoolean(KEY_NO_TOOLBAR, false))
        {
            tvTitle.setText(getIntent().getExtras().getString(KEY_TITLE_TOOLBAR, ""));
        }

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.primary), getResources().getColor(R.color.color_primary));

        webView.setWebViewClient(WebViewClient);
        webView.setWebChromeClient(webChromeClient);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);


//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setAllowFileAccessFromFileURLs(true);
//        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
//        webView.clearCache(true);
//        webView.clearHistory();
//        webView.getSettings().setAllowContentAccess(true);
//        webView.getSettings().setDomStorageEnabled(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setSupportZoom(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setUseWideViewPort(true);
//
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setDisplayZoomControls(false);

        webView.loadUrl(getIntent().getExtras().getString("value"));

        ivClose.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(() ->
        {
            swipeRefreshLayout.setRefreshing(true);
            webView.reload();
        });
    }

    private final WebViewClient WebViewClient = new WebViewClient()
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);
            swipeRefreshLayout.setRefreshing(false);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error)
        {
//            super.onReceivedSslError(view, handler, error);
            handler.proceed();
        }
    };

    private final WebChromeClient webChromeClient = new WebChromeClient()
    {
        @Override
        public void onReceivedTitle(WebView view, String title)
        {
            super.onReceivedTitle(view, title);
            if (tvTitle != null && !getIntent().getExtras().getBoolean(KEY_NO_TOOLBAR, false))
            {
                tvTitle.setText(title);
            }
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            if(newProgress < 100 && progressBar.getVisibility() == ProgressBar.GONE)
            {
                progressBar.setVisibility(ProgressBar.VISIBLE);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                progressBar.setProgress(newProgress, true);
            }
            else
            {
                progressBar.setProgress(newProgress);
            }
            if (newProgress == 100)
            {
                progressBar.setVisibility(ProgressBar.GONE);
            }
        }
    };



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            switch (keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack())
                    {
                        webView.goBack();
                    }
                    else
                    {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onPause()
    {
        webView.onPause();
        super.onPause();

    }

    @Override
    protected void onResume()
    {
        webView.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        webView.destroy();
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivClose)
        {
            finish();
        }
    }
}

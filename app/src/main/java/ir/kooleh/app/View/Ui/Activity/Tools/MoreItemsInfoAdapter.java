package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

import ir.kooleh.app.R;

public class MoreItemsInfoAdapter extends RecyclerView.Adapter<MoreItemsInfoAdapter.AdapterHolder> {


    private Context context;
    private List<MoreInfo> list;
    private OnClickItemListener itemListener ;

    public MoreItemsInfoAdapter(Context context , List<MoreInfo> list , OnClickItemListener itemListener) {

        this.context = context;
        this.list = list;
        this.itemListener = itemListener;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final AdapterHolder viewHolder = new AdapterHolder(LayoutInflater.from(context).inflate(R.layout.item_more_info_service, parent , false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdapterHolder holder, int position) {
        final MoreInfo item = list.get(position);
        holder.tvTitle.setText(item.getTitle());
        holder.tvExplain.setText(item.getExplain());
        //todo set symbol for more info
        if(position == list.size()-1)
        {
            holder.view.setVisibility(View.VISIBLE);
        }
//        if (item.getImage()!=null && !item.getImage().equals(""))
//            Picasso.get().load(item.getImage()).into(holder.ivSymbol);
//        else
//            Picasso.get().load(R.drawable.triangle).into(holder.ivSymbol);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemListener.onClickItem(v,item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class AdapterHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle, tvExplain;
        private ImageView ivSymbol;
        private View view;

        public AdapterHolder(View itemView) {
            super(itemView);
            ivSymbol = (ImageView) itemView.findViewById(R.id.iv_symbol_more_info_item);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_more_info_item);
            tvExplain = (TextView) itemView.findViewById(R.id.tv_explain_more_info_item);
            view = itemView.findViewById(R.id.view_splitter_more_info_item);
        }
    }

    public interface OnClickItemListener{
        void onClickItem(View view, MoreInfo item);
    }
}

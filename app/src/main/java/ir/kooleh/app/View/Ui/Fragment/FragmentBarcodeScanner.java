package ir.kooleh.app.View.Ui.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.zxing.integration.android.IntentIntegrator;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceUserBill;

public class FragmentBarcodeScanner extends Fragment
{
    private View view;
    IntentIntegrator integrator;
    private InterfaceUserBill.BarcodeScannerFragment.IntCloseFragmnetBarcodeScanner intCloseFragmnetBarcodeScanner;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_barcode_scanner, container, false);
        initViews();
        initVars();
        intCloseFragmnetBarcodeScanner = (InterfaceUserBill.BarcodeScannerFragment.IntCloseFragmnetBarcodeScanner) getActivity();
        return view;
    }

    private void initViews()
    {
    }

    private void initVars()
    {
        integrator = new IntentIntegrator(getActivity());
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(true);
        integrator.initiateScan();
    }


    @Override
    public void onDetach()
    {
        super.onDetach();
        intCloseFragmnetBarcodeScanner.onResponseCloseFragment();
    }
}

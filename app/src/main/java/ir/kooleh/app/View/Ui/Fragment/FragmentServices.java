package ir.kooleh.app.View.Ui.Fragment;


import android.content.Context;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Service.ApiService;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Interface.Service.InterfaceService;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Service.ModelIndexService;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterSliderDashboard;
import ir.kooleh.app.Utility.Adapter.Fragment.AdapterRecyclerViewFragmentServiceHorizontal;
import ir.kooleh.app.Utility.Adapter.Fragment.AdapterRecyclerViewFragmentServiceSquare;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class FragmentServices extends Fragment implements InterfaceService.FragmentService, View.OnClickListener
{
    //view
    private LinearLayout lnrFragmentService;
    private RecyclerView rvServieSquare, rvServiceHorizontal;
    private ViewLoading viewLoading;
    private AdapterSliderDashboard adapterSliderServices;


    private Context context;
    private AdapterRecyclerViewFragmentServiceSquare adapterRecyclerViewFragmentServiceSquare;
    private AdapterRecyclerViewFragmentServiceHorizontal adapterRecyclerViewFragmentServiceHorizontal;
    private ApiService apiService;
    private ViewPager2 viewPager2Slider;
    private DotsIndicator dotsIndicator;

    private InterfaceMessageHandler.HomeFragmentsResponse homeFragmentsResponse;

    private Handler handler;
    private Runnable runnable;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.fragment_services, container, false);

        initView(view);
        createVar();

        getIndexService();

        return view;
    }


    public void initView(View view)
    {
        lnrFragmentService = view.findViewById(R.id.lnr_fragment_services);
        lnrFragmentService.setVisibility(View.GONE);
        viewPager2Slider = view.findViewById(R.id.viewpater2_slider_services);
        dotsIndicator = view.findViewById(R.id.dots_indicator_slider_services);
        rvServieSquare = view.findViewById(R.id.rv_service_square_fragment_services);
        rvServiceHorizontal = view.findViewById(R.id.rv_service_horizontal_fragment_services);

    }


    private void createVar()
    {
        context = getContext();
        viewLoading = new ViewLoading(context);
        adapterSliderServices = new AdapterSliderDashboard(getActivity());
        viewPager2Slider.setAdapter(adapterSliderServices);
        handler = new Handler();

        apiService = new ApiService(context);

        rvServieSquare.setLayoutManager(new GridLayoutManager(context, 4, GridLayoutManager.VERTICAL, false));
        rvServieSquare.setAdapter(adapterRecyclerViewFragmentServiceSquare = new AdapterRecyclerViewFragmentServiceSquare(getActivity()));

        rvServiceHorizontal.setLayoutManager(new LinearLayoutManager(context, GridLayoutManager.VERTICAL, false));
        rvServiceHorizontal.setAdapter(adapterRecyclerViewFragmentServiceHorizontal = new AdapterRecyclerViewFragmentServiceHorizontal(getActivity()));
        homeFragmentsResponse = (HomeActivity) getActivity();

        viewPager2Slider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback()
        {
            @Override
            public void onPageSelected(int position)
            {
                super.onPageSelected(position);
                startSliderCycle(position);
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
                super.onPageScrollStateChanged(state);
                if (state == ViewPager2.SCROLL_STATE_DRAGGING)
                {
                    startSliderCycle(viewPager2Slider.getCurrentItem());
                }
            }
        });
    }

    private void getIndexService()
    {
        viewLoading.show();
        apiService.getIndexService(this);
    }


    @Override
    public void onResponseIndexData(boolean response, ModelIndexService indexService, String message)
    {

        viewLoading.close();
        lnrFragmentService.setVisibility(View.GONE);

        if (response)
        {
            lnrFragmentService.setVisibility(View.VISIBLE);
            if (indexService.getSlider() != null && indexService.getSlider().size() > 0)
            {
                adapterSliderServices.setListSlider(indexService.getSlider());
                dotsIndicator.setViewPager2(viewPager2Slider);
            }

            if (indexService.getServiceSquareList() != null && indexService.getServiceSquareList().size() > 0)
            {
                adapterRecyclerViewFragmentServiceSquare.addListService(indexService.getServiceSquareList());
            }

            if (indexService.getServiceHorizontalList() != null && indexService.getServiceHorizontalList().size() > 0)
            {
                adapterRecyclerViewFragmentServiceHorizontal.addListService(indexService.getServiceHorizontalList());
            }

        }
        else
        {
            Bundle args = new Bundle();
            args.putString("message", message);
            args.putString(InterClassDataPassKeys.FragmentMessageHandler.FRAGMENT_TYPE, "fragment_service");
            args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.NO_NETWORK);
            homeFragmentsResponse.onResponseHomeFragments(args);
        }
    }

    private void startSliderCycle(int position)
    {
        if (runnable != null)
        {
            handler.removeCallbacks(runnable);
        }
        runnable = () ->
        {
            int numPages = viewPager2Slider.getAdapter().getItemCount();
            viewPager2Slider.setCurrentItem((position + 1) % numPages);
            handler.postDelayed(runnable, 5000);
        };
        handler.postDelayed(runnable, 5000);
    }


    @Override
    public void onClick(View view)
    {
    }

    @Override
    public void onDestroy()
    {
        if (runnable != null)
        {
            handler.removeCallbacks(runnable);
        }
        super.onDestroy();
    }
}

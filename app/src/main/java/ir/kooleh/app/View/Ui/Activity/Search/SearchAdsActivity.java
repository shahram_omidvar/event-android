package ir.kooleh.app.View.Ui.Activity.Search;

import androidx.appcompat.app.AppCompatActivity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;

import android.widget.ImageView;
import android.widget.LinearLayout;


import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Ads.ApiAds;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTextOnListener;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Search.ModelSearchAds;
import ir.kooleh.app.Utility.Adapter.Ads.Search.AdapterListSearchAds;
import ir.kooleh.app.Utility.Adapter.Ads.Search.AdapterSearchCategoryAds;
import ir.kooleh.app.View.Ui.Activity.Ads.AdDetailsActivity;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.CustomTextListener;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class SearchAdsActivity extends AppCompatActivity implements
        InterfaceAds.Search,
        View.OnClickListener
        , InterfaceAds.IAdapterListSearchAds, InterfaceAds.IAdapterCategorySearchAds, InterfaceTextOnListener,
        InterfaceMessageHandler.FragmentResult
{
    private RecyclerView rvCategorySearchAds, rvItemSearchAds;


    private LinearLayout lnrSearchAdsActivity;

    private ImageView ivBack, ivHome, ivHelp;
    private CTextView tvTitleSearchActivity;


    //Var
    private AdapterListSearchAds adapterListSearchAds;
    private AdapterSearchCategoryAds adapterSearchCategoryAds;
    private ApiAds apiAds;
    private Context context;
    private ViewLoading viewLoading;
    private CustomTextListener customTextListener;
    private String categoryId;
    private FragmentMessageHandler fragmentMessageHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_ads);

        initView();
        createVar();

        tvTitleSearchActivity.setText("جستجو در آگهی ها");
        searchAds("", "");


    }

    private void initView()
    {

        ivBack = findViewById(R.id.iv_back_toolbar);
        ivHome = findViewById(R.id.iv_home_toolbar);
        ivHelp = findViewById(R.id.iv_help_toolbar);
        tvTitleSearchActivity = findViewById(R.id.tv_title_toolbar);

        lnrSearchAdsActivity = findViewById(R.id.lnr_search_ads_activity);

        ivBack.setOnClickListener(this);
        ivHome.setOnClickListener(this);

        rvCategorySearchAds = findViewById(R.id.rv_category_search_ads);
        rvItemSearchAds = findViewById(R.id.rv_item_search_ads);
        customTextListener = findViewById(R.id.ctl_search_activity);


    }

    private void searchAds(String text, String idCategory)
    {
        viewLoading.show();
        apiAds.searchAds(this, text, idCategory);
    }

    private void createVar()
    {

        context = this;
        viewLoading = new ViewLoading(context);
        apiAds = new ApiAds(context);

        customTextListener.setInterface(this);

        rvCategorySearchAds.setAdapter(adapterSearchCategoryAds = new AdapterSearchCategoryAds(this, this));

        rvItemSearchAds.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvItemSearchAds.setAdapter(adapterListSearchAds = new AdapterListSearchAds(this, this));

    }

    @Override
    public void onResponseSearchAds(boolean response, ModelSearchAds resultSearchAds, String message)
    {
        lnrSearchAdsActivity.setVisibility(View.GONE);
        viewLoading.close();
        if (response)
        {
            lnrSearchAdsActivity.setVisibility(View.VISIBLE);
            adapterSearchCategoryAds.addListSearchCategoryAds(resultSearchAds.getCategory());
            adapterListSearchAds.addListSearchAds(resultSearchAds.getAds());
        }

        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }


    @Override
    public void onClick(View view)
    {

        if (view == ivBack || view == ivHome)
            finish();
        else if (view == ivHelp)
        {
            //TODO FIX For Dialog Help
        }

    }


    @Override
    public void onClickCategorySearchAds(ModelSearchAds.Category category)
    {
        searchAds(customTextListener.getText(), category.getId() + "");
    }


    @Override
    public void onClickItemSearchAds(ModelSearchAds.Ads ads)
    {
        Intent intent = new Intent(SearchAdsActivity.this, AdDetailsActivity.class);
        intent.putExtra(AdDetailsActivity.Ad_ID, ads.getId());
        startActivity(intent);
    }

    @Override
    public void onClickSaveCommentSearchAds(ModelSearchAds.Ads ads)
    {
        //TODO FIX IT
    }

    @Override
    public void onResultTextOnListener(boolean ok, String value)
    {
        if (ok)
            searchAds(value, categoryId);
    }


    @Override
    public void onResponseMessageHandler()
    {
        searchAds(customTextListener.getText(), categoryId);
    }
}
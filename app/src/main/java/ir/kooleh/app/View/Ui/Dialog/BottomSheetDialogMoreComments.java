package ir.kooleh.app.View.Ui.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Comment.ApiComment;
import ir.kooleh.app.Structure.Enum.PaginateValues;
import ir.kooleh.app.Structure.Interface.Comment.InterfaceComment;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCloseBottomSheetMoreComments;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Comment.ModelComment;
import ir.kooleh.app.Structure.Model.Comment.ModelUpdateVoteComment;
import ir.kooleh.app.Utility.Adapter.Comment.AdapterCommentLoad;
import ir.kooleh.app.Utility.Class.CustomLayoutPaginate;
import ir.kooleh.app.Utility.Class.CustomToast;

public class BottomSheetDialogMoreComments extends BottomSheetDialogFragment implements
        InterfaceCloseBottomSheetMoreComments,
        InterfaceTryAgainPaginate,
        InterfaceComment,
        Paginate.Callbacks
{
    private RecyclerView rvMoreComments;
    private AdapterCommentLoad adapterCommentLoad;
    private ProgressBar progressBar;

    private ApiComment apiComment;

    private ModelPaginate modelPaginate;
    private List<ModelComment> commentList  = new ArrayList<>();
    private Paginate paginate;
    private final String adID;
    private boolean loading = true;
    private int nextPage = 1;

    private Parcelable recylerViewState;


    public BottomSheetDialogMoreComments(@NonNull Context context, String adID)
    {
        apiComment = new ApiComment(context);
        this.adID = adID;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.bottom_sheet_dialog_more_comments, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        rvMoreComments = view.findViewById(R.id.rv_bottom_sheet_more_comments);
        progressBar = view.findViewById(R.id.progress_loading_bottom_sheet_more_comments);

        progressBar.setVisibility(View.VISIBLE);
        adapterCommentLoad = new AdapterCommentLoad(this, this.getContext());
        rvMoreComments.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false));
        rvMoreComments.setAdapter(adapterCommentLoad);
        adapterCommentLoad.showIvCloseComment();
        apiComment.getListCommentAds(this, adID, nextPage, PaginateValues.itemsPerPage);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetTransparent);
        return super.onCreateDialog(savedInstanceState);
    }

    public void dismiss()
    {
        if (getDialog() != null)
        {
            if (getDialog().isShowing())
            {
                commentList = null;
                rvMoreComments = null;
                adapterCommentLoad = null;
                if (paginate != null)
                {
                    paginate.unbind();
                    paginate = null;
                }
            }
        }
        super.dismiss();
    }


    @Override
    public void onResponseListComment(boolean response, List<ModelComment> listComment, ModelPaginate modelPaginate, String message)
    {
        progressBar.setVisibility(View.GONE);
        if (response)
        {
            loading = false;
            this.commentList.addAll(listComment);
            this.modelPaginate = modelPaginate;
            adapterCommentLoad.addList(listComment);
            if (paginate == null)
            {
                paginate = Paginate.with(rvMoreComments, this)
                        .setLoadingTriggerThreshold(PaginateValues.threshold)
                        .addLoadingListItem(true)
                        .setLoadingListItemCreator(new CustomLayoutPaginate())
                        .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
                        .build();
                rvMoreComments.getLayoutManager().onRestoreInstanceState(recylerViewState);
            }
        }
        else
        {
            if (nextPage == 1)
            {
                if (getDialog() != null)
                {
                    CustomToast customToast = new CustomToast(this.getContext());
                    customToast.showError(message);
                    dismiss();
                }
            }
            else
            {
                adapterCommentLoad.showBtnTryAgain();
                if (paginate != null)
                {
                    paginate.unbind();
                }
            }
        }
    }

    @Override
    public void onResponseVoteComment(boolean response, ModelUpdateVoteComment.Result updateVoteComment, String message, int position)
    {

    }

    @Override
    public void onResponseSavePointAndComment(boolean response, String message)
    {

    }



    @Override
    public void onResponseCloseBottomSheetMoreComments()
    {
        this.dismiss();
    }

    @Override
    public void onResponseTryAgainPaginate()
    {
        paginate = null;
        recylerViewState = rvMoreComments.getLayoutManager().onSaveInstanceState();
        apiComment.getListCommentAds(this, adID, nextPage, PaginateValues.itemsPerPage);
    }

    @Override
    public void onLoadMore()
    {
        Log.i("paginateOnloadMore", String.valueOf(rvMoreComments.getAdapter().getItemCount()));
        loading = true;
        nextPage++;
        apiComment.getListCommentAds(this, adID, nextPage, PaginateValues.itemsPerPage);
    }

    @Override
    public boolean isLoading()
    {
        Log.i("paginateisloading", String.valueOf(loading));
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems()
    {
        if (modelPaginate != null)
        {
            return commentList.size() == Integer.parseInt(modelPaginate.getTotal());
        }
        return  false;
    }

}
